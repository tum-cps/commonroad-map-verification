from crmapver.verification.owl.satisfaction import OWLVerificationChecker
from crmapver.verification.hol.mapping import HOLMapping
from crmapver.verification.hol.satisfaction import HOLVerificationChecker
from crmapver.verification.asp.mapping import ASPMapping
from crmapver.verification.owl.mapping import OWLMapping
from crmapver.verification.z3.mapping import Z3Mapping
from crmapver.verification.z3.satisfaction import Z3VerificationChecker
from crmapver.verification.asp.satisfaction import ASPVerificationChecker

from typing import Union


Mappings = Union[Z3Mapping, ASPMapping, HOLMapping, OWLMapping]
VerificationCheckers = \
    Union[Z3VerificationChecker, ASPVerificationChecker, HOLVerificationChecker, OWLVerificationChecker]
