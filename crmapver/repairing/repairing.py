from abc import ABC

from commonroad.scenario.lanelet import LaneletNetwork


class ElementRepairing(ABC):
    """
    The class includes all repairing methods for each supported formula.
    """

    def __init__(self, network: LaneletNetwork):
        """
        Constructor.

        :param network: Lanelet network.
        """
        self._complete_map_name = network.meta_information.complete_map_name
        self._network = network
