import copy
from typing import List, Dict, Union

import numpy as np
from commonroad.scenario.intersection import Intersection, IncomingGroup
from commonroad.scenario.lanelet import LaneletNetwork, Lanelet, StopLine
from commonroad.scenario.traffic_sign import TrafficSign, TrafficSignElement
from commonroad.scenario.traffic_light import TrafficLight, TrafficLightCycleElement
from shapely.geometry import LineString

from crmapver.config import MapVerParams
from crmapver.verification.mapping import Mapping, Preprocessing


class ASPMapping(Mapping):
    """
    The class is responsible for mapping the CommonRoad elements to ASP instances by the ASP solver.
    """

    def __init__(self, network: LaneletNetwork, config: MapVerParams = MapVerParams()):
        """
        Constructor.

        :param network: Lanelet network.
        :param config: Configuration.
        """
        super().__init__(network, config)

        self._pre = ASPPreprocessing(network, config)

        self._facts = {}

    @property
    def facts(self) -> Dict[str, List[str]]:
        return self._facts

    def map_lanelet_network(self):
        """
        Maps all elements of a lanelet network to corresponding ASP instances.

        :return: Mapped ASP instances.
        """
        self._map_map()

        for lanelet in self._network.lanelets:
            self._map_lanelet(lanelet)

        for traffic_sign in self._network.traffic_signs:
            self._map_traffic_sign(traffic_sign)

        for traffic_light in self._network.traffic_lights:
            self._map_traffic_light(traffic_light)

        for intersection in self._network.intersections:
            self._map_intersection(intersection)

    def map_verification_paras(self):
        """Maps all verification parameters to corresponding ASP instances."""
        self._store_instance('connection_thresh',
                             [str(self._config.verification.connection_thresh)])
        self._store_instance('border_thresh',
                             [str(self._config.verification.border_thresh)])
        self._store_instance('potential_connection_thresh',
                             [str(self._config.verification.potential_connection_thresh)])
        self._store_instance('potential_border_thresh',
                             [str(self._config.verification.potential_border_thresh)])

    def _map_map(self):
        """Maps all information about the map to corresponding ASP instances."""
        predicate = 'country_id'
        country_id = self._complete_map_name.split("_")[0]
        self._store_instance(predicate, [country_id])

        predicate = 'map_name'
        map_name = self._complete_map_name.split("_")[1].split("-")[0]
        self._store_instance(predicate, [map_name])

        predicate = 'map_id'
        map_id = self._complete_map_name.split("_")[1].split("-")[1]
        self._store_instance(predicate, [map_id])

    def _map_lanelet(self, lanelet: Lanelet):
        """
        Maps lanelet to corresponding ASP instances.

        :param lanelet: Lanelet.
        """
        predicate = 'lanelet'
        lanelet_name = self._lanelet_name(lanelet)
        self._store_instance(predicate, [lanelet_name])

        predicate = 'has_lanelet_id'
        has_lanelet_id = str(lanelet.lanelet_id)
        self._store_instance(predicate, [lanelet_name, has_lanelet_id])

        predicate = 'has_left_polyline'
        has_left_polyline = self._polyline_name(lanelet.left_vertices)
        self._store_instance(predicate, [lanelet_name, has_left_polyline])

        predicate = 'has_right_polyline'
        has_left_polyline = self._polyline_name(lanelet.right_vertices)
        self._store_instance(predicate, [lanelet_name, has_left_polyline])

        predicate = 'has_center_polyline'
        has_center_polyline = self._polyline_name(lanelet.center_vertices)
        self._store_instance(predicate, [lanelet_name, has_center_polyline])

        predicate = 'has_successor'
        has_successors = [str(successor) for successor in lanelet.successor]
        for has_successor in has_successors:
            self._store_instance(predicate, [lanelet_name, has_successor])

        predicate = 'has_predecessor'
        has_predecessors = [str(predecessor) for predecessor in lanelet.predecessor]
        for has_predecessor in has_predecessors:
            self._store_instance(predicate, [lanelet_name, has_predecessor])

        predicate = 'has_left_adj'
        if lanelet.adj_left is not None:
            has_left_adj = str(lanelet.adj_left)
            self._store_instance(predicate, [lanelet_name, has_left_adj])

        predicate = 'has_left_adj_type'
        if lanelet.adj_left is not None and self._network.find_lanelet_by_id(lanelet.adj_left) is not None:
            has_left_adj_type = self._pre.adjacency_types[lanelet.lanelet_id][0]
            self._store_instance(predicate, [lanelet_name, has_left_adj_type])

        predicate = 'has_adj_left_same_dir'
        if lanelet.adj_left_same_direction is not None:
            has_adj_left_same_dir = str(lanelet.adj_left_same_direction)
            self._store_instance(predicate, [lanelet_name, has_adj_left_same_dir])

        predicate = 'has_left_topological_adj'
        has_left_topological_adjs = self._pre.left_topological_adjs.get(lanelet.lanelet_id)
        for has_left_topological_adj in has_left_topological_adjs:
            has_left_topological_adj = str(has_left_topological_adj)
            self._store_instance(predicate, [lanelet_name, has_left_topological_adj])

        predicate = 'has_right_adj'
        if lanelet.adj_right is not None:
            has_right_adj = str(lanelet.adj_right)
            self._store_instance(predicate, [lanelet_name, has_right_adj])

        predicate = 'has_right_adj_type'
        if lanelet.adj_right is not None and self._network.find_lanelet_by_id(lanelet.adj_right) is not None:
            has_right_adj_type = self._pre.adjacency_types[lanelet.lanelet_id][1]
            self._store_instance(predicate, [lanelet_name, has_right_adj_type])

        predicate = 'has_adj_right_same_dir'
        if lanelet.adj_right_same_direction is not None:
            has_adj_right_same_dir = str(lanelet.adj_right_same_direction)
            self._store_instance(predicate, [lanelet_name, has_adj_right_same_dir])

        predicate = 'has_right_topological_adj'
        has_right_topological_adjs = self._pre.right_topological_adjs.get(lanelet.lanelet_id)
        for has_right_topological_adj in has_right_topological_adjs:
            has_right_topological_adj = str(has_right_topological_adj)
            self._store_instance(predicate, [lanelet_name, has_right_topological_adj])

        predicate = 'has_type'
        has_types = [t.value for t in lanelet.lanelet_type]
        for has_type in has_types:
            self._store_instance(predicate, [lanelet_name, has_type])

        predicate = 'has_traffic_sign'
        has_traffic_signs = [str(ref) for ref in lanelet.traffic_signs]
        for has_traffic_sign in has_traffic_signs:
            self._store_instance(predicate, [lanelet_name, has_traffic_sign])

        predicate = 'has_traffic_light'
        has_traffic_lights = [str(ref) for ref in lanelet.traffic_lights]
        for has_traffic_light in has_traffic_lights:
            self._store_instance(predicate, [lanelet_name, has_traffic_light])

        predicate = 'has_stop_line'
        if lanelet.stop_line is not None:
            has_stop_line = self._stop_line_name(lanelet.stop_line)
            self._store_instance(predicate, [lanelet_name, has_stop_line])

        self._map_polyline(lanelet.left_vertices)
        self._map_polyline(lanelet.right_vertices)
        self._map_polyline(lanelet.center_vertices)

        if lanelet.stop_line is not None:
            self._map_stop_line(lanelet.stop_line)

    def _map_polyline(self, vertices: np.ndarray):
        """
        Maps polyline to corresponding ASP instances.

        :param vertices: Vertices.
        """
        predicate = 'polyline'
        polyline_name = self._polyline_name(vertices)
        self._store_instance(predicate, [polyline_name])

        predicate = 'has_polyline_id'
        has_polyline_id = str(hash(str(vertices.tolist())))
        self._store_instance(predicate, [polyline_name, has_polyline_id])

        predicate = 'has_size'
        has_size = str(len(vertices))
        self._store_instance(predicate, [polyline_name, has_size])

        predicate = 'has_length'
        has_length = str(LineString(vertices).length)
        self._store_instance(predicate, [polyline_name, has_length])

        predicate = 'has_vertices'
        has_vertices = str(vertices.tolist())
        self._store_instance(predicate, [polyline_name, has_vertices])

        predicate = 'has_start_vertex'
        has_start_vertex = str(vertices[0].tolist())
        self._store_instance(predicate, [polyline_name, has_start_vertex])

        predicate = 'has_end_vertex'
        has_end_vertex = str(vertices[len(vertices) - 1].tolist())
        self._store_instance(predicate, [polyline_name, has_end_vertex])

        predicate = 'has_similarity_number'
        has_similarity_number = None
        for poly_id, polylines in self._pre.similar_polylines.items():
            for polyline in polylines:
                if vertices is polyline:
                    has_similarity_number = str(poly_id)
        self._store_instance(predicate, [polyline_name, has_similarity_number])

    def _map_stop_line(self, stop_line: StopLine):
        """
        Maps stop line to corresponding ASP instances.

        :param stop_line: Stop line.
        """
        predicate = 'stop_line'
        stop_line_name = self._stop_line_name(stop_line)
        self._store_instance(predicate, [stop_line_name])

        predicate = 'has_stop_line_id'
        has_stop_line_id = str(hash(stop_line))
        self._store_instance(predicate, [stop_line_name, has_stop_line_id])

        predicate = 'has_left_point'
        if stop_line.start is not None:
            has_left_point = str(stop_line.start.tolist())
            self._store_instance(predicate, [stop_line_name, has_left_point])

        predicate = 'has_right_point'
        if stop_line.end is not None:
            has_right_point = str(stop_line.end.tolist())
            self._store_instance(predicate, [stop_line_name, has_right_point])

        predicate = 'has_line_marking'
        has_line_marking = str(stop_line.line_marking.value)
        self._store_instance(predicate, [stop_line_name, has_line_marking])

        predicate = 'has_traffic_sign'
        if stop_line.traffic_sign_ref is not None:
            has_traffic_signs = [str(ref) for ref in stop_line.traffic_sign_ref]
            for has_traffic_sign in has_traffic_signs:
                self._store_instance(predicate, [stop_line_name, has_traffic_sign])

        predicate = 'has_traffic_light'
        if stop_line.traffic_light_ref is not None:
            has_traffic_lights = [str(ref) for ref in stop_line.traffic_light_ref]
            for has_traffic_light in has_traffic_lights:
                self._store_instance(predicate, [stop_line_name, has_traffic_light])

    def _map_traffic_sign(self, traffic_sign: TrafficSign):
        """
        Maps traffic sign to corresponding ASP instances.

        :param traffic_sign: Traffic sign.
        """
        predicate = 'traffic_sign'
        traffic_sign_name = self._traffic_sign_name(traffic_sign)
        self._store_instance(predicate, [traffic_sign_name])

        predicate = 'has_traffic_sign_id'
        has_traffic_sign_id = str(traffic_sign.traffic_sign_id)
        self._store_instance(predicate, [traffic_sign_name, has_traffic_sign_id])

        predicate = 'has_traffic_sign_element'
        has_traffic_sign_elements = [self._traffic_sign_element_name(element)
                                     for element in traffic_sign.traffic_sign_elements]
        for has_traffic_sign_element in has_traffic_sign_elements:
            self._store_instance(predicate, [traffic_sign_name, has_traffic_sign_element])

        predicate = 'has_first_occurrence'
        has_first_occurrences = [str(occ) for occ in list(traffic_sign.first_occurrence)]
        for has_first_occurrence in has_first_occurrences:
            self._store_instance(predicate, [traffic_sign_name, has_first_occurrence])

        predicate = 'has_position'
        if traffic_sign.position is not None:
            has_position = str(traffic_sign.position.tolist())
            self._store_instance(predicate, [traffic_sign_name, has_position])

        for traffic_sign_element in traffic_sign.traffic_sign_elements:
            self._map_traffic_sign_element(traffic_sign_element)

    def _map_traffic_sign_element(self, traffic_sign_element: TrafficSignElement):
        """
        Maps traffic sign element to corresponding ASP instances.

        :param traffic_sign_element: Traffic sign element.
        """
        predicate = 'traffic_sign_element'
        traffic_sign_element_name = self._traffic_sign_element_name(traffic_sign_element)
        self._store_instance(predicate, [traffic_sign_element_name])

        predicate = 'has_traffic_sign_element_id'
        has_traffic_sign_element_id = str(hash(traffic_sign_element))
        self._store_instance(predicate, [traffic_sign_element_name, has_traffic_sign_element_id])

        predicate = 'has_sign_id'
        has_sign_id = str(traffic_sign_element.traffic_sign_element_id.name.lower())
        self._store_instance(predicate, [traffic_sign_element_name, has_sign_id])

        predicate = 'has_additional_value'
        has_additional_values = [str(value) for value in traffic_sign_element.additional_values]
        for has_additional_value in has_additional_values:
            self._store_instance(predicate, [traffic_sign_element_name, has_additional_value])

    def _map_traffic_light(self, traffic_light: TrafficLight):
        """
        Maps traffic light to corresponding ASP instances.

        :param traffic_light: Traffic light.
        """
        predicate = 'traffic_light'
        traffic_light_name = self._traffic_light_name(traffic_light)
        self._store_instance(predicate, [traffic_light_name])

        predicate = 'has_traffic_light_id'
        has_traffic_light_id = str(traffic_light.traffic_light_id)
        self._store_instance(predicate, [traffic_light_name, has_traffic_light_id])

        predicate = 'has_cycle_element'
        has_cycle_elements = [self._cycle_element_name(cycle_element)
                              for cycle_element in traffic_light.traffic_light_cycle.cycle_elements] \
            if traffic_light.traffic_light_cycle is not None else []
        for has_cycle_element in has_cycle_elements:
            self._store_instance(predicate, [traffic_light_name, has_cycle_element])

        predicate = 'has_position'
        if traffic_light.position is not None:
            has_position = str(traffic_light.position)
            self._store_instance(predicate, [traffic_light_name, has_position])

        if traffic_light.traffic_light_cycle is not None:
            for cycle_element in traffic_light.traffic_light_cycle.cycle_elements:
                self._map_cycle_element(cycle_element)

    def _map_cycle_element(self, cycle_element: TrafficLightCycleElement):
        """
        Maps cycle element to corresponding ASP instances.

        :param cycle_element: Cycle element.
        """
        predicate = 'cycle_element'
        cycle_element_name = self._cycle_element_name(cycle_element)
        self._store_instance(predicate, [cycle_element_name])

        predicate = 'has_duration'
        has_duration = cycle_element.duration
        self._store_instance(predicate, [cycle_element_name, has_duration])

        predicate = 'has_state'
        has_state = str(cycle_element.state)
        self._store_instance(predicate, [cycle_element_name, has_state])

    def _map_intersection(self, intersection: Intersection):
        """
        Maps intersection to corresponding ASP instances.

        :param intersection: Intersection.
        """
        predicate = 'intersection'
        intersection_name = self._intersection_name(intersection)
        self._store_instance(predicate, [intersection_name])

        predicate = 'has_intersection_id'
        has_intersection_id = str(intersection.intersection_id)
        self._store_instance(predicate, [intersection_name, has_intersection_id])

        predicate = 'has_incoming_element'
        has_incoming_elements = [self._incoming_element_name(inc) for inc in intersection.incomings]
        for has_incoming_element in has_incoming_elements:
            self._store_instance(predicate, [intersection_name, has_incoming_element])

        predicate = 'has_clockwise_incoming'
        has_clockwise_incomings = self._pre.clockwise_incomings.get(intersection.intersection_id)
        for i, has_clockwise_incoming in enumerate(has_clockwise_incomings):
            self._store_instance(predicate, [intersection_name, str(has_clockwise_incoming), i])

        for incoming_element in intersection.incomings:
            self._map_incoming_element(incoming_element)

    def _map_incoming_element(self, incoming_group: IncomingGroup):
        """
        Maps incoming element to corresponding ASP instances.

        :param incoming_group: Incoming group element.
        """
        predicate = 'incoming_element'
        incoming_element_name = self._incoming_element_name(incoming_group)
        self._store_instance(predicate, [incoming_element_name])

        predicate = 'has_incoming_element_id'
        has_incoming_element_id = str(incoming_group.incoming_id)
        self._store_instance(predicate, [incoming_element_name, has_incoming_element_id])

        predicate = 'has_incoming_lanelet'
        has_incoming_lanelets = [str(inc) for inc in list(incoming_group.incoming_lanelets)]
        for has_incoming_lanelet in has_incoming_lanelets:
            self._store_instance(predicate, [incoming_element_name, has_incoming_lanelet])

        predicate = 'has_outgoing_right'
        has_outgoings_right = [str(out) for out in list(incoming_group.outgoing_right)]
        for has_outgoing_right in has_outgoings_right:
            self._store_instance(predicate, [incoming_element_name, has_outgoing_right])

        predicate = 'has_outgoing_straight'
        has_outgoings_straight = [str(out) for out in list(incoming_group.outgoing_straight)]
        for has_outgoing_straight in has_outgoings_straight:
            self._store_instance(predicate, [incoming_element_name, has_outgoing_straight])

        predicate = 'has_outgoing_left'
        has_outgoings_left = [str(out) for out in list(incoming_group.outgoing_left)]
        for has_outgoing_left in has_outgoings_left:
            self._store_instance(predicate, [incoming_element_name, has_outgoing_left])

    def _store_instance(self, predicate: str, names: List[Union[str, int, float]]):
        """
        Generates and stores an instance.

        :param predicate: Name of predicate.
        :param names: Arguments of instance.
        """
        instance = predicate + "("
        for index, name in enumerate(names, start=0):
            if type(name) == str:
                instance = instance + "\"" + name + "\""
            elif type(name) == int or type(name) == float:
                instance = instance + str(name)
            if index != len(names) - 1:
                instance = instance + ", "
        instance = instance + ")."
        if predicate in self._facts.keys():
            pre_instances = self._facts[predicate]
            self._facts.update({predicate: pre_instances + [instance]})
        else:
            self._facts.update({predicate: [instance]})

    @staticmethod
    def _lanelet_name(lanelet: Lanelet) -> str:
        """
        Returns unique name of a lanelet.

        :return: Name of lanelet.
        """
        return 'lan' + str(hash(lanelet))

    @staticmethod
    def _polyline_name(vertices: np.ndarray) -> str:
        """
        Returns unique name of a polyline.

        :return: Name of polyline.
        """
        return 'pol' + str(hash(str(vertices.tolist())))

    @staticmethod
    def _stop_line_name(stop_line: StopLine) -> str:
        """
        Returns unique name of a stop line.

        :return: Name of stop line.
        """
        return 'sto' + str(hash(stop_line))

    @staticmethod
    def _traffic_sign_name(traffic_sign: TrafficSign) -> str:
        """
        Returns unique name of a traffic sign.

        :return: Name of traffic sign.
        """
        return 'trs' + str(hash(traffic_sign))

    @staticmethod
    def _traffic_sign_element_name(traffic_sign_element: TrafficSignElement) -> str:
        """
        Returns unique name of a traffic sign element.

        :return: Name of traffic sign element.
        """
        return 'tse' + str(hash(traffic_sign_element))

    @staticmethod
    def _traffic_light_name(traffic_light: TrafficLight):
        """
        Returns unique name of a traffic light.

        :return: Name of traffic light.
        """
        return 'trl' + str(hash(traffic_light))

    @staticmethod
    def _cycle_element_name(cycle_element: TrafficLightCycleElement):
        """
        Returns unique name of a cycle element.

        :return: Name of cycle element.
        """
        return 'cyc' + str(hash(cycle_element))

    @staticmethod
    def _intersection_name(intersection: Intersection):
        """
        Returns unique name of an intersection.

        :return: Name of intersection.
        """
        return 'int' + str(hash(intersection))

    @staticmethod
    def _incoming_element_name(incoming_group: IncomingGroup):
        """
        Returns unique name of an incoming element.

        :return: Name of incoming element.
        """
        return 'inc' + str(hash(incoming_group))


class ASPPreprocessing(Preprocessing):
    """
    The class is responsible for preprocessing map properties mapped to ASP instances.
    """

    def __init__(self, network: LaneletNetwork, config: MapVerParams = MapVerParams()):
        super().__init__(network, config)

        self._clockwise_incomings = None
        self._clockwise_incomings = self.clockwise_incomings

    @property
    def clockwise_incomings(self) -> Dict[int, List[int]]:
        if self._clockwise_incomings is None:
            self._clockwise_incomings = {}
            for intersection in self._network.intersections:
                incomings = intersection.incomings
                incomings_id = [incoming.incoming_id for incoming in incomings]
                c_incomings = self._compute_left_list(copy.deepcopy(incomings), incomings_id)
                self._clockwise_incomings.update({intersection.intersection_id: c_incomings})

        return self._clockwise_incomings

    def _compute_left_list(self, incomings: List[IncomingGroup], incomings_id: List[int]) -> List[int]:
        """
        Finds clockwise order for the incomings.

        :param incomings: Incomings of one intersection.
        :param incomings_id: List of incoming IDs.
        :return: Incomings in clockwise order.
        """
        left_list = []

        # check requirements for computing left list
        for incoming in incomings:
            incs = list(incoming.incoming_lanelets)
            for lanelet_id in incs:
                lanelet = self._network.find_lanelet_by_id(lanelet_id)
                if lanelet is None:
                    incoming.incoming_lanelets.remove(lanelet_id)

        incs = []
        for incoming in incomings:
            if len(incoming.incoming_lanelets) > 0:
                incs.append(incoming)
        incomings = incs

        if len(incomings) < 2:
            return left_list

        # choose a reference incoming vector
        ref = self._ref_v(0, incomings)
        angles = [(0, 0)]

        # calculate all incoming angle from the reference incoming vector
        for index in range(1, len(incomings)):
            new_v = self._ref_v(index, incomings)
            angle = self._get_angle(ref, new_v)
            if angle < 0:
                angle += 360
            angles.append((index, angle))

        # sort the angles from the reference to go clockwise
        angles.sort(key=lambda tup: tup[1])
        prev = -1

        # take the incomings which have less than 90 degrees in between
        for index in range(0, len(incomings)):
            angle = angles[index][1] - angles[prev][1]
            if angle < 0:
                angle += 360

            intersection_straight_thresh = 35.

            # add is_left_of relation if angle is less than intersection straight threshold
            if angle <= 180. - intersection_straight_thresh:
                # is left of the previous incoming
                is_left_of = angles[prev][0]
                left_list.append(incomings_id[is_left_of])
            else:
                left_list.append(incomings_id[angles[prev][0]])
                # left_list.append(None)

            prev = index

        return left_list

    def _ref_v(self, incoming_index: int, incomings: List[IncomingGroup]) -> np.ndarray:
        """
        Gets reference vector of specific incoming.

        :param incoming_index: Specific incoming index.
        :param incomings: All incomings of one intersection.
        :return: Reference vector of one specific incoming.
        """
        lanelets = incomings[incoming_index].incoming_lanelets
        front_node, back_node = [], []

        for laneId in lanelets:
            lane = self._network.find_lanelet_by_id(laneId)
            if lane is None:
                continue

            front_node.append(lane.center_vertices[-1])
            if len(lane.center_vertices) > 4:
                back_node.append(lane.center_vertices[-4])
            else:
                back_node.append(lane.center_vertices[0])

        front_node.sort(key=lambda tup: tup[0])
        back_node.sort(key=lambda tup: tup[0])

        ref = 0.5 * (front_node[-1] + front_node[0]) - 0.5 * (back_node[-1] + back_node[0])

        return ref

    @staticmethod
    def _get_angle(v1: np.ndarray, v2: np.ndarray) -> float:
        """
        Get clockwise angle between vectors.

        :param v1: First vector.
        :param v2: Second vector.
        :return: Clockwise angle between vectors in degrees.
        """
        x = [v1[0], v2[0]]
        y = [v1[1], v2[1]]
        angles = np.arctan2(y, x) + np.pi
        diff1 = angles[0] - angles[1]
        angle = diff1 / np.pi * 180.
        return angle
