import dataclasses
import pathlib
from typing import List
import os


@dataclasses.dataclass
class FormulaManagerASP:
    f_lps: str = ""
    lp_files: List[str] = dataclasses.field(
            default_factory=lambda: [
                "base_formulas.lp", "lanelet_formulas.lp", "traffic_sign_formulas.lp",  "traffic_light_formulas.lp",
                "intersection_formulas.lp"])
    content_dir: str = str(pathlib.Path(__file__).resolve().parent) + "/"

    def __post_init__(self):
        for f_lp in [open(os.path.join(self.content_dir, lp_file), "r").read() for lp_file in self.lp_files]:
            self.f_lps += f_lp + "\n\n\n"
