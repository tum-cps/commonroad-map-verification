% Lanelet formulas

#program unique_id_la.
unique_id_la(ID1) :- lanelet(L1), lanelet(L2), L1 != L2, has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2), ID1 == ID2.

#program same_vertices_size.
same_vertices_size(ID) :- lanelet(L), polyline(P1), polyline(P2), has_lanelet_id(L, ID), has_left_polyline(L, P1), has_right_polyline(L, P2), has_size(P1, S1), has_size(P2, S2), S1 != S2.

#program vertices_more_than_one.
vertices_more_than_one(ID) :- lanelet(L), polyline(P), has_lanelet_id(L, ID), has_left_polyline(L, P), has_size(P, S), S <= 1.
vertices_more_than_one(ID) :- lanelet(L), polyline(P), has_lanelet_id(L, ID), has_right_polyline(L, P), has_size(P, S), S <= 1.

#program existence_left_adj.
valid_left_adj_ref(ID1) :- lanelet(L1), lanelet(L2), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2), ID1 != ID2, has_left_adj(L1, R), ID2 == R.
existence_left_adj(ID) :- lanelet(L), has_lanelet_id(L, ID), has_left_adj(L, _), not valid_left_adj_ref(ID).

#program existence_right_adj.
valid_right_adj_ref(ID1) :- lanelet(L1), lanelet(L2), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2), ID1 != ID2, has_right_adj(L1, R), ID2 == R.
existence_right_adj(ID) :- lanelet(L), has_lanelet_id(L, ID), has_right_adj(L, _), not valid_right_adj_ref(ID).

#program existence_predecessor.
valid_predecessor_ref(ID1, R) :- lanelet(L1), lanelet(L2), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2), has_predecessor(L1, R), ID2 == R.
existence_predecessor(ID, R) :- lanelet(L), has_lanelet_id(L, ID), has_predecessor(L, R), not valid_predecessor_ref(ID, R).

#program existence_successor.
valid_successor_ref(ID1, R) :- lanelet(L1), lanelet(L2), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2), has_successor(L1, R), ID2 == R.
existence_successor(ID, R) :- lanelet(L), has_lanelet_id(L, ID), has_successor(L, R), not valid_successor_ref(ID, R).

#program connections_predecessor.
valid_pre_connections(ID1, ID2) :- lanelet(L1), lanelet(L2), polyline(P1), polyline(P2), polyline(P3), polyline(P4), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2), has_predecessor(L1, ID2),
                                   has_left_polyline(L1, P1), has_right_polyline(L1, P2), has_left_polyline(L2, P3), has_right_polyline(L2, P4), has_start_vertex(P1, V1), has_start_vertex(P2, V2),
                                   has_end_vertex(P3, V3), has_end_vertex(P4, V4), connection_thresh(T), @are_similar_points(V1, V3, T) == "True", @are_similar_points(V2, V4, T) == "True".
connections_predecessor(ID, R) :- lanelet(L), has_lanelet_id(L, ID), has_predecessor(L, R), not valid_pre_connections(ID, R).

#program connections_successor.
valid_suc_connections(ID1, ID2) :- lanelet(L1), lanelet(L2), polyline(P1), polyline(P2), polyline(P3), polyline(P4), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2), has_successor(L1, ID2),
                                   has_left_polyline(L1, P1), has_right_polyline(L1, P2), has_left_polyline(L2, P3), has_right_polyline(L2, P4), has_end_vertex(P1, V1), has_end_vertex(P2, V2),
                                   has_start_vertex(P3, V3), has_start_vertex(P4, V4), connection_thresh(T), @are_similar_points(V1, V3, T) == "True", @are_similar_points(V2, V4, T) == "True".
connections_successor(ID, R) :- lanelet(L), has_lanelet_id(L, ID), has_successor(L, R), not valid_suc_connections(ID, R).

#program polylines_left_same_dir_parallel_adj.
polylines_left_same_dir_parallel_adj(ID1, ID2) :- lanelet(L1), lanelet(L2), polyline(P1), polyline(P2), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2), has_left_adj(L1, ID2),
                                                  has_adj_left_same_dir(L1, "True"), has_left_adj_type(L1, "Parallel"), has_left_polyline(L1, P1), has_right_polyline(L2, P2),
                                                  has_vertices(P1, V1), has_vertices(P2, V2), border_thresh(T), @are_similar_polylines(V1, V2, "True", T) == "False".

#program polylines_left_opposite_dir_parallel_adj.
polylines_left_opposite_dir_parallel_adj(ID1, ID2) :- lanelet(L1), lanelet(L2), polyline(P1), polyline(P2), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2), has_left_adj(L1, ID2),
                                                      has_adj_left_same_dir(L1, "False"), has_left_adj_type(L1, "Parallel"), has_left_polyline(L1, P1), has_left_polyline(L2, P2),
                                                      has_vertices(P1, V1), has_vertices(P2, V2), border_thresh(T), @are_similar_polylines(V1, V2, "False", T) == "False".

#program polylines_right_same_dir_parallel_adj.
polylines_right_same_dir_parallel_adj(ID1, ID2) :- lanelet(L1), lanelet(L2), polyline(P1), polyline(P2), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2), has_right_adj(L1, ID2),
                                                   has_adj_right_same_dir(L1, "True"), has_right_adj_type(L1, "Parallel"), has_right_polyline(L1, P1), has_left_polyline(L2, P2),
                                                   has_vertices(P1, V1), has_vertices(P2, V2), border_thresh(T), @are_similar_polylines(V1, V2, "True", T) == "False".

#program polylines_right_opposite_dir_parallel_adj.
polylines_right_opposite_dir_parallel_adj(ID1, ID2) :- lanelet(L1), lanelet(L2), polyline(P1), polyline(P2), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2), has_right_adj(L1, ID2),
                                                       has_adj_right_same_dir(L1, "False"), has_right_adj_type(L1, "Parallel"), has_right_polyline(L1, P1), has_right_polyline(L2, P2),
                                                       has_vertices(P1, V1), has_vertices(P2, V2), border_thresh(T), @are_similar_polylines(V1, V2, "False", T) == "False".

#program connections_left_merging_adj.
valid_left_merging_connections(ID1, ID2) :- lanelet(L1), lanelet(L2), polyline(P1), polyline(P2), polyline(P3), polyline(P4), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2),
                                            has_left_adj(L1, ID2), has_left_adj_type(L1, "Merge"), has_left_polyline(L1, P1), has_right_polyline(L1, P2), has_left_polyline(L2, P3),
                                            has_right_polyline(L2, P4), has_start_vertex(P1, V1), has_end_vertex(P1, V2), has_end_vertex(P2, V3), has_start_vertex(P4, V4),
                                            has_end_vertex(P3, V5), has_end_vertex(P4, V6), connection_thresh(T), @are_similar_points(V1, V4, T) == "True", @are_similar_points(V2, V5, T) == "True",
                                            @are_similar_points(V3, V6, T) == "True".
connections_left_merging_adj(ID, R) :- lanelet(L), has_lanelet_id(L, ID), has_left_adj(L, R), has_left_adj_type(L, "Merge"), not valid_left_merging_connections(ID, R).

#program connections_right_merging_adj.
valid_right_merging_connections(ID1, ID2) :- lanelet(L1), lanelet(L2), polyline(P1), polyline(P2), polyline(P3), polyline(P4), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2),
                                             has_right_adj(L1, ID2), has_right_adj_type(L1, "Merge"), has_left_polyline(L1, P1), has_right_polyline(L1, P2), has_left_polyline(L2, P3),
                                             has_right_polyline(L2, P4), has_start_vertex(P2, V1), has_end_vertex(P1, V2), has_end_vertex(P2, V3), has_start_vertex(P3, V4),
                                             has_end_vertex(P3, V5), has_end_vertex(P4, V6), connection_thresh(T), @are_similar_points(V1, V4, T) == "True", @are_similar_points(V2, V5, T) == "True",
                                             @are_similar_points(V3, V6, T) == "True".
connections_right_merging_adj(ID, R) :- lanelet(L), has_lanelet_id(L, ID), has_right_adj(L, R), has_right_adj_type(L, "Merge"), not valid_right_merging_connections(ID, R).

#program connections_left_forking_adj.
valid_left_forking_connections(ID1, ID2) :- lanelet(L1), lanelet(L2), polyline(P1), polyline(P2), polyline(P3), polyline(P4), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2),
                                            has_left_adj(L1, ID2), has_left_adj_type(L1, "Fork"), has_left_polyline(L1, P1), has_right_polyline(L1, P2), has_left_polyline(L2, P3),
                                            has_right_polyline(L2, P4), has_start_vertex(P1, V1), has_start_vertex(P2, V2), has_end_vertex(P1, V3), has_start_vertex(P3, V4),
                                            has_start_vertex(P4, V5), has_end_vertex(P4, V6), connection_thresh(T), @are_similar_points(V1, V4, T) == "True", @are_similar_points(V2, V5, T) == "True",
                                            @are_similar_points(V3, V6, T) == "True".
connections_left_forking_adj(ID, R) :- lanelet(L), has_lanelet_id(L, ID), has_left_adj(L, R), has_left_adj_type(L, "Fork"), not valid_left_forking_connections(ID, R).

#program connections_right_forking_adj.
valid_right_forking_connections(ID1, ID2) :- lanelet(L1), lanelet(L2), polyline(P1), polyline(P2), polyline(P3), polyline(P4), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2),
                                             has_right_adj(L1, ID2), has_right_adj_type(L1, "Fork"), has_left_polyline(L1, P1), has_right_polyline(L1, P2), has_left_polyline(L2, P3),
                                             has_right_polyline(L2, P4), has_start_vertex(P1, V1), has_start_vertex(P2, V2), has_end_vertex(P2, V3), has_start_vertex(P3, V4),
                                             has_start_vertex(P4, V5), has_end_vertex(P3, V6), connection_thresh(T), @are_similar_points(V1, V4, T) == "True", @are_similar_points(V2, V5, T) == "True",
                                             @are_similar_points(V3, V6, T) == "True".
connections_right_forking_adj(ID, R) :- lanelet(L), has_lanelet_id(L, ID), has_right_adj(L, R), has_right_adj_type(L, "Fork"), not valid_right_forking_connections(ID, R).


#program potential_predecessor.
potential_predecessor(ID1, ID2) :- lanelet(L1), lanelet(L2), polyline(P1), polyline(P2), polyline(P3), polyline(P4), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2), not has_predecessor(L1, ID2),
                                   has_left_polyline(L1, P1), has_right_polyline(L1, P2), has_left_polyline(L2, P3), has_right_polyline(L2, P4), has_start_vertex(P1, V1), has_start_vertex(P2, V2),
                                   has_end_vertex(P3, V3), has_end_vertex(P4, V4), potential_connection_thresh(T), @are_similar_points(V1, V3, T) == "True", @are_similar_points(V2, V4, T) == "True".

#program potential_successor.
potential_successor(ID1, ID2) :- lanelet(L1), lanelet(L2), polyline(P1), polyline(P2), polyline(P3), polyline(P4), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2), not has_successor(L1, ID2),
                                 has_left_polyline(L1, P1), has_right_polyline(L1, P2), has_left_polyline(L2, P3), has_right_polyline(L2, P4), has_end_vertex(P1, V1), has_end_vertex(P2, V2),
                                 has_start_vertex(P3, V3), has_start_vertex(P4, V4), potential_connection_thresh(T), @are_similar_points(V1, V3, T) == "True", @are_similar_points(V2, V4, T) == "True".

#program potential_left_same_dir_parallel_adj.
potential_left_same_dir_parallel_adj(ID1, ID2) :- lanelet(L1), lanelet(L2), polyline(P1), polyline(P2), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2), L1 != L2, not has_left_adj(L1, ID2),
                                                  has_left_polyline(L1, P1), has_right_polyline(L2, P2), has_similarity_number(P1, N), has_similarity_number(P2, N).

#program potential_left_opposite_dir_parallel_adj.
potential_left_opposite_dir_parallel_adj(ID1, ID2) :- lanelet(L1), lanelet(L2), polyline(P1), polyline(P2), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2), L1 != L2, not has_left_adj(L1, ID2),
                                                      has_left_polyline(L1, P1), has_left_polyline(L2, P2), has_similarity_number(P1, N), has_similarity_number(P2, N).

#program potential_right_same_dir_parallel_adj.
potential_right_same_dir_parallel_adj(ID1, ID2) :- lanelet(L1), lanelet(L2), polyline(P1), polyline(P2), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2), L1 != L2, not has_right_adj(L1, ID2),
                                                   has_right_polyline(L1, P1), has_left_polyline(L2, P2), has_similarity_number(P1, N), has_similarity_number(P2, N).

#program potential_right_opposite_dir_parallel_adj.
potential_right_opposite_dir_parallel_adj(ID1, ID2) :- lanelet(L1), lanelet(L2), polyline(P1), polyline(P2), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2), L1 != L2, not has_right_adj(L1, ID2),
                                                       has_right_polyline(L1, P1), has_right_polyline(L2, P2), has_similarity_number(P1, N), has_similarity_number(P2, N).

#program potential_left_merging_adj.
potential_left_merging_adj(ID1, ID2) :- lanelet(L1), lanelet(L2), polyline(P1), polyline(P2), polyline(P3), polyline(P4), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2),
                                        not has_left_adj(L1, ID2), has_left_polyline(L1, P1), has_right_polyline(L1, P2), has_left_polyline(L2, P3),
                                        has_right_polyline(L2, P4), has_start_vertex(P1, V1), has_end_vertex(P1, V2), has_end_vertex(P2, V3), has_start_vertex(P4, V4),
                                        has_end_vertex(P3, V5), has_end_vertex(P4, V6), potential_connection_thresh(T), @are_similar_points(V1, V4, T) == "True",
                                        @are_similar_points(V2, V5, T) == "True", @are_similar_points(V3, V6, T) == "True".

#program potential_right_merging_adj.
potential_right_merging_adj(ID1, ID2) :- lanelet(L1), lanelet(L2), polyline(P1), polyline(P2), polyline(P3), polyline(P4), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2),
                                         not has_right_adj(L1, ID2), has_left_polyline(L1, P1), has_right_polyline(L1, P2), has_left_polyline(L2, P3),
                                         has_right_polyline(L2, P4), has_start_vertex(P2, V1), has_end_vertex(P1, V2), has_end_vertex(P2, V3), has_start_vertex(P3, V4),
                                         has_end_vertex(P3, V5), has_end_vertex(P4, V6), potential_connection_thresh(T), @are_similar_points(V1, V4, T) == "True",
                                         @are_similar_points(V2, V5, T) == "True", @are_similar_points(V3, V6, T) == "True".

#program potential_left_forking_adj.
potential_left_forking_adj(ID1, ID2) :- lanelet(L1), lanelet(L2), polyline(P1), polyline(P2), polyline(P3), polyline(P4), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2),
                                        not has_left_adj(L1, ID2), has_left_polyline(L1, P1), has_right_polyline(L1, P2), has_left_polyline(L2, P3),
                                        has_right_polyline(L2, P4), has_start_vertex(P1, V1), has_start_vertex(P2, V2), has_end_vertex(P1, V3), has_start_vertex(P3, V4),
                                        has_start_vertex(P4, V5), has_end_vertex(P4, V6), potential_connection_thresh(T), @are_similar_points(V1, V4, T) == "True",
                                        @are_similar_points(V2, V5, T) == "True", @are_similar_points(V3, V6, T) == "True".

#program potential_right_forking_adj.
potential_right_forking_adj(ID1, ID2) :- lanelet(L1), lanelet(L2), polyline(P1), polyline(P2), polyline(P3), polyline(P4), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2),
                                          not has_right_adj(L1, ID2), has_left_polyline(L1, P1), has_right_polyline(L1, P2), has_left_polyline(L2, P3),
                                          has_right_polyline(L2, P4), has_start_vertex(P1, V1), has_start_vertex(P2, V2), has_end_vertex(P2, V3), has_start_vertex(P3, V4),
                                          has_start_vertex(P4, V5), has_end_vertex(P3, V6), potential_connection_thresh(T), @are_similar_points(V1, V4, T) == "True",
                                          @are_similar_points(V2, V5, T) == "True", @are_similar_points(V3, V6, T) == "True".

#program non_predecessor_as_successor.
non_predecessor_as_successor(ID1, ID2) :- lanelet(L1), lanelet(L2), polyline(P1), polyline(P2), polyline(P3), polyline(P4), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2),
                                          not has_predecessor(L1, ID2), has_successor(L1, ID2), has_left_polyline(L1, P1), has_right_polyline(L1, P2), has_left_polyline(L2, P3),
                                          has_right_polyline(L2, P4), has_start_vertex(P1, V1), has_start_vertex(P2, V2), has_end_vertex(P3, V3), has_end_vertex(P4, V4), connection_thresh(T),
                                          @are_similar_points(V1, V3, T) == "True", @are_similar_points(V2, V4, T) == "True".

#program non_successor_as_predecessor.
non_successor_as_predecessor(ID1, ID2) :- lanelet(L1), lanelet(L2), polyline(P1), polyline(P2), polyline(P3), polyline(P4), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2),
                                          not has_successor(L1, ID2), has_predecessor(L1, ID2), has_left_polyline(L1, P1), has_right_polyline(L1, P2), has_left_polyline(L2, P3),
                                          has_right_polyline(L2, P4), has_end_vertex(P1, V1), has_end_vertex(P2, V2), has_start_vertex(P3, V3), has_start_vertex(P4, V4),
                                          connection_thresh(T), @are_similar_points(V1, V3, T) == "True", @are_similar_points(V2, V4, T) == "True".

#program polylines_intersection.
polylines_intersection(ID) :- lanelet(L), polyline(P1), polyline(P2), has_lanelet_id(L, ID), has_left_polyline(L, P1), has_right_polyline(L, P2), has_vertices(P1, V1), has_vertices(P2, V2),
                              @are_mutually_intersected_polylines(V1, V2) == "True".

#program left_self_intersection.
left_self_intersection(ID) :- lanelet(L), polyline(P), has_lanelet_id(L, ID), has_left_polyline(L, P), has_vertices(P, V), @is_self_intersected_polyline(V) == "True".

#program right_self_intersection.
right_self_intersection(ID) :- lanelet(L), polyline(P), has_lanelet_id(L, ID), has_right_polyline(L, P), has_vertices(P, V), @is_self_intersected_polyline(V) == "True".

#program lanelet_types_combination.
lanelet_types_combination(ID, T1, T2) :- lanelet(L), has_lanelet_id(L, ID), has_type(L, T1), has_type(L, T2), T1 != T2,
                                         1 {T1 == "urban" : T2 = ("country");
                                            T1 == "country" : T2 = ("urban");
                                            T1 == "highway" : T2 = ("interstate");
                                            T1 == "driveWay" : T2 = ("interstate"; "highway"; "busLane"; "busStop"; "bicycleLane"; "intersection");
                                            T1 == "mainCarriageWay" : T2 = ("crosswalk", "intersection");
                                            T1 == "shoulder" : T2 = ("busLane", "busStop", "sidewalk");
                                            T1 == "busLane" : T2 = ("shoulder", "driveWay", "interstate");
                                            T1 == "busStop" : T2 = ("interstate", "shoulder", "driveWay");
                                            T1 == "bicycleLane" : T2 = ("interstate", "driveWay");
                                            T1 == "sidewalk" : T2 = ("interstate");
                                            T1 == "crosswalk" : T2 = ("mainCarriageWay", "interstate");
                                            T1 == "interstate" : T2 = ("busStop"; "crosswalk");
                                            T1 == "intersection" : T2 = ("driveWay", "mainCarriageWay")}.

#program non_followed_composable_lanelets.
composable_lanelets(ID1, ID2) :- lanelet(L1), lanelet(L2), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2), has_successor(L1, ID2), #count{R : has_successor(L1, R)} == 1,
                                 #count{R : has_predecessor(L2, R)} == 1, has_traffic_sign(L2, SID) : has_traffic_sign(L1, SID); has_traffic_light(L2, TID) : has_traffic_light(L1, TID);
                                 has_type(L2, T) : has_type(L1, T).
non_left_topological_composable_lanelets(ID1, ID2) :- lanelet(L1), lanelet(L2), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2), has_successor(L1, ID2),
                                                      #count{R : has_left_topological_adj(L1, R)} == C1, #count{R : has_left_topological_adj(L2, R)} == C2, C1 != C2.
non_left_topological_composable_lanelets(ID1, ID2) :- lanelet(L1), lanelet(L2), lanelet(L3), lanelet(4), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2), has_lanelet_id(L3, ID3),
                                                      has_lanelet_id(L4, ID4), has_successor(L1, ID2), has_left_topological_adj(L1, ID3), has_left_topological_adj(L2, ID4), has_successor(L3, ID4),
                                                      not composable_lanelets(ID3, ID4).
non_right_topological_composable_lanelets(ID1, ID2) :- lanelet(L1), lanelet(L2), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2), has_successor(L1, ID2),
                                                       #count{R : has_right_topological_adj(L1, R)} == C1, #count{R : has_right_topological_adj(L2, R)} == C2, C1 != C2.
non_right_topological_composable_lanelets(ID1, ID2) :- lanelet(L1), lanelet(L2), lanelet(L3), lanelet(4), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2), has_lanelet_id(L3, ID3),
                                                       has_lanelet_id(L4, ID4), has_successor(L1, ID2), has_right_topological_adj(L1, ID3), has_right_topological_adj(L2, ID4), has_successor(L3, ID4),
                                                       not composable_lanelets(ID3, ID4).
non_followed_composable_lanelets(ID1, ID2) :- lanelet(L1), lanelet(L2), has_lanelet_id(L1, ID1), has_lanelet_id(L2, ID2), has_successor(L1, ID2), composable_lanelets(ID1, ID2),
                                              not non_left_topological_composable_lanelets(ID1, ID2), not non_right_topological_composable_lanelets(ID1, ID2).


#program referenced_intersecting_lanelets.

#program existence_traffic_signs.
valid_traffic_sign_ref(LID, SID) :- lanelet(L), traffic_sign(S), has_lanelet_id(L, LID), has_traffic_sign_id(S, SID), has_traffic_sign(L, SID).
existence_traffic_signs(LID, SID) :- lanelet(L), has_lanelet_id(L, LID), has_traffic_sign(L, SID), not valid_traffic_sign_ref(LID, SID).

#program existence_traffic_lights.
valid_traffic_light_ref(LID, TID) :- lanelet(L), traffic_light(T), has_lanelet_id(L, LID), has_traffic_light_id(T, TID), has_traffic_light(L, TID).
existence_traffic_lights(LID, TID) :- lanelet(L), has_lanelet_id(L, LID), has_traffic_light(L, TID), not valid_traffic_light_ref(LID, TID).

#program existence_stop_line_traffic_signs.
valid_stop_line_traffic_sign_ref(LID, SID) :- lanelet(L), stop_line(SL), traffic_sign(S), has_lanelet_id(L, LID), has_traffic_sign_id(S, SID), has_stop_line(L, SL),
                                              has_traffic_sign(SL, SID).
existence_stop_line_traffic_signs(LID, SID) :- lanelet(L), stop_line(SL), has_lanelet_id(L, LID), has_stop_line(L, SL), has_traffic_sign(SL, SID),
                                               not valid_stop_line_traffic_sign_ref(LID, SID).

#program existence_stop_line_traffic_lights.
valid_stop_line_traffic_light_ref(LID, TID) :- lanelet(L), stop_line(SL), traffic_light(S), has_lanelet_id(L, LID), has_traffic_light_id(S, TID), has_stop_line(L, SL),
                                               has_traffic_light(SL, TID).
existence_stop_line_traffic_lights(LID, TID) :- lanelet(L), stop_line(SL), has_lanelet_id(L, LID), has_stop_line(L, SL), has_traffic_light(SL, TID),
                                                not valid_stop_line_traffic_light_ref(LID, TID).

#program included_stop_line_traffic_signs.
included_stop_line_traffic_signs(LID, SID) :- lanelet(L), stop_line(SL), traffic_sign(S), has_lanelet_id(L, LID), has_traffic_sign_id(S, SID), has_stop_line(L, SL),
                                              has_traffic_sign(SL, SID), not has_traffic_sign(L, SID).

#program included_stop_line_traffic_lights.
included_stop_line_traffic_lights(LID, TID) :- lanelet(L), stop_line(SL), traffic_light(T), has_lanelet_id(L, LID), has_traffic_light_id(T, TID), has_stop_line(L, SL),
                                               has_traffic_light(SL, TID), not has_traffic_light(L, TID).

#program zero_or_two_points_stop_line.
zero_or_two_points_stop_line(ID) :- lanelet(L), stop_line(SL), has_lanelet_id(L, ID), has_stop_line(L, SL), has_left_point(SL, _), not has_right_point(SL, _).
zero_or_two_points_stop_line(ID) :- lanelet(L), stop_line(SL), has_lanelet_id(L, ID), has_stop_line(L, SL), not has_left_point(SL, _), has_right_point(SL, _).

#program stop_line_points_on_polylines.
stop_line_points_on_polylines(ID) :- lanelet(L), polyline(P), stop_line(SL), has_lanelet_id(L, ID), has_left_polyline(L, P), has_vertices(P, V), has_stop_line(L, SL),
                                     has_left_point(SL, X), connection_thresh(T), @is_point_on_polyline(X, V, T) == "False".
stop_line_points_on_polylines(ID) :- lanelet(L), polyline(P), stop_line(SL), has_lanelet_id(L, ID), has_right_polyline(L, P), has_vertices(P, V), has_stop_line(L, SL),
                                     has_right_point(SL, X), connection_thresh(T), @is_point_on_polyline(X, V, T) == "False".