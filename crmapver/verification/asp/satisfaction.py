import ast
import math
import os
import pathlib
from typing import List

import numpy as np
from clingo import Control, Model, Symbol, String
from shapely import GEOSException

from crmapver.config import VerificationParams
from crmapver.verification.satisfaction import VerificationChecker, InvalidStates
from shapely.geometry import LineString, Point, Polygon
from similaritymeasures import similaritymeasures

from crmapver.verification.asp.mapping import ASPMapping
from crmapver.verification.formula_ids import LaneletFormulaID, TrafficSignFormulaID, \
    TrafficLightFormulaID, IntersectionFormulaID, FormulaID


class ASPVerificationChecker(VerificationChecker):
    """
    The class is responsible for solving the desired ASP formulas. The inferred invalid states are extracted and
    returned.
    """

    def __init__(self, mapping: ASPMapping, formula_ids: List[FormulaID] = None):
        super().__init__(mapping, formula_ids)

        self._all_f_ids = []
        f_types = [LaneletFormulaID, TrafficSignFormulaID, TrafficLightFormulaID, IntersectionFormulaID]
        for f_type in f_types:
            self._all_f_ids += [f_id for f_id in f_type]

        if formula_ids is None:
            self._desired_f_ids = self._all_f_ids
        else:
            self._desired_f_ids = formula_ids

        self._invalid_states = None

    def check_validity(self, config: VerificationParams, manager_results: List[InvalidStates]):
        """
        Checks the validity of a map. An ASP program is constructed which infers invalid states in case of
        violating the formulas.

        :param config: Verification config parameters.
        :param manager_results: Manager list.
        """
        self._invalid_states = {}

        lp = self._build_lp(config)

        ctl = Control()
        ctl.add("program", [], lp)

        parts = [("base", [])] + [(str(f.value), []) for f in self._desired_f_ids]

        ctl.ground(parts=parts, context=Context())

        ctl.solve(on_model=self._on_model)

        manager_results.append(self._invalid_states)

    def _on_model(self, m: Model):
        """
        Handles the model after solving the ASP program. The inferred atoms representing the invalid states are
        filtered and stores with corresponding formula ID.

        :param m: Model.
        """
        for atom in m.symbols(atoms=True):
            atom_f_id = None
            for f_id in self._all_f_ids:
                if atom.name == f_id.value:
                    atom_f_id = f_id
            if atom_f_id is None:
                continue

            arguments = []
            for arg in atom.arguments:
                arg = arg.string
                if str.isnumeric(arg):
                    arg = int(arg)
                else:
                    arg = hash(arg)
                arguments.append(arg)

            arguments = tuple(arguments)
            if atom_f_id in self._invalid_states.keys():
                locations = self._invalid_states[atom_f_id]
                self._invalid_states.update({atom_f_id: locations + [arguments]})
            else:
                self._invalid_states.update({atom_f_id: [arguments]})

    def _build_lp(self, config: VerificationParams) -> str:
        """
        Builds the final ASP program for inferring invalid states. The formulas for lanelets, traffic signs,
        traffic lights, and intersections are loaded separately from different files and finally combined.
        It is the same for all used definitions of atoms.

        :param config: Verification config parameters.
        :return: Logic program.
        """
        facts = self._mapping.facts

        lp = "% Logic Program for CommonRoad Map Verification\n\n"

        atom_d_lp = open(config.formula_manager.content_dir + "atom_definitions.lp", "r").read()

        lp += atom_d_lp + "\n\n"

        lp += "% Instances\n#program base.\n\n"

        for predicate, instances in facts.items():
            for instance in instances:
                lp += instance
            lp += "\n\n"

        lp += config.formula_manager.f_lps

        return lp


class Context:

    @staticmethod
    def are_similar_points(p1: Symbol, p2: Symbol, thresh: Symbol) -> Symbol:
        """
        Checks the similarity of two points.

        :param p1: First point symbol.
        :param p2: Second point symbol.
        :param thresh: Threshold symbol.
        :return: Boolean symbol indicates whether the two points are similar.
        """
        p1 = ast.literal_eval(p1.string)
        p2 = ast.literal_eval(p2.string)
        thresh = float(str(thresh.string))

        df = np.linalg.norm(np.array([p1]) - np.array([p2]))
        return String('True' if df < thresh else 'False')

    @staticmethod
    def are_similar_polylines(v1: Symbol, v2: Symbol, same_dr: Symbol, thresh: Symbol) -> Symbol:
        """
        Checks the similarity of two polylines.

        :param v1: First polyline symbol.
        :param v2: Second polyline symbol.
        :param same_dr: Boolean indicates whether the same direction is to be considered.
        :param thresh: Threshold symbol.
        :return: Boolean symbol indicates whether the two polylines are similar.
        """
        v1 = np.array(ast.literal_eval(v1.string))
        v2 = np.array(ast.literal_eval(v2.string))
        thresh = float(str(thresh.string))

        if same_dr.string == 'False':
            v1 = np.flip(v1, 0)
        if not ((np.linalg.norm(v1[0] - v2[0]) < 0.1) and (np.linalg.norm((v1[-1] - v2[-1]) < 0.1)) or
                ((np.linalg.norm(v1[0] - v2[0]) < 0.1) and (np.linalg.norm(v1[-1] - v2[-1]) < 0.1))):
            return String('False')
        df = similaritymeasures.frechet_dist(v1, v2)
        return String('True' if df < thresh else 'False')

    @staticmethod
    def are_mutually_intersected_polylines(v1: Symbol, v2: Symbol) -> Symbol:
        """
        Checks whether two polylines are mutually intersected.

        :param v1: First polyline symbol.
        :param v2: Second polyline symbol.
        :return: Boolean symbol indicates whether the two polylines are mutually intersected.
        """
        vs = [np.array(ast.literal_eval(v.string)) for v in [v1, v2]]

        result = LineString(vs[0]).intersection(LineString(vs[1]))

        return String('True' if not result.is_empty else 'False')

    @staticmethod
    def are_mutually_intersected_lanelets(v1: Symbol, v2: Symbol, v3: Symbol, v4: Symbol) -> Symbol:
        """
        Checks whether two lanelets are mutually intersected. One lanelet consists of two polylines.

        :param v1: First polyline symbol of first lanelet.
        :param v2: Second polyline symbol of first lanelet.
        :param v3: First polyline symbol of second lanelet.
        :param v4: Second polyline symbol of second lanelet.
        :return: Boolean symbol indicates whether the two lanelets are mutually intersected.
        """
        vs = [np.array(ast.literal_eval(v.string)) for v in [v1, v2, v3, v4]]

        polygons = [Polygon(np.concatenate((vs[i + 1], np.flip(vs[i], 0)))) for i in range(0, 3, 2)]

        try:
            result = polygons[0].intersection(polygons[1])
        except GEOSException:
            return String('False')

        return String('True' if not result.is_empty else 'False')

    @staticmethod
    def is_self_intersected_polyline(v: Symbol) -> Symbol:
        """
        Checks whether the polyline is self intersected.

        :param v: Polyline symbol.
        :return: Boolean symbol indicates whether the polyline is self intersected.
        """
        v = np.array(ast.literal_eval(v.string))

        return String('True' if not LineString(v).is_simple else 'False')

    @staticmethod
    def is_point_on_polyline(p: Symbol, v: Symbol, thresh: Symbol) -> Symbol:
        """
        Checks whether the point lies on the polyline.

        :param p: Point symbol.
        :param v: Polyline symbol.
        :param thresh: Threshold symbol.
        :return: Boolean symbol indicates whether the point lies on the polyline.
        """
        p = ast.literal_eval(p.string)
        v = np.array(ast.literal_eval(v.string))
        thresh = float(str(thresh.string))

        dist = LineString(v).distance(Point(p))

        return String('True' if dist < thresh else 'False')

    @staticmethod
    def is_point_close_to_lanelet(p: Symbol, v1: Symbol, v2: Symbol, thresh: Symbol) -> Symbol:
        """
        Checks whether the point lies close to the lanelet.

        :param p: Point symbol.
        :param v1: First polyline symbol.
        :param v2: Second polyline symbol.
        :param thresh: Threshold symbol.
        :return: Boolean indicates whether the point lies close to the lanelet.
        """
        p = ast.literal_eval(p.string)
        v1 = np.array(ast.literal_eval(v1.string))
        v2 = np.array(ast.literal_eval(v2.string))
        thresh = float(thresh.number)

        dist = Polygon(np.concatenate((v1, v2[::-1]), axis=0)).distance(Point(p))

        return String('True' if dist < thresh else 'False')

    @staticmethod
    def get_outgoing_type(v1: Symbol, v2: Symbol) -> Symbol:
        """
        Computes the type of outgoing lanelet.

        :param v1: Center polyline symbol of first lanelet.
        :param v2: Center polyline symbol of second lanelet.
        :return: Type of outgoing symbol.
        """
        v1 = np.array(ast.literal_eval(v1.string))
        v2 = np.array(ast.literal_eval(v2.string))

        if len(v1) > 4:
            v1 = [v1[-4], v1[-1]]
        else:
            v1 = [v1[0], v1[-1]]

        if len(v2) > 4:
            v2 = [v2[0], v2[4]]
        else:
            v2 = [v2[0], v2[-1]]

        angle_1 = math.atan2(v1[0][1] - v1[1][1], v1[0][0] - v1[1][0])
        angle_2 = math.atan2(v2[0][1] - v2[1][1], v2[0][0] - v2[1][0])
        angle_deg = (angle_1 - angle_2) * 360 / (2 * math.pi)

        if angle_deg < 0.:
            angle_deg += 360

        straight_thresh = 35.

        if straight_thresh < angle_deg <= 180.:
            outgoing_type = 'Right'
        elif 180. < angle_deg <= 360. - straight_thresh:
            outgoing_type = 'Left'
        else:
            outgoing_type = 'Straight'

        return String(outgoing_type)
