from copy import deepcopy
from multiprocessing.pool import ThreadPool
from typing import Tuple, List, Union

import z3

from crmapver.config import VerificationParams
from crmapver.verification.satisfaction import VerificationChecker, InvalidStates
from z3 import Solver, Or, Not, BoolRef, ModelRef, sat, Context, main_ctx, ExprRef, simplify, IntSort, RealSort, \
    BoolSort

from crmapver.verification.z3.formulas.formula_pool import FormulaPool
from crmapver.verification.formula_ids import LaneletFormulaID, TrafficSignFormulaID, \
    TrafficLightFormulaID, IntersectionFormulaID, FormulaID
from crmapver.verification.z3.formulas.intersection_formulas import IntersectionFormulaPool
from crmapver.verification.z3.formulas.lanelet_formulas import LaneletFormulaPool
from crmapver.verification.z3.mapping import Z3Mapping, LaneletSort, TrafficSignSort, \
    TrafficLightSort, IntersectionSort, TrafficSignElementSort, IncomingElementSort
from crmapver.verification.z3.formulas.traffic_light_formulas import TrafficLightFormulaPool
from crmapver.verification.z3.formulas.traffic_sign_formulas import TrafficSignFormulaPool


class Z3VerificationChecker(VerificationChecker):

    def __init__(self, mapping: Z3Mapping, formula_ids: List[FormulaID] = None):
        super().__init__(mapping, formula_ids)

        self._formula_pool = {}

        f_funcs = {}
        f_pools = [LaneletFormulaPool, TrafficSignFormulaPool, TrafficLightFormulaPool, IntersectionFormulaPool]
        for f_pool in f_pools:
            f_pool = f_pool(mapping)
            func_names = [func for func in dir(f_pool) if callable(getattr(f_pool, func))]
            abc_func_names = [func for func in dir(FormulaPool) if callable(getattr(FormulaPool, func))]
            f_names = [f_name.value for f_name in formula_ids] if formula_ids is not None else None
            for func_name in func_names:
                if func_name not in abc_func_names and not func_name.startswith('__') and not func_name.endswith('__'):
                    if formula_ids is not None and func_name not in f_names:
                        continue

                    func = getattr(f_pool, func_name)

                    f_id_pools = [LaneletFormulaID, TrafficSignFormulaID, TrafficLightFormulaID, IntersectionFormulaID]
                    for f_id_pool in f_id_pools:
                        f_values = [f_id.value for f_id in f_id_pool]
                        if func_name in f_values:
                            f_funcs.update({f_id_pool(func_name): func})

        for f_id, f_func in f_funcs.items():
            f, refs = f_func()

            self._formula_pool.update({f_id: (f, refs)})

    def check_validity(self, config: VerificationParams, manager_results: List[InvalidStates]):
        """
        Checks the network for validity.

        :param config: Verification config parameters.
        :param manager_results: List where invalid states are stored.
        """
        pool = ThreadPool(config.num_threads)

        results = []
        for f_id, formula in self._formula_pool.items():
            f_expr, f_refs = formula

            ctx = Context()
            f_expr = deepcopy(f_expr).translate(ctx)
            f_refs = [deepcopy(f_ref).translate(ctx) for f_ref in f_refs]

            results.append(pool.apply_async(Z3VerificationChecker._solve_ctx, [f_id, f_expr, f_refs, ctx]))

        pool.close()
        pool.join()

        invalid_states = {}
        for s_id, s_models, refs in [result.get() for result in results]:
            if not s_models:
                continue

            m_values = []
            for model in s_models:
                values = []
                for ref in refs:
                    ref_v = deepcopy(model[ref]).translate(z3.main_ctx())
                    value = self._extract_value(ref_v)
                    values.append(value)
                m_values.append(tuple(values))

            invalid_states.update({s_id: m_values})

        manager_results.append(invalid_states)

    def translate_ctx(self) -> Context:
        ctx = Context()
        for f_id, formula in self._formula_pool.items():
            f_expr, f_refs = formula

            f_expr = deepcopy(f_expr).translate(ctx)
            f_refs = [deepcopy(f_ref).translate(ctx) for f_ref in f_refs]

            self._formula_pool.update({f_id: (f_expr, f_refs)})

        return ctx

    @staticmethod
    def _solve_ctx(f_id: FormulaID, formula: BoolRef, refs: List[ExprRef], ctx: Context) -> \
            Tuple[FormulaID, List[ModelRef], List[ExprRef]]:
        """
        Computes all models of a formula in the environment of a given context.

        :param f_id: Identification of formula
        :param formula: Logical formula
        :param refs: Reference to unbound variables
        :param ctx: Context node
        :return: Satisfied models
        """
        assert formula.ctx == ctx
        assert formula.ctx != main_ctx()

        models = []

        s = Solver(ctx=ctx)
        s.add(Not(formula))
        while s.check() == sat:
            m = s.model()
            models.append(m)

            model_assignments = [asm != m[asm] for asm in refs]
            s.add(Or(model_assignments, ctx))

        return f_id, models, refs

    @staticmethod
    def _extract_value(ref: ExprRef) -> Union[int, float, bool]:
        """
        Extracts value from an unknown Z3 type.

        :param ref: Z3 value
        :return: Extracted value
        """
        ref_sort = ref.sort()
        val = None
        if ref_sort == LaneletSort():
            val = simplify(LaneletSort().lanelet_id(ref)).as_long()
        elif ref_sort == TrafficSignSort():
            val = simplify(TrafficSignSort().traffic_sign_id(ref)).as_long()
        elif ref_sort == TrafficLightSort():
            val = simplify(TrafficLightSort().traffic_light_id(ref)).as_long()
        elif ref_sort == IntersectionSort():
            val = simplify(IntersectionSort().intersection_id(ref)).as_long()
        elif ref_sort == TrafficSignElementSort():
            val = simplify(TrafficSignElementSort().traffic_sign_element_id(ref)).as_long()
        elif ref_sort == IncomingElementSort():
            val = simplify(IncomingElementSort().incoming_element_id(ref)).as_long()
        elif ref_sort == IntSort():
            val = simplify(ref).as_long()
        elif ref_sort == RealSort():
            val = float(simplify(ref).as_decimal(prec=8).replace('?', ''))
        elif ref_sort == BoolSort():
            val = bool(simplify(ref))

        return val
