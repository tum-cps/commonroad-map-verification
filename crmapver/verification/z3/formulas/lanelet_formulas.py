from z3 import Implies, ForAll, And, Exists, Int, Contains, Unit, Not, Length, Or, Extract, If

from crmapver.verification.z3.mapping import Z3Mapping, LaneletSort, PolylineSort, \
    TrafficSignSort, TrafficLightSort, StopLineSort
from crmapver.verification.z3.formulas.formula_pool import Formula, FormulaPool
from crmapver.verification.z3.formulas.subformulas import SubformulaPool


class LaneletFormulaPool(FormulaPool):
    """
    This class contains a pool of lanelet formulas.
    """

    def __init__(self, mapping: Z3Mapping):
        super(LaneletFormulaPool, self).__init__(mapping)

        self._sf = SubformulaPool(mapping)

    def unique_id_la(self) -> Formula:
        """
        Each lanelet has an unique ID.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)

        formula = Implies(self._sf.is_lanelet(lanelet_0),
                          ForAll([lanelet_1],
                                 Implies(And(self._sf.is_lanelet(lanelet_1), lanelet_0 != lanelet_1),
                                         LaneletSort().lanelet_id(lanelet_0) != LaneletSort().lanelet_id(lanelet_1))))

        return formula, [lanelet_0]

    def same_vertices_size(self) -> Formula:
        """
        Left and right polyline of lanelet have the same number of vertices.

        :return: Formula.
        """
        lanelet = self.create_consts(LaneletSort())

        formula = Implies(self._sf.is_lanelet(lanelet),
                          PolylineSort().size(LaneletSort().left_polyline(lanelet)) == PolylineSort().size(
                              LaneletSort().right_polyline(lanelet)))

        return formula, [lanelet]

    def vertices_more_than_one(self) -> Formula:
        """
        Polyline is constructed of at least two vertices.

        :return: Formula.
        """
        lanelet = self.create_consts(LaneletSort())

        formula = Implies(self._sf.is_lanelet(lanelet),
                          And(PolylineSort().size(LaneletSort().left_polyline(lanelet)) > 1,
                              PolylineSort().size(LaneletSort().right_polyline(lanelet)) > 1))

        return formula, [lanelet]

    def existence_left_adj(self) -> Formula:
        """
        For the given ID representing the left adjacency there exists a lanelet with the same ID.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)

        formula = Implies(And(self._sf.is_lanelet(lanelet_0), LaneletSort().left_adj(lanelet_0) >= 0),
                          Exists([lanelet_1], And(self._sf.is_lanelet(lanelet_1),
                                                  LaneletSort().left_adj(lanelet_0) == LaneletSort().lanelet_id(
                                                      lanelet_1))))

        return formula, [lanelet_0]

    def existence_right_adj(self) -> Formula:
        """
        For the given ID representing the right adjacency there exists a lanelet with the same ID.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)

        formula = Implies(And(self._sf.is_lanelet(lanelet_0), LaneletSort().right_adj(lanelet_0) >= 0),
                          Exists([lanelet_1],
                                 And(self._sf.is_lanelet(lanelet_1),
                                     LaneletSort().right_adj(lanelet_0) == LaneletSort().lanelet_id(lanelet_1))))

        return formula, [lanelet_0]

    def existence_predecessor(self) -> Formula:
        """
        For each of the given IDs representing all predecessor there exists a lanelet with the same ID.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)
        predecessor_id = Int('predecessor_id')

        formula = Implies(And(self._sf.is_lanelet(lanelet_0),
                              Contains(LaneletSort().predecessor(lanelet_0), Unit(predecessor_id))),
                          Exists([lanelet_1], And(self._sf.is_lanelet(lanelet_1),
                                                  predecessor_id == LaneletSort().lanelet_id(lanelet_1))))

        return formula, [lanelet_0, predecessor_id]

    def existence_successor(self) -> Formula:
        """
        For each of the given IDs representing all successors there exists a lanelet with the same ID.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)
        successor_id = Int('successor_id')

        formula = Implies(And(self._sf.is_lanelet(lanelet_0),
                              Contains(LaneletSort().successor(lanelet_0), Unit(successor_id))),
                          Exists([lanelet_1], And(self._sf.is_lanelet(lanelet_1),
                                                  successor_id == LaneletSort().lanelet_id(lanelet_1))))

        return formula, [lanelet_0, successor_id]

    def connections_predecessor(self) -> Formula:
        """
        The connection points of lanelet and each corresponding predecessor are equal.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)

        formula = Implies(And(self._sf.is_lanelet(lanelet_0), self._sf.is_lanelet(lanelet_1),
                              Contains(LaneletSort().predecessor(lanelet_0), Unit(LaneletSort().lanelet_id(
                                  lanelet_1)))),
                          self._sf.are_predecessor_connections(lanelet_0, lanelet_1, True))

        return formula, [lanelet_0, lanelet_1]

    def connections_successor(self) -> Formula:
        """
        The connection points of lanelet and each corresponding successor are equal.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)

        formula = Implies(And(self._sf.is_lanelet(lanelet_0), self._sf.is_lanelet(lanelet_1),
                              Contains(LaneletSort().successor(lanelet_0), Unit(LaneletSort().lanelet_id(
                                  lanelet_1)))),
                          self._sf.are_successor_connections(lanelet_0, lanelet_1, True))

        return formula, [lanelet_0, lanelet_1]

    def polylines_left_same_dir_parallel_adj(self) -> Formula:
        """
        The left polyline of lanelet and corresponding right polyline of left parallel adjacency with
        same direction are equal.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)

        formula = Implies(And(self._sf.is_lanelet(lanelet_0), self._sf.is_lanelet(lanelet_1),
                              LaneletSort().left_adj(lanelet_0) == LaneletSort().lanelet_id(lanelet_1),
                              self._sf.is_left_adj_type(lanelet_0, 'Parallel'),
                              LaneletSort().adj_left_same_dir(lanelet_0)),
                          PolylineSort().vertices(LaneletSort().left_polyline(lanelet_0)) == PolylineSort().vertices(
                              LaneletSort().right_polyline(lanelet_1)))

        return formula, [lanelet_0, lanelet_1]

    def polylines_left_opposite_dir_parallel_adj(self) -> Formula:
        """
        The left polyline of lanelet and corresponding left polyline of left parallel adjacency with
        opposite direction are equal.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)

        formula = Implies(And(self._sf.is_lanelet(lanelet_0), self._sf.is_lanelet(lanelet_1),
                              LaneletSort().left_adj(lanelet_0) == LaneletSort().lanelet_id(lanelet_1),
                              self._sf.is_left_adj_type(lanelet_0, 'Parallel'),
                              Not(LaneletSort().adj_left_same_dir(lanelet_0))),
                          PolylineSort().vertices(LaneletSort().left_polyline(lanelet_0)) == PolylineSort().vertices(
                              LaneletSort().left_polyline(lanelet_1)))

        return formula, [lanelet_0, lanelet_1]

    def polylines_right_same_dir_parallel_adj(self) -> Formula:
        """
        The right polyline of lanelet and corresponding left polyline of right parallel adjacency with
        same direction are equal.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)

        formula = Implies(And(self._sf.is_lanelet(lanelet_0), self._sf.is_lanelet(lanelet_1),
                              LaneletSort().right_adj(lanelet_0) == LaneletSort().lanelet_id(lanelet_1),
                              self._sf.is_right_adj_type(lanelet_0, 'Parallel'),
                              LaneletSort().adj_right_same_dir(lanelet_0)),
                          PolylineSort().vertices(LaneletSort().right_polyline(lanelet_0)) == PolylineSort().vertices(
                              LaneletSort().left_polyline(lanelet_1)))

        return formula, [lanelet_0, lanelet_1]

    def polylines_right_opposite_dir_parallel_adj(self) -> Formula:
        """
        The right polyline of lanelet and corresponding right polyline of right parallel adjacency with
        opposite direction are equal.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)

        formula = Implies(And(self._sf.is_lanelet(lanelet_0), self._sf.is_lanelet(lanelet_1),
                              LaneletSort().right_adj(lanelet_0) == LaneletSort().lanelet_id(lanelet_1),
                              self._sf.is_right_adj_type(lanelet_0, 'Parallel'),
                              Not(LaneletSort().adj_right_same_dir(lanelet_0))),
                          PolylineSort().vertices(LaneletSort().right_polyline(lanelet_0)) == PolylineSort()
                          .vertices(LaneletSort().right_polyline(lanelet_1)))

        return formula, [lanelet_0, lanelet_1]

    def connections_left_merging_adj(self) -> Formula:
        """
        The connection points of lanelet and corresponding left merging adjacency are equal.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)

        formula = Implies(And(self._sf.is_lanelet(lanelet_0), self._sf.is_lanelet(lanelet_1),
                              self._sf.is_left_adj_type(lanelet_0, 'Merge'),
                              LaneletSort().left_adj(lanelet_0) == LaneletSort().lanelet_id(lanelet_1)),
                          self._sf.are_left_merging_adj_connections(lanelet_0, lanelet_1, True))

        return formula, [lanelet_0, lanelet_1]

    def connections_right_merging_adj(self) -> Formula:
        """
        The connection points of lanelet and corresponding right merging adjacency are equal.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)

        formula = Implies(And(self._sf.is_lanelet(lanelet_0), self._sf.is_lanelet(lanelet_1),
                              self._sf.is_right_adj_type(lanelet_0, 'Merge'),
                              LaneletSort().right_adj(lanelet_0) == LaneletSort().lanelet_id(lanelet_1)),
                          self._sf.are_right_merging_adj_connections(lanelet_0, lanelet_1, True))

        return formula, [lanelet_0, lanelet_1]

    def connections_left_forking_adj(self) -> Formula:
        """
        The connection points of lanelet and corresponding left forking adjacency are equal.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)

        formula = Implies(And(self._sf.is_lanelet(lanelet_0), self._sf.is_lanelet(lanelet_1),
                              self._sf.is_left_adj_type(lanelet_0, 'Fork'),
                              LaneletSort().left_adj(lanelet_0) == LaneletSort().lanelet_id(lanelet_1)),
                          self._sf.are_left_forking_adj_connections(lanelet_0, lanelet_1, True))

        return formula, [lanelet_0, lanelet_1]

    def connections_right_forking_adj(self) -> Formula:
        """
        The connection points of lanelet and corresponding right forking adjacency are equal.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)

        formula = Implies(And(self._sf.is_lanelet(lanelet_0), self._sf.is_lanelet(lanelet_1),
                              self._sf.is_right_adj_type(lanelet_0, 'Fork'),
                              LaneletSort().right_adj(lanelet_0) == LaneletSort().lanelet_id(lanelet_1)),
                          self._sf.are_right_forking_adj_connections(lanelet_0, lanelet_1, True))

        return formula, [lanelet_0, lanelet_1]

    def potential_successor(self) -> Formula:
        """
        Each successor of lanelet is correctly referenced by ID.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)

        formula = Implies(And(self._sf.is_lanelet(lanelet_0), self._sf.is_lanelet(lanelet_1),
                              Not(Contains(LaneletSort().successor(lanelet_0), Unit(LaneletSort().lanelet_id(
                                  lanelet_1))))),
                          Not(self._sf.are_successor_connections(lanelet_0, lanelet_1, True)))

        return formula, [lanelet_0, lanelet_1]

    def potential_predecessor(self) -> Formula:
        """
        Each predecessor of lanelet is correctly referenced by ID.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)

        formula = Implies(And(self._sf.is_lanelet(lanelet_0), self._sf.is_lanelet(lanelet_1),
                              Not(Contains(LaneletSort().predecessor(lanelet_0), Unit(LaneletSort().lanelet_id(
                                  lanelet_1))))),
                          Not(self._sf.are_predecessor_connections(lanelet_0, lanelet_1, True)))

        return formula, [lanelet_0, lanelet_1]

    def potential_left_same_dir_parallel_adj(self) -> Formula:
        """
        The left parallel adjacency with same direction of lanelet is correctly referenced by ID.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)

        formula = Implies(And(self._sf.is_lanelet(lanelet_0), self._sf.is_lanelet(lanelet_1), lanelet_0 != lanelet_1,
                              Not(And(LaneletSort().left_adj(lanelet_0) == LaneletSort().lanelet_id(lanelet_1),
                                      LaneletSort().adj_left_same_dir(lanelet_0)))),
                          PolylineSort().vertices(LaneletSort().left_polyline(lanelet_0)) != PolylineSort().vertices(
                              LaneletSort().right_polyline(lanelet_1)))

        return formula, [lanelet_0, lanelet_1]

    def potential_left_opposite_dir_parallel_adj(self) -> Formula:
        """
        The left parallel adjacency with opposite direction of lanelet is correctly referenced by ID.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)

        formula = Implies(And(self._sf.is_lanelet(lanelet_0), self._sf.is_lanelet(lanelet_1), lanelet_0 != lanelet_1,
                              Not(And(LaneletSort().left_adj(lanelet_0) == LaneletSort().lanelet_id(lanelet_1),
                                      Not(LaneletSort().adj_left_same_dir(lanelet_0))))),
                          PolylineSort().vertices(LaneletSort().left_polyline(lanelet_0)) != PolylineSort().vertices(
                              LaneletSort().left_polyline(lanelet_1)))

        return formula, [lanelet_0, lanelet_1]

    def potential_right_same_dir_parallel_adj(self) -> Formula:
        """
        The right parallel adjacency with same direction of lanelet is correctly referenced by ID.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)

        formula = Implies(And(self._sf.is_lanelet(lanelet_0), self._sf.is_lanelet(lanelet_1), lanelet_0 != lanelet_1,
                              Not(And(LaneletSort().right_adj(lanelet_0) == LaneletSort().lanelet_id(lanelet_1),
                                      LaneletSort().adj_right_same_dir(lanelet_0)))),
                          PolylineSort().vertices(LaneletSort().right_polyline(lanelet_0)) != PolylineSort().vertices(
                              LaneletSort().left_polyline(lanelet_1)))

        return formula, [lanelet_0, lanelet_1]

    def potential_right_opposite_dir_parallel_adj(self) -> Formula:
        """
        The right parallel adjacency with opposite direction of lanelet is correctly referenced by ID.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)

        formula = Implies(And(self._sf.is_lanelet(lanelet_0), self._sf.is_lanelet(lanelet_1), lanelet_0 != lanelet_1,
                              Not(And(LaneletSort().right_adj(lanelet_0) == LaneletSort().lanelet_id(lanelet_1),
                                      Not(LaneletSort().adj_right_same_dir(lanelet_0))))),
                          PolylineSort().vertices(LaneletSort().right_polyline(lanelet_0)) != PolylineSort().vertices(
                              LaneletSort().right_polyline(lanelet_1)))

        return formula, [lanelet_0, lanelet_1]

    def potential_left_merging_adj(self) -> Formula:
        """
        Each left merging adjacency of lanelet is correctly referenced by ID.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)

        formula = Implies(And(self._sf.is_lanelet(lanelet_0), self._sf.is_lanelet(lanelet_1), lanelet_0 != lanelet_1,
                              LaneletSort().left_adj(lanelet_0) != LaneletSort().lanelet_id(lanelet_1)),
                          Not(self._sf.are_left_merging_adj_connections(lanelet_0, lanelet_1, False)))

        return formula, [lanelet_0, lanelet_1]

    def potential_right_merging_adj(self) -> Formula:
        """
        Each right merging adjacency of lanelet is correctly referenced by ID.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)

        formula = Implies(And(self._sf.is_lanelet(lanelet_0), self._sf.is_lanelet(lanelet_1), lanelet_0 != lanelet_1,
                              LaneletSort().right_adj(lanelet_0) != LaneletSort().lanelet_id(lanelet_1)),
                          Not(self._sf.are_right_merging_adj_connections(lanelet_0, lanelet_1, False)))

        return formula, [lanelet_0, lanelet_1]

    def potential_left_forking_adj(self) -> Formula:
        """
        Each left forking adjacency of lanelet is correctly referenced by ID.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)

        formula = Implies(And(self._sf.is_lanelet(lanelet_0), self._sf.is_lanelet(lanelet_1), lanelet_0 != lanelet_1,
                              LaneletSort().left_adj(lanelet_0) != LaneletSort().lanelet_id(lanelet_1)),
                          Not(self._sf.are_left_forking_adj_connections(lanelet_0, lanelet_1, False)))

        return formula, [lanelet_0, lanelet_1]

    def potential_right_forking_adj(self) -> Formula:
        """
        Each right forking adjacency of lanelet is correctly referenced by ID.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)

        formula = Implies(And(self._sf.is_lanelet(lanelet_0), self._sf.is_lanelet(lanelet_1), lanelet_0 != lanelet_1,
                              LaneletSort().right_adj(lanelet_0) != LaneletSort().lanelet_id(lanelet_1)),
                          Not(self._sf.are_right_forking_adj_connections(lanelet_0, lanelet_1, False)))

        return formula, [lanelet_0, lanelet_1]

    def non_predecessor_as_successor(self) -> Formula:
        """
        No predecessor is referenced by a lanelet as successor.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)

        formula = Implies(And(self._sf.is_lanelet(lanelet_0), self._sf.is_lanelet(lanelet_1), lanelet_0 != lanelet_1,
                              Contains(LaneletSort().successor(lanelet_0), Unit(LaneletSort().lanelet_id(lanelet_1))),
                              Not(Contains(LaneletSort().predecessor(lanelet_0),
                                           Unit(LaneletSort().lanelet_id(lanelet_1))))),
                          Not(self._sf.are_predecessor_connections(lanelet_0, lanelet_1, False)))

        return formula, [lanelet_0, lanelet_1]

    def non_successor_as_predecessor(self) -> Formula:
        """
        No successor is referenced by a lanelet as predecessor.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)

        formula = Implies(And(self._sf.is_lanelet(lanelet_0), self._sf.is_lanelet(lanelet_1), lanelet_0 != lanelet_1,
                              Contains(LaneletSort().predecessor(lanelet_0), Unit(
                                      LaneletSort().lanelet_id(lanelet_1))),
                              Not(Contains(LaneletSort().successor(lanelet_0),
                                           Unit(LaneletSort().lanelet_id(lanelet_1))))),
                          Not(self._sf.are_successor_connections(lanelet_0, lanelet_1, False)))

        return formula, [lanelet_0, lanelet_1]

    def polylines_intersection(self) -> Formula:
        """
        The left and right polyline of lanelet do not intersect each other.

        :return: Formula.
        """
        lanelet = self.create_consts(LaneletSort())

        formula = Implies(self._sf.is_lanelet(lanelet), Not(LaneletSort().is_polylines_intersection(lanelet)))

        return formula, [lanelet]

    def left_self_intersection(self) -> Formula:
        """
        The left polyline of lanelet does not intersect itself.

        :return: Formula.
        """
        lanelet = self.create_consts(LaneletSort())

        formula = Implies(self._sf.is_lanelet(lanelet),
                          Not(PolylineSort().is_self_intersection(LaneletSort().left_polyline(lanelet))))

        return formula, [lanelet]

    def right_self_intersection(self) -> Formula:
        """
        The right polyline of lanelet does not intersect itself.

        :return: Formula.
        """
        lanelet = self.create_consts(LaneletSort())

        formula = Implies(self._sf.is_lanelet(lanelet),
                          Not(PolylineSort().is_self_intersection(LaneletSort().right_polyline(lanelet))))

        return formula, [lanelet]

    def lanelet_types_combination(self) -> Formula:
        """
        The combination of the lanelet types are allowed.

        :return: Formula.
        """
        lanelet = self.create_consts(LaneletSort())
        type_0 = Int('type_0')
        type_1 = Int('type_1')

        formula = Implies(And(self._sf.is_lanelet(lanelet), Contains(LaneletSort().types(lanelet), Unit(type_0)),
                              Contains(LaneletSort().types(lanelet), Unit(type_1))),
                          self._sf.valid_type_combination(type_0, type_1))

        return formula, [lanelet, type_0, type_1]

    def non_followed_composable_lanelets(self) -> Formula:
        """
        The only successor is different to lanelet and are not composable. Or one of the topological left and right
        adjacencies and its corresponding successor/predecessor are not composable.

        :return: Formula.
        """
        lanelet, successor, lanelet_adj, successor_adj = self.create_consts(LaneletSort(), 4)
        depth = Int('depth')

        formula = Implies(And(self._sf.is_lanelet(lanelet), self._sf.is_lanelet(successor),
                              Contains(LaneletSort().successor(lanelet), Unit(LaneletSort().lanelet_id(successor))),
                              Length(LaneletSort().left_topological_adjs(lanelet)) == Length(
                                      LaneletSort().left_topological_adjs(successor)),
                              Length(LaneletSort().right_topological_adjs(lanelet)) == Length(
                                      LaneletSort().right_topological_adjs(successor))),
                          Or(Not(self._sf.are_followed_composable(lanelet, successor)),
                             Exists([depth, lanelet_adj, successor_adj],
                                    And(depth >= 0, depth < Length(LaneletSort().left_topological_adjs(lanelet)),
                                        self._sf.is_lanelet(lanelet_adj), self._sf.is_lanelet(successor_adj),
                                        Extract(LaneletSort().left_topological_adjs(lanelet), depth, 1) == Unit(
                                                LaneletSort().lanelet_id(lanelet_adj)),
                                        Extract(LaneletSort().left_topological_adjs(successor), depth, 1) == Unit(
                                                LaneletSort().lanelet_id(successor_adj)),
                                        If(Contains(LaneletSort().successor(lanelet_adj), Unit(
                                                LaneletSort().lanelet_id(successor_adj))),
                                           Not(self._sf.are_followed_composable(lanelet_adj, successor_adj)),
                                           Not(self._sf.are_followed_composable(successor_adj, lanelet_adj))))),
                             Exists([depth, lanelet_adj, successor_adj],
                                    And(depth >= 0, depth < Length(LaneletSort().right_topological_adjs(lanelet)),
                                        self._sf.is_lanelet(lanelet_adj), self._sf.is_lanelet(successor_adj),
                                        Extract(LaneletSort().right_topological_adjs(lanelet), depth, 1) == Unit(
                                            LaneletSort().lanelet_id(lanelet_adj)),
                                        Extract(LaneletSort().right_topological_adjs(successor), depth, 1) == Unit(
                                            LaneletSort().lanelet_id(successor_adj)),
                                        If(Contains(LaneletSort().successor(lanelet_adj), Unit(
                                                LaneletSort().lanelet_id(successor_adj))),
                                           Not(self._sf.are_followed_composable(lanelet_adj, successor_adj)),
                                           Not(self._sf.are_followed_composable(successor_adj, lanelet_adj)))))))

        return formula, [lanelet, successor]

    def referenced_intersecting_lanelets(self) -> Formula:
        """
        A lanelet is intersected by a lanelet which is referenced as left or right adjacency, successor,
        and predecessor.

        :return: Formula.
        """
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)

        formula = Implies(And(self._sf.is_lanelet(lanelet_0), self._sf.is_lanelet(lanelet_1),
                          Contains(LaneletSort().intersecting_lanelets(lanelet_0), Unit(
                                  LaneletSort().lanelet_id(lanelet_1)))),
                          Or(LaneletSort().left_adj(lanelet_0) == LaneletSort().lanelet_id(lanelet_1),
                             LaneletSort().right_adj(lanelet_0) == LaneletSort().lanelet_id(lanelet_1),
                             Contains(LaneletSort().successor(lanelet_0), Unit(LaneletSort().lanelet_id(lanelet_1))),
                             Contains(LaneletSort().predecessor(lanelet_0), Unit(LaneletSort().lanelet_id(lanelet_1)))))

        return formula, [lanelet_0, lanelet_1]

    def existence_traffic_signs(self) -> Formula:
        """
        For each of the given IDs representing all traffic signs there exists a traffic sign with the same ID.

        :return: Formula.
        """
        lanelet = self.create_consts(LaneletSort())
        traffic_sign = self.create_consts(TrafficSignSort())
        traffic_sign_id = Int('traffic_sign_id')

        formula = Implies(And(self._sf.is_lanelet(lanelet),
                              Contains(LaneletSort().traffic_signs(lanelet), Unit(traffic_sign_id))),
                          Exists([traffic_sign], And(self._sf.is_traffic_sign(traffic_sign),
                                 traffic_sign_id == TrafficSignSort().traffic_sign_id(traffic_sign))))

        return formula, [lanelet, traffic_sign_id]

    def existence_traffic_lights(self) -> Formula:
        """
        For each of the given IDs representing all traffic lights there exists a traffic light with the same ID.

        :return: Formula.
        """
        lanelet = self.create_consts(LaneletSort())
        traffic_light = self.create_consts(TrafficLightSort())
        traffic_light_id = Int('traffic_light_id')

        formula = Implies(And(self._sf.is_lanelet(lanelet),
                              Contains(LaneletSort().traffic_lights(lanelet), Unit(traffic_light_id))),
                          Exists([traffic_light], And(self._sf.is_traffic_light(traffic_light),
                                 traffic_light_id == TrafficLightSort().traffic_light_id(traffic_light))))

        return formula, [lanelet, traffic_light_id]

    def existence_stop_line_traffic_signs(self) -> Formula:
        """
        For each of the given IDs representing all traffic signs of stop line there exists a traffic sign
        with the same ID.

        :return: Formula.
        """
        lanelet = self.create_consts(LaneletSort())
        traffic_sign = self.create_consts(TrafficSignSort())
        traffic_sign_id = Int('traffic_sign_id')

        formula = Implies(And(self._sf.is_lanelet(lanelet), LaneletSort().stop_line(lanelet) != StopLineSort().none,
                              Contains(StopLineSort().traffic_signs(LaneletSort().stop_line(lanelet)), Unit(
                                  traffic_sign_id))),
                          Exists([traffic_sign], And(self._sf.is_traffic_sign(traffic_sign),
                                 traffic_sign_id == TrafficSignSort().traffic_sign_id(traffic_sign))))

        return formula, [lanelet, traffic_sign_id]

    def existence_stop_line_traffic_lights(self) -> Formula:
        """
        For each of the given IDs representing all traffic lights of stop line there exists a traffic light
        with the same ID.

        :return: Formula.
        """
        lanelet = self.create_consts(LaneletSort())
        traffic_light = self.create_consts(TrafficLightSort())
        traffic_light_id = Int('traffic_light_id')

        formula = Implies(And(self._sf.is_lanelet(lanelet), LaneletSort().stop_line(lanelet) != StopLineSort().none,
                              Contains(StopLineSort().traffic_lights(LaneletSort().stop_line(lanelet)), Unit(
                                  traffic_light_id))),
                          Exists([traffic_light], And(self._sf.is_traffic_light(traffic_light),
                                 traffic_light_id == TrafficLightSort().traffic_light_id(traffic_light))))

        return formula, [lanelet, traffic_light_id]

    def included_stop_line_traffic_signs(self) -> Formula:
        """
        Each ID of traffic signs of stop line are included by the traffic signs of lanelet.

        :return: Formula.
        """
        lanelet = self.create_consts(LaneletSort())
        traffic_sign_id = Int('traffic_sign_id')

        formula = Implies(And(self._sf.is_lanelet(lanelet), Not(self._sf.is_none(LaneletSort().stop_line(lanelet))),
                              Contains(StopLineSort().traffic_signs(LaneletSort().stop_line(lanelet)), Unit(
                                  traffic_sign_id))),
                          Contains(LaneletSort().traffic_signs(lanelet), Unit(traffic_sign_id)))

        return formula, [lanelet, traffic_sign_id]

    def included_stop_line_traffic_lights(self) -> Formula:
        """
        Each ID of traffic lights of stop line are included by the traffic lights of lanelet.

        :return: Formula.
        """
        lanelet = self.create_consts(LaneletSort())
        traffic_light_id = Int('traffic_light_id')

        formula = Implies(And(self._sf.is_lanelet(lanelet), Not(self._sf.is_none(LaneletSort().stop_line(lanelet))),
                              Contains(StopLineSort().traffic_lights(LaneletSort().stop_line(lanelet)), Unit(
                                  traffic_light_id))),
                          Contains(LaneletSort().traffic_lights(lanelet), Unit(traffic_light_id)))

        return formula, [lanelet, traffic_light_id]

    def zero_or_two_points_stop_line(self) -> Formula:
        """
        Stop line of lanelet is constructed of either zero or two points.

        :return: Formula.
        """
        lanelet = self.create_consts(LaneletSort())

        formula = Implies(And(self._sf.is_lanelet(lanelet),
                              Not(self._sf.is_none(LaneletSort().stop_line(lanelet)))),
                          self._sf.is_none(StopLineSort().left_point(
                              LaneletSort().stop_line(lanelet))) == self._sf.is_none(
                              StopLineSort().right_point(LaneletSort().stop_line(lanelet))))

        return formula, [lanelet]

    def stop_line_points_on_polylines(self) -> Formula:
        """
        Left point of stop line lies on the left polyline. It is analogously the same for the right point.

        :return: Formula.
        """
        lanelet = self.create_consts(LaneletSort())

        formula = Implies(And(self._sf.is_lanelet(lanelet), Not(self._sf.is_none(LaneletSort().stop_line(lanelet)))),
                          And(self._sf.equal_reals(StopLineSort().left_point_distance(LaneletSort().stop_line(
                                  lanelet)), 0.0),
                              self._sf.equal_reals(StopLineSort().right_point_distance(LaneletSort().stop_line(
                                  lanelet)), 0.0)))

        return formula, [lanelet]
