from z3 import Implies, Length, And, Contains, Unit, Exists, Int

from crmapver.verification.z3.mapping import Z3Mapping, IntersectionSort, \
    IncomingElementSort, LaneletSort
from crmapver.verification.z3.formulas.formula_pool import Formula, FormulaPool
from crmapver.verification.z3.formulas.subformulas import SubformulaPool


class IntersectionFormulaPool(FormulaPool):
    """
    This class contains a pool of intersection formulas.
    """

    def __init__(self, mapping: Z3Mapping):
        super(IntersectionFormulaPool, self).__init__(mapping)

        self._sf = SubformulaPool(mapping)

    def at_least_two_incoming_elements(self) -> Formula:
        """
        Intersection consists of at least two incoming elements.

        :return: Formula.
        """
        intersection = self.create_consts(IntersectionSort())

        formula = Implies(self._sf.is_intersection(intersection),
                          Length(IntersectionSort().incoming_elements(intersection)) > 1)

        return formula, [intersection]

    def at_least_one_incoming_lanelet(self) -> Formula:
        """
        Incoming element consists of at least one incoming lanelet.

        :return: Formula.
        """
        intersection = self.create_consts(IntersectionSort())
        incoming_element = self.create_consts(IncomingElementSort())

        formula = Implies(And(self._sf.is_intersection(intersection),
                              Contains(IntersectionSort().incoming_elements(intersection), Unit(incoming_element))),
                          Length(IncomingElementSort().incoming_lanelets(incoming_element)) > 0)

        return formula, [intersection, incoming_element]

    def existence_incoming_lanelets(self) -> Formula:
        """
        For each of the given IDs representing all incoming lanelets there exists a lanelet with the same ID.

        :return: Formula.
        """
        intersection = self.create_consts(IntersectionSort())
        incoming_element = self.create_consts(IncomingElementSort())
        lanelet = self.create_consts(LaneletSort())
        lanelet_id = Int('lanelet_id')

        formula = Implies(And(self._sf.is_intersection(intersection),
                              Contains(IntersectionSort().incoming_elements(intersection), Unit(incoming_element)),
                              Contains(IncomingElementSort().incoming_lanelets(incoming_element), Unit(lanelet_id))),
                          Exists([lanelet], And(self._sf.is_lanelet(lanelet),
                                                lanelet_id == LaneletSort().lanelet_id(lanelet))))

        return formula, [intersection, incoming_element, lanelet_id]

    def existence_outgoing_right(self) -> Formula:
        """
        For each of the given IDs representing all right outgoing lanelets there exists a lanelet with the same ID.

        :return: Formula.
        """
        intersection = self.create_consts(IntersectionSort())
        incoming_element = self.create_consts(IncomingElementSort())
        lanelet = self.create_consts(LaneletSort())
        lanelet_id = Int('lanelet_id')

        formula = Implies(And(self._sf.is_intersection(intersection),
                              Contains(IntersectionSort().incoming_elements(intersection), Unit(incoming_element)),
                              Contains(IncomingElementSort().outgoings_right(incoming_element), Unit(lanelet_id))),
                          Exists([lanelet], And(self._sf.is_lanelet(lanelet),
                                                lanelet_id == LaneletSort().lanelet_id(lanelet))))

        return formula, [intersection, incoming_element, lanelet_id]

    def existence_outgoing_straight(self) -> Formula:
        """
        For each of the given IDs representing all straight outgoing lanelets there exists a lanelet with the same ID.

        :return: Formula.
        """
        intersection = self.create_consts(IntersectionSort())
        incoming_element = self.create_consts(IncomingElementSort())
        lanelet = self.create_consts(LaneletSort())
        lanelet_id = Int('lanelet_id')

        formula = Implies(And(self._sf.is_intersection(intersection),
                              Contains(IntersectionSort().incoming_elements(intersection), Unit(incoming_element)),
                              Contains(IncomingElementSort().outgoings_straight(incoming_element), Unit(lanelet_id))),
                          Exists([lanelet],
                                 And(self._sf.is_lanelet(lanelet), lanelet_id == LaneletSort().lanelet_id(lanelet))))

        return formula, [intersection, incoming_element, lanelet_id]

    def existence_outgoing_left(self) -> Formula:
        """
        For each of the given IDs representing all left outgoing lanelets there exists a lanelet with the same ID.

        :return: Formula.
        """
        intersection = self.create_consts(IntersectionSort())
        incoming_element = self.create_consts(IncomingElementSort())
        lanelet = self.create_consts(LaneletSort())
        lanelet_id = Int('lanelet_id')

        formula = Implies(And(self._sf.is_intersection(intersection),
                              Contains(IntersectionSort().incoming_elements(intersection), Unit(incoming_element)),
                              Contains(IncomingElementSort().outgoings_left(incoming_element), Unit(lanelet_id))),
                          Exists([lanelet],
                                 And(self._sf.is_lanelet(lanelet), lanelet_id == LaneletSort().lanelet_id(lanelet))))

        return formula, [intersection, incoming_element, lanelet_id]