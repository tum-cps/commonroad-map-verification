from commonroad.scenario.traffic_light import TrafficLightState
from z3 import Implies, Length, Int, And, Contains, Unit, Exists, ForAll, Empty, SeqSort, IntSort, Concat, IntVal, \
    Extract, StringVal, Or

from crmapver.verification.z3.mapping import Z3Mapping, TrafficLightSort, IntersectionSort, \
    IncomingElementSort, LaneletSort, CycleElementSort, MapSort
from crmapver.verification.z3.formulas.formula_pool import Formula, FormulaPool
from crmapver.verification.z3.formulas.subformulas import SubformulaPool


class TrafficLightFormulaPool(FormulaPool):
    """
    This class contains a pool of traffic light formulas.
    """

    def __init__(self, mapping: Z3Mapping):
        super(TrafficLightFormulaPool, self).__init__(mapping)

        self._sf = SubformulaPool(mapping)

    def at_least_one_cycle_element(self) -> Formula:
        """
        Traffic light consists of at least one cycle element.

        :return: Formula.
        """
        traffic_light = self.create_consts(TrafficLightSort())

        formula = Implies(self._sf.is_traffic_light(traffic_light),
                          Length(TrafficLightSort().cycle_elements(traffic_light)) > 0)

        return formula, [traffic_light]

    def traffic_light_per_incoming(self) -> Formula:
        """
        Each traffic light is referenced from exactly one incoming element.

        :return: Formula.
        """
        traffic_light = self.create_consts(TrafficLightSort())
        intersection = self.create_consts(IntersectionSort())
        incoming_element = self.create_consts(IncomingElementSort())
        lanelet_0, lanelet_1 = self.create_consts(LaneletSort(), 2)
        traffic_light_id = Int('traffic_light_id')

        formula = Implies(And(self._sf.is_traffic_light(traffic_light), self._sf.is_lanelet(lanelet_0),
                              self._sf.is_lanelet(lanelet_1),
                              traffic_light_id == TrafficLightSort().traffic_light_id(traffic_light),
                              Contains(LaneletSort().traffic_lights(lanelet_0), Unit(traffic_light_id)),
                              Contains(LaneletSort().traffic_lights(lanelet_1), Unit(traffic_light_id))),
                          Exists([intersection, incoming_element],
                                 And(self._sf.is_intersection(intersection),
                                     self._sf.is_incoming_element(incoming_element),
                                     IntersectionSort().intersection_id(
                                         intersection) == IncomingElementSort().related_intersection(incoming_element),
                                     Contains(IncomingElementSort().incoming_lanelets(incoming_element),
                                              Unit(LaneletSort().lanelet_id(lanelet_0))),
                                     Contains(IncomingElementSort().incoming_lanelets(incoming_element),
                                              Unit(LaneletSort().lanelet_id(lanelet_1))))))

        return formula, [traffic_light_id, lanelet_0, lanelet_1]

    def referenced_traffic_light(self) -> Formula:
        """
        Each traffic light is referenced by a lanelet.

        :return: Formula.
        """
        traffic_light = self.create_consts(TrafficLightSort())
        lanelet = self.create_consts(LaneletSort())

        formula = Implies(self._sf.is_traffic_light(traffic_light),
                          Exists([lanelet], And(self._sf.is_lanelet(lanelet),
                                                Contains(LaneletSort().traffic_lights(lanelet), Unit(
                                                    TrafficLightSort().traffic_light_id(traffic_light))))))

        return formula, [lanelet]

    def non_zero_duration(self) -> Formula:
        """
        The duration of cycle element is non-zero.

        :return: Formula.
        """
        traffic_light = self.create_consts(TrafficLightSort())
        cycle_element = self.create_consts(CycleElementSort())

        formula = Implies(And(self._sf.is_traffic_light(traffic_light), self._sf.is_cycle_element(cycle_element),
                              Contains(TrafficLightSort().cycle_elements(traffic_light), Unit(cycle_element))),
                          CycleElementSort().duration(cycle_element) > 0)

        return formula, [traffic_light]

    def unique_state_in_cycle(self) -> Formula:
        """
        A state is unique and cannot be used again in the same cycle element.

        :return: Formula.
        """
        traffic_light = self.create_consts(TrafficLightSort())
        cycle_element_0, cycle_element_1 = self.create_consts(CycleElementSort(), 2)
        state = Int('state')

        formula = Implies(And(self._sf.is_traffic_light(traffic_light), self._sf.is_cycle_element(cycle_element_0),
                              Contains(TrafficLightSort().cycle_elements(traffic_light), Unit(cycle_element_0)),
                              state == CycleElementSort().state(cycle_element_0)),
                          ForAll([cycle_element_1],
                                 Implies(And(self._sf.is_cycle_element(cycle_element_1),
                                             cycle_element_0 != cycle_element_1,
                                             CycleElementSort().related_traffic_light(
                                                 cycle_element_1) == TrafficLightSort().traffic_light_id(
                                                 traffic_light)),
                                         CycleElementSort().state(cycle_element_0) != CycleElementSort().state(
                                             cycle_element_1))))

        return formula, [traffic_light, state]

    def cycle_state_combinations(self) -> Formula:
        """
        The order of the cycle states of a traffic light is valid for the corresponding country.

        :return: Formula.
        """
        TLS = TrafficLightState
        state_combinations = {'DEU': [[TLS.RED, TLS.RED_YELLOW, TLS.GREEN, TLS.YELLOW],
                                      [TLS.INACTIVE, TLS.YELLOW, TLS.RED], [TLS.YELLOW, TLS.INACTIVE]],
                              'CHN': [[TLS.GREEN, TLS.RED, TLS.YELLOW], [TLS.YELLOW, TLS.INACTIVE]],
                              'ESP': [[TLS.RED, TLS.GREEN, TLS.YELLOW], [TLS.YELLOW, TLS.INACTIVE]],
                              'RUS': [[TLS.RED, TLS.YELLOW, TLS.GREEN, TLS.INACTIVE, TLS.GREEN,
                                       TLS.INACTIVE, TLS.GREEN, TLS.INACTIVE, TLS.GREEN, TLS.YELLOW],
                                      [TLS.YELLOW, TLS.INACTIVE]],
                              'BEL': [[TLS.RED, TLS.GREEN, TLS.YELLOW], [TLS.YELLOW, TLS.INACTIVE]],
                              'FRA': [[TLS.RED, TLS.GREEN, TLS.YELLOW], [TLS.YELLOW, TLS.INACTIVE]],
                              'GRC': [[TLS.RED, TLS.GREEN, TLS.YELLOW], [TLS.YELLOW, TLS.INACTIVE]],
                              'HRV': [[TLS.RED, TLS.RED_YELLOW, TLS.RED, TLS.YELLOW], [TLS.YELLOW, TLS.INACTIVE]],
                              'USA': [[TLS.RED, TLS.GREEN, TLS.YELLOW], [TLS.YELLOW, TLS.INACTIVE]],
                              'ITA': [[TLS.RED, TLS.GREEN, TLS.YELLOW], [TLS.YELLOW, TLS.INACTIVE]],
                              'ZAM': [[TLS.RED, TLS.RED_YELLOW, TLS.GREEN, TLS.YELLOW],
                                      [TLS.INACTIVE, TLS.YELLOW, TLS.RED], [TLS.YELLOW, TLS.INACTIVE]]}

        road_map = self.create_consts(MapSort())
        traffic_light = self.create_consts(TrafficLightSort())
        cycle_element = self.create_consts(CycleElementSort())
        cycle_position = Int('cycle_position')
        index = Int('index')

        country_combinations_seqs = []
        for country_id, combinations in state_combinations.items():
            combination_seqs = []
            for combination in combinations:
                seq = Empty(SeqSort(IntSort()))
                for state in combination:
                    seq = Concat(seq, Unit(IntVal(hash(state))))
                combination_seqs.append(And(Extract(seq, cycle_position, 1) == Unit(
                    CycleElementSort().state(cycle_element)),
                                            Length(TrafficLightSort().cycle_elements(traffic_light)) == Length(seq)))
            country_combinations_seqs.append(And(StringVal(country_id) == MapSort().country_id(road_map),
                                                 Or(combination_seqs)))
        combinations_subformula = Or(country_combinations_seqs)

        formula = Implies(And(self._sf.is_traffic_light(traffic_light), self._sf.is_map(road_map)),
                          Exists([index],
                                 And(index >= 0, index < Length(TrafficLightSort().cycle_elements(traffic_light)),
                                     ForAll([cycle_position], Implies(And(cycle_position >= 0, cycle_position < Length(
                                         TrafficLightSort().cycle_elements(traffic_light))),
                                            And(Exists([cycle_element],
                                                And(self._sf.is_cycle_element(cycle_element),
                                                    Extract(TrafficLightSort().cycle_elements(traffic_light),
                                                            (cycle_position + index) % Length(
                                                                TrafficLightSort().cycle_elements(
                                                                    traffic_light)), 1) == Unit(cycle_element),
                                                    combinations_subformula))))))))

        return formula, [traffic_light]
