import collections.abc
from typing import Any

from commonroad.scenario.lanelet import LaneletType
from z3 import DatatypeRef, BoolRef, Or, BoolVal, IntNumRef, IntVal, RealVal, ArithRef, If, RatNumRef, \
    DatatypeSortRef, And, IntSort, Not, Length

from crmapver.verification.z3.mapping import Z3Mapping, VertexSort, PolylineSort, \
    StopLineSort, LaneletSort, TrafficSignSort, TrafficSignElementSort, TrafficLightSort, CycleElementSort, \
    IntersectionSort, IncomingElementSort, VerificationSort
from crmapver.verification.z3.formulas.formula_pool import FormulaPool


class SubformulaPool(FormulaPool):
    """
    This class contains a pool of subformulas.
    """

    def __init__(self, mapping: Z3Mapping):
        super(SubformulaPool, self).__init__(mapping)

    def is_map(self, road_map: DatatypeRef) -> BoolRef:
        """
        Map is equal to map from network.

        :param road_map: Datatype reference of map.
        :return: Reference of logical boolean expression.
        """
        subformula = road_map == self._mapping.map_z3

        return subformula

    def is_lanelet(self, lanelet: DatatypeRef) -> BoolRef:
        """
        Lanelet is contained by lanelets from network.

        Example: lanelet_0 ∨ lanelet_1 ∨ ... ∨ lanelet_n

        :param: Datatype reference of lanelet.
        :return: Reference of logical boolean expression.
        """
        subformula = Or([lanelet == network_lanelet for network_lanelet in self._mapping.lanelets_z3])

        return subformula

    def is_traffic_sign(self, traffic_sign: DatatypeRef) -> BoolRef:
        """
        Traffic sign is contained by traffic signs from network.

        Example: traffic_sign_0 ∨ traffic_sign_1 ∨ ... ∨ traffic_sign_n

        :param traffic_sign: Datatype reference of traffic sign.
        :return: Reference of logical boolean expression.
        """
        subformula = Or([traffic_sign == network_traffic_sign
                         for network_traffic_sign in self._mapping.traffic_signs_z3])

        return subformula

    def is_traffic_sign_element(self, traffic_sign_element: DatatypeRef) -> BoolRef:
        """
        Traffic sign element is contained by traffic sign elements from network.

        Example: traffic_sign_element_0 ∨ traffic_sign_element_1 ∨ ... ∨ traffic_sign_element_n

        :param traffic_sign_element: Datatype reference of traffic sign element.
        :return: Reference of logical boolean expression.
        """
        subformula = Or([traffic_sign_element == network_traffic_sign_element
                         for network_traffic_sign_element in self._mapping.traffic_signs_elements_z3])

        return subformula

    def is_traffic_light(self, traffic_light: DatatypeRef) -> BoolRef:
        """
        Traffic sign is contained by traffic lights from network.

        Example: traffic_light_0 ∨ traffic_light_1 ∨ ... ∨ traffic_light_n

        :param traffic_light: Datatype reference of traffic light.
        :return: Reference of logical boolean expression.
        """
        subformula = Or([traffic_light == network_traffic_light
                         for network_traffic_light in self._mapping.traffic_lights_z3])

        return subformula

    def is_cycle_element(self, cycle_element: DatatypeRef) -> BoolRef:
        """
        Cycle element is contained by cycle elements from network.

        Example: cycle_element_0 ∨ cycle_element_1 ∨ ... ∨ cycle_element_n

        :param cycle_element: Datatype reference of cycle element.
        :return: Reference of logical boolean expression.
        """
        subformula = Or([cycle_element == network_cycle_element
                         for network_cycle_element in self._mapping.cycle_elements_z3])

        return subformula

    def is_intersection(self, intersection: DatatypeRef) -> BoolRef:
        """
        Intersection is contained by intersections from network.

        Example: intersection_0 ∨ intersection_1 ∨ ... ∨ intersection_n

        :param intersection: Datatype reference of intersection.
        :return: Reference of logical boolean expression.
        """
        subformula = Or([intersection == network_intersection
                         for network_intersection in self._mapping.intersections_z3])

        return subformula

    def is_incoming_element(self, incoming_element: DatatypeRef) -> BoolRef:
        """
        Incoming element is contained by incoming elements from network.

        Example: incoming_element_0 ∨ incoming_element_1 ∨ ... ∨ incoming_element_n

        :param incoming_element: Datatype reference of incoming element.
        :return: Reference of logical boolean expression.
        """
        subformula = Or([incoming_element == network_incoming_element
                         for network_incoming_element in self._mapping.incoming_elements_z3])

        return subformula

    @staticmethod
    def is_none(value: DatatypeRef) -> BoolRef:
        """
        Value of datatype is none.

        :param value: Datatype reference of value.
        :return: Reference of logical boolean expression.
        """
        sorts = [VertexSort(), PolylineSort(), StopLineSort(), LaneletSort(), TrafficSignSort(),
                 TrafficSignElementSort(), TrafficLightSort(), CycleElementSort(), IntersectionSort(),
                 IncomingElementSort()]

        sort = value.sort()

        subformula = BoolVal(True)
        for s in sorts:
            if s == sort:
                subformula = value == s.none

        return subformula

    @staticmethod
    def hash_val(val: Any) -> IntNumRef:
        """
        Computes the hash of value.

        :param val: Value.
        :return: Reference of hashed value.
        """
        assert isinstance(val, collections.abc.Hashable), "Value val={} is not hashable!".format(val)

        subformula = IntVal(hash(val))

        return subformula

    @staticmethod
    def abs_real(val: RealVal) -> ArithRef:
        """
        Computes the absolute value of real.

        :param val: Real value.
        :return: Reference of logical arithmetic expression.
        """
        subformula = If(val >= 0., val, -val)

        return subformula

    @staticmethod
    def equal_reals(val_0: RealVal, val_1: RealVal, thresh: RatNumRef = RealVal("{:.20f}".format(1e-8))) -> BoolRef:
        """
        Equality of two float values in dependence on a threshold.

        :param val_0: First real value.
        :param val_1: Second real value.
        :param thresh: Fixed threshold value.
        :return: Reference of logical boolean expression.
        """
        subformula = SubformulaPool.abs_real(val_0 - val_1) < thresh

        return subformula

    @staticmethod
    def valid_id(val: IntVal) -> BoolRef:
        """
        ID is a positive integer number.

        :param val: Integer value.
        :return: Reference of logical boolean expression.
        """
        subformula = val >= 0

        return subformula

    @staticmethod
    def vertices_distance(vertex_0: DatatypeRef, vertex_1: DatatypeRef) -> ArithRef:
        """
        Computes the distance without square root between two vertices.

        :param vertex_0: Datatype reference of first vertex.
        :param vertex_1: Datatype reference of second vertex.
        :return: Reference of logical arithmetic expression.
        """
        subformula = (VertexSort().x(vertex_1) - VertexSort().x(vertex_0)) ** 2 +  \
                     (VertexSort().y(vertex_1) - VertexSort().y(vertex_0)) ** 2

        return subformula

    @staticmethod
    def vertex_left_of_line(start_vertex: DatatypeRef, end_vertex: DatatypeRef, vertex: DatatypeRef) -> BoolRef:
        """
        Checks whether a vertex is located on the left of line represented by two other vertices.

        :param start_vertex: Datatype reference of start vertex of line.
        :param end_vertex: Datatype reference of end vertex of line.
        :param vertex: Datatype reference of considered vertex.
        :return: Reference of logical boolean expression.
        """
        subformula = ((VertexSort().x(end_vertex) - VertexSort().x(start_vertex)) *
                      (VertexSort().y(vertex) - VertexSort().y(start_vertex)) -
                      (VertexSort().y(end_vertex) - VertexSort().y(start_vertex)) *
                      (VertexSort().x(vertex) - VertexSort().x(start_vertex))) > 0

        return subformula

    def thresh(self, accurate: BoolVal) -> RatNumRef:
        """
        Computes the threshold for a connection between two polylines. Either the accurate or potential
        threshold is used. The accurate threshold is a small constant value and the potential threshold represents
        the minimal maximal length or half of width of a group of lanelets.

        :param accurate: Boolean value indicates whether an exact threshold is to be considered.
        :return: Reference of logical arithmetic expression.
        """
        verification = self._mapping.verification_z3

        subformula = If(accurate, VerificationSort().connection_thresh(verification),
                        VerificationSort().potential_connection_thresh(verification))

        return subformula

    def equal_vertices(self, vertex_0: DatatypeRef, vertex_1: DatatypeRef, thresh: RatNumRef) -> BoolRef:
        """
        Equality of two vertices.

        :param vertex_0: Datatype reference of first vertex.
        :param vertex_1: Datatype reference of second vertex.
        :param thresh: Reference of threshold.
        :return: Reference of logical boolean expression.
        """
        subformula = self.vertices_distance(vertex_0, vertex_1) < thresh

        return subformula

    def is_connection(self, polyline_0: DatatypeSortRef, polyline_1: DatatypeSortRef, is_start_0: BoolVal,
                      is_start_1: BoolVal, thresh: RatNumRef) -> BoolRef:
        """
        Equality of two starting or ending vertices from two polylines.

        :param polyline_0: Datatype reference of first polyline.
        :param polyline_1: Datatype reference of second polyline.
        :param is_start_0: Boolean reference indicates whether starting or ending point of the first polyline
         is to be considered.
        :param is_start_1: Boolean reference indicates whether starting or ending point of the second polyline
        is to be considered.
        :param thresh: Reference of threshold.
        :return: Reference of logical boolean expression.
        """
        subformula = self.equal_vertices(If(is_start_0, PolylineSort().start_vertex(polyline_0),
                                            PolylineSort().end_vertex(polyline_0)),
                                         If(is_start_1, PolylineSort().start_vertex(polyline_1),
                                            PolylineSort().end_vertex(polyline_1)), thresh)

        return subformula

    def are_successor_connections(self, lanelet_0: DatatypeRef, lanelet_1: DatatypeRef,
                                  accurate: bool) -> BoolRef:
        """
        The ending points of first lanelet and starting points of second lanelet are equal.

        :param lanelet_0: Datatype reference of first lanelet.
        :param lanelet_1: Datatype reference of second lanelet.
        :param accurate: Boolean indicates whether an exact threshold is to be considered.
        :return: Reference of logical boolean expression.
        """
        subformula = And(self.is_connection(LaneletSort().left_polyline(lanelet_0),
                                            LaneletSort().left_polyline(lanelet_1), False, True,
                                            self.thresh(accurate)),
                         self.is_connection(LaneletSort().right_polyline(lanelet_0),
                                            LaneletSort().right_polyline(lanelet_1), False, True,
                                            self.thresh(accurate)))

        return subformula

    def are_predecessor_connections(self, lanelet_0: DatatypeRef, lanelet_1: DatatypeRef,
                                    accurate: bool) -> BoolRef:
        """
        The starting points of first lanelet and ending points of second lanelet are equal.

        :param lanelet_0: Datatype reference of first lanelet.
        :param lanelet_1: Datatype reference of second lanelet.
        :param accurate: Boolean indicates whether an exact threshold is to be considered.
        :return: Reference of logical boolean expression.
        """
        subformula = And(self.is_connection(LaneletSort().left_polyline(lanelet_0),
                                            LaneletSort().left_polyline(lanelet_1), True, False,
                                            self.thresh(accurate)),
                         self.is_connection(LaneletSort().right_polyline(lanelet_0),
                                            LaneletSort().right_polyline(lanelet_1), True, False,
                                            self.thresh(accurate)))

        return subformula

    def are_left_merging_adj_connections(self, lanelet_0: DatatypeRef, lanelet_1: DatatypeRef,
                                         accurate: bool) -> BoolRef:
        """
        The left starting point of first lanelet and right starting point of second lanelet are equal.
        It is the same for the ending points of first lanelet and second lanelet.

        :param lanelet_0: Datatype reference of first lanelet.
        :param lanelet_1: Datatype reference of second lanelet.
        :param accurate: Boolean indicates whether an exact threshold is to be considered.
        :return: Reference of logical boolean expression.
        """
        subformula = And(self.is_connection(LaneletSort().left_polyline(lanelet_0),
                                            LaneletSort().right_polyline(lanelet_1), True, True,
                                            self.thresh(accurate)),
                         self.is_connection(LaneletSort().left_polyline(lanelet_0),
                                            LaneletSort().left_polyline(lanelet_1), False, False,
                                            self.thresh(accurate)),
                         self.is_connection(LaneletSort().right_polyline(lanelet_0),
                                            LaneletSort().right_polyline(lanelet_1), False, False,
                                            self.thresh(accurate)))

        return subformula

    def are_right_merging_adj_connections(self, lanelet_0: DatatypeRef, lanelet_1: DatatypeRef,
                                          accurate: bool) -> BoolRef:
        """
        The right starting point of first lanelet and left starting point of second lanelet are equal.
        It is the same for the ending points of first lanelet and second lanelet.

        :param lanelet_0: Datatype reference of first lanelet.
        :param lanelet_1: Datatype reference of second lanelet.
        :param accurate: Boolean indicates whether an exact threshold is to be considered.
        :return: Reference of logical boolean expression.
        """
        subformula = And(self.is_connection(LaneletSort().right_polyline(lanelet_0),
                                            LaneletSort().left_polyline(lanelet_1), True, True,
                                            self.thresh(accurate)),
                         self.is_connection(LaneletSort().left_polyline(lanelet_0),
                                            LaneletSort().left_polyline(lanelet_1), False, False,
                                            self.thresh(accurate)),
                         self.is_connection(LaneletSort().right_polyline(lanelet_0),
                                            LaneletSort().right_polyline(lanelet_1), False, False,
                                            self.thresh(accurate)))

        return subformula

    def are_left_forking_adj_connections(self, lanelet_0: DatatypeRef, lanelet_1: DatatypeRef,
                                         accurate: bool) -> BoolRef:
        """
        The starting points of first and second lanelet are equal. It is the same for the left ending point
        of first lanelet and right ending point of second lanelet.

        :param lanelet_0: Datatype reference of first lanelet.
        :param lanelet_1: Datatype reference of second lanelet.
        :param accurate: Boolean indicates whether an exact threshold is to be considered.
        :return: Reference of logical boolean expression.
        """
        subformula = And(self.is_connection(LaneletSort().left_polyline(lanelet_0),
                                            LaneletSort().left_polyline(lanelet_1), True, True,
                                            self.thresh(accurate)),
                         self.is_connection(LaneletSort().right_polyline(lanelet_0),
                                            LaneletSort().right_polyline(lanelet_1), True, True,
                                            self.thresh(accurate)),
                         self.is_connection(LaneletSort().left_polyline(lanelet_0),
                                            LaneletSort().right_polyline(lanelet_1), False, False,
                                            self.thresh(accurate)))

        return subformula

    def are_right_forking_adj_connections(self, lanelet_0: DatatypeRef, lanelet_1: DatatypeRef,
                                          accurate: bool) -> BoolRef:
        """
        The starting points of first and second lanelet are equal. It is the same for the right ending point
        of first lanelet and left ending point of second lanelet.

        :param lanelet_0: Datatype reference of first lanelet.
        :param lanelet_1: Dataytpe reference of second lanelet.
        :param accurate: Boolean indicates whether an extract threshold is not be considered.
        :return: Reference of logical boolean expression.
        """
        subformula = And(self.is_connection(LaneletSort().left_polyline(lanelet_0),
                                            LaneletSort().left_polyline(lanelet_1), True, True,
                                            self.thresh(accurate)),
                         self.is_connection(LaneletSort().right_polyline(lanelet_0),
                                            LaneletSort().right_polyline(lanelet_1), True, True,
                                            self.thresh(accurate)),
                         self.is_connection(LaneletSort().right_polyline(lanelet_0),
                                            LaneletSort().left_polyline(lanelet_1), False, False,
                                            self.thresh(accurate)))

        return subformula

    def is_left_adj_type(self, lanelet: DatatypeRef, type_name: str) -> BoolRef:
        """
        The left adjacency has a suited type. The type can be PARALLEL, MERGE, and FORK.

        :param lanelet: Datatype reference of lanelet.
        :param type_name: Name of adjacency type.
        :return: Reference of logical boolean expression.
        """
        assert type_name in ['Parallel', 'Merge', 'Fork'], "Given adj_type={} in subformula is not defined!" \
            .format(type_name)

        subformula = LaneletSort().left_adj_type(lanelet) == self.hash_val(type_name)

        return subformula

    def is_right_adj_type(self, lanelet: DatatypeRef, type_name: str) -> BoolRef:
        """
        The right adjacency has a suited type. The type can be PARALLEL, MERGE, and FORK.

        :param lanelet: Datatype reference of lanelet.
        :param type_name: Name of adjacency type.
        :return: Reference of logical boolean expression.
        """
        assert type_name in ['Parallel', 'Merge', 'Fork'], "Given adj_type={} in subformula is not defined!" \
            .format(type_name)

        subformula = LaneletSort().right_adj_type(lanelet) == self.hash_val(type_name)

        return subformula

    @staticmethod
    def valid_type_combination(type_0: IntSort, type_1: IntSort) -> BoolRef:
        """
        The combinations of two lanelet types are allowed to describe simultaneously lanelet.

        :param type_0: Reference of first lanelet type.
        :param type_1: Reference of second type.
        :return: Reference of logical boolean expression.
        """
        LT = LaneletType
        invalid_combis = {LT.URBAN: {LT.COUNTRY},
                          LT.COUNTRY: {LT.URBAN},
                          LT.HIGHWAY: {LT.INTERSTATE},
                          LT.DRIVE_WAY: {LT.INTERSTATE, LT.HIGHWAY, LT.BUS_LANE, LT.BUS_STOP, LT.BICYCLE_LANE,
                                         LT.INTERSECTION},
                          LT.MAIN_CARRIAGE_WAY: {LT.CROSSWALK, LT.INTERSECTION},
                          LT.SHOULDER: {LT.BUS_LANE, LT.BUS_STOP, LT.SIDEWALK},
                          LT.BUS_LANE: {LT.SHOULDER, LT.DRIVE_WAY, LT.INTERSTATE},
                          LT.BUS_STOP: {LT.INTERSTATE, LT.SHOULDER, LT.DRIVE_WAY},
                          LT.BICYCLE_LANE: {LT.INTERSTATE, LT.DRIVE_WAY},
                          LT.SIDEWALK: {LT.INTERSTATE},
                          LT.CROSSWALK: {LT.MAIN_CARRIAGE_WAY, LT.INTERSTATE},
                          LT.INTERSTATE: {LT.BUS_STOP, LT.CROSSWALK},
                          LT.INTERSECTION: {LT.DRIVE_WAY, LT.MAIN_CARRIAGE_WAY}}

        and_parts = []
        finished_types = set()
        for l_type, i_types in invalid_combis.items():
            for i_type in i_types:
                if i_type in finished_types:
                    continue
                and_part = And(type_0 == SubformulaPool.hash_val(l_type.value),
                               type_1 == SubformulaPool.hash_val(i_type.value))
                and_parts.append(and_part)

        subformula = Not(Or(and_parts))

        return subformula

    @staticmethod
    def are_followed_composable(lanelet_0: DatatypeRef, lanelet_1: DatatypeRef) -> BoolRef:
        """
        Two lanelets are composable if their traffic signs, traffic lights, and lanelet types are equal. The
        first lanelet should have one successor and the second lanelet should have one predecessor.

        :param lanelet_0: Datatype reference of first lanelet.
        :param lanelet_1: Datatype reference of second lanelet.
        :return: Reference of logical boolean expression.
        """
        subformula = And(Length(LaneletSort().successor(lanelet_0)) == 1,
                         Length(LaneletSort().predecessor(lanelet_1)) == 1,
                         LaneletSort().traffic_signs(lanelet_0) == LaneletSort().traffic_signs(lanelet_1),
                         LaneletSort().traffic_lights(lanelet_0) == LaneletSort().traffic_lights(lanelet_1),
                         LaneletSort().types(lanelet_0) == LaneletSort().types(lanelet_1))

        return subformula
