import re
from abc import ABC
from typing import Tuple, List, Union

from z3 import BoolRef, ExprRef, DatatypeSortRef, DatatypeRef, Const

from crmapver.verification.z3.mapping import Z3Mapping

Formula = Tuple[BoolRef, List[ExprRef]]


class FormulaPool(ABC):
    """
    This class contains a pool of formulas.
    """

    def __init__(self, mapping: Z3Mapping):
        self._mapping = mapping

    @staticmethod
    def create_consts(dtype: DatatypeSortRef, num: int = 1) -> Union[DatatypeRef, Tuple[DatatypeRef, ...]]:
        """
        Creates a desired number of consts of specific type.

        :param dtype: Datatype.
        :param num: Number of desired consts.
        :return: Const name.
        """
        assert num > 0, "The number num={} has to be higher than zero!".format(num)

        name = re.sub('(?<!^)(?=[A-Z])', '_', dtype.name()).lower()

        consts = []
        for index in range(num):
            const: DatatypeRef = Const(name + ('' if num == 1 else '_{}'.format(index)), dtype)
            consts.append(const)

        if len(consts) == 1:
            return consts[0]

        return tuple(consts)
