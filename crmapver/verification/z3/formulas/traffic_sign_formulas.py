from commonroad.scenario.traffic_sign import TrafficSignIDGermany, TrafficSignIDZamunda, TrafficSignIDUsa, \
    TrafficSignIDChina, TrafficSignIDRussia, TrafficSignIDBelgium, TrafficSignIDFrance, TrafficSignIDGreece, \
    TrafficSignIDCroatia, TrafficSignIDItaly, TrafficSignIDPuertoRico
from z3 import Implies, Length, Exists, And, Contains, Unit, Or, Real, RealVal, IntSort, RealSort, Int, Extract

from crmapver.verification.z3.mapping import Z3Mapping, TrafficSignSort, LaneletSort, \
    TrafficSignElementSort, TupleSort
from crmapver.verification.z3.formulas.formula_pool import Formula, FormulaPool
from crmapver.verification.z3.formulas.subformulas import SubformulaPool

TSGer = TrafficSignIDGermany
TSZam = TrafficSignIDZamunda
TSUsa = TrafficSignIDUsa
TSChn = TrafficSignIDChina
TSRus = TrafficSignIDRussia
TSBel = TrafficSignIDBelgium
TSFra = TrafficSignIDFrance
TSGrc = TrafficSignIDGreece
TSHrv = TrafficSignIDCroatia
TSIta = TrafficSignIDItaly
TSPri = TrafficSignIDPuertoRico


class TrafficSignFormulaPool(FormulaPool):
    """
    This class contains a pool of traffic sign formulas.
    """

    def __init__(self, mapping: Z3Mapping):
        super(TrafficSignFormulaPool, self).__init__(mapping)

        self._sf = SubformulaPool(mapping)

    def at_least_one_traffic_sign_element(self) -> Formula:
        """
        Traffic sign consists of at least one traffic sign element.

        :return: Formula.
        """
        traffic_sign = self.create_consts(TrafficSignSort())

        formula = Implies(self._sf.is_traffic_sign(traffic_sign),
                          Length(TrafficSignSort().traffic_sign_elements(traffic_sign)) > 0)

        return formula, [traffic_sign]

    def referenced_traffic_sign(self) -> Formula:
        """
        Each traffic sign is referenced by a lanelet.

        :return: Formula.
        """
        traffic_sign = self.create_consts(TrafficSignSort())
        lanelet = self.create_consts(LaneletSort())

        formula = Implies(self._sf.is_traffic_sign(traffic_sign),
                          Exists([lanelet], And(self._sf.is_lanelet(lanelet),
                                 Contains(LaneletSort().traffic_signs(lanelet), Unit(
                                     TrafficSignSort().traffic_sign_id(traffic_sign))))))

        return formula, [traffic_sign]

    def given_additional_value(self) -> Formula:
        """
        The signs with ID 274, 275, and 310 require an additional value.

        :return: Formula.
        """
        traffic_sign = self.create_consts(TrafficSignSort())
        traffic_sign_element = self.create_consts(TrafficSignElementSort())

        signs_ids = [TSGer.TOWN_SIGN, TSGer.MAX_SPEED, TSGer.MAX_SPEED_ZONE_START, TSGer.MAX_SPEED_ZONE_END,
                     TSGer.MIN_SPEED, TSGer.MAX_SPEED_END, TSZam.MAX_SPEED, TSZam.MAX_SPEED_ZONE_START,
                     TSZam.MAX_SPEED_ZONE_END, TSZam.MIN_SPEED, TSZam.TOWN_SIGN, TSUsa.MAX_SPEED, TSChn.MAX_SPEED,
                     TSRus.MAX_SPEED, TSBel.MAX_SPEED, TSFra.MAX_SPEED, TSGrc.MAX_SPEED, TSHrv.MAX_SPEED,
                     TSIta.MAX_SPEED, TSPri.MAX_SPEED]  # TODO: Extend traffic signs
        is_sign_id = Or([TrafficSignElementSort().traffic_sign_element_id(
                traffic_sign_element) == hash(sign_id.name.lower())
                         for sign_id in signs_ids])

        formula = Implies(And(self._sf.is_traffic_sign(traffic_sign),
                              Contains(TrafficSignSort().traffic_sign_elements(traffic_sign), Unit(
                                  traffic_sign_element)), is_sign_id),
                          Length(TrafficSignElementSort().additional_values(traffic_sign_element)) > 0)

        return formula, [traffic_sign, traffic_sign_element]

    def valid_additional_value_speed_sign(self) -> Formula:
        """
        The speed signs such as 274 and 275 require a positive non-zero value as additional value.

        :return: Formula.
        """
        traffic_sign = self.create_consts(TrafficSignSort())
        traffic_sign_element = self.create_consts(TrafficSignElementSort())
        additional_value = Real('additional_value')

        signs_ids = [TSGer.MAX_SPEED, TSGer.MAX_SPEED_ZONE_START, TSGer.MAX_SPEED_ZONE_END, TSGer.MIN_SPEED,
                     TSGer.MAX_SPEED_END, TSZam.MAX_SPEED, TSZam.MAX_SPEED_ZONE_START, TSZam.MAX_SPEED_ZONE_END,
                     TSZam.MIN_SPEED, TSUsa.MAX_SPEED, TSChn.MAX_SPEED, TSChn.MAX_SPEED, TSRus.MAX_SPEED,
                     TSBel.MAX_SPEED, TSRus.MAX_SPEED, TSFra.MAX_SPEED, TSGrc.MAX_SPEED, TSHrv.MAX_SPEED,
                     TSIta.MAX_SPEED, TSPri.MAX_SPEED]
        is_sign_id = Or([TrafficSignElementSort().traffic_sign_element_id(
                traffic_sign_element) == hash(sign_id.name.lower())
                         for sign_id in signs_ids])

        formula = Implies(And(self._sf.is_traffic_sign(traffic_sign),
                              Contains(TrafficSignSort().traffic_sign_elements(traffic_sign), Unit(
                                  traffic_sign_element)),
                              Contains(TrafficSignElementSort().additional_values(traffic_sign_element), Unit(
                                  additional_value)), is_sign_id), additional_value > RealVal(0))

        return formula, [traffic_sign, traffic_sign_element]

    def maximal_distance_from_lanelet(self) -> Formula:
        """
         The distance between one of the lanelets referring traffic sign first and the traffic sign is
         a maximum of one meter.

        :return: Formula.
        """
        traffic_sign = self.create_consts(TrafficSignSort())
        distance = self.create_consts(TupleSort([IntSort(), RealSort()]))
        index = Int('index')

        formula = Implies(self._sf.is_traffic_sign(traffic_sign),
                          Exists([index, distance],
                                 And(index >= 0, index < Length(TrafficSignSort().distances(traffic_sign)),
                                     Extract(TrafficSignSort().distances(traffic_sign), index, 1) == Unit(distance),
                                     TupleSort([IntSort(), RealSort()]).project_1(distance) < RealVal(10.0),
                                     Contains(TrafficSignSort().first_occurrence(traffic_sign),
                                              Unit(TupleSort([IntSort(), RealSort()]).project_0(distance))))))

        return formula, [traffic_sign]
