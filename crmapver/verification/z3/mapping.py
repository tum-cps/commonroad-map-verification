from typing import Callable, Dict, List, Any, Tuple, Set
import numpy as np
from commonroad.scenario.intersection import IncomingGroup, Intersection
from commonroad.scenario.lanelet import LaneletNetwork, Lanelet, TrafficSign, TrafficLight, StopLine
from commonroad.scenario.traffic_sign import TrafficSignElement
from commonroad.scenario.traffic_light import TrafficLightCycleElement
from shapely.geometry import LineString, Point
from z3 import Datatype, IntSort, SeqSort, Empty, Concat, Unit, IntVal, RealSort, BoolSort, RealVal, SortRef, SeqRef, \
    StringSort, DatatypeRef, StringVal, ExprRef

from crmapver.config import MapVerParams
from crmapver.verification.mapping import Mapping, Preprocessing


class Z3Mapping(Mapping):

    def __init__(self, network: LaneletNetwork, config: MapVerParams = MapVerParams()):
        """
        Constructor.

        :param network: Lanelet network.
        :param config: Configuration.
        """
        super().__init__(network, config)

        self._pre = Z3Preprocessing(network, config)

        self._verification_z3 = None
        self._map_z3 = None
        self._lanelets_z3 = set()
        self._traffic_signs_z3 = set()
        self._traffic_sign_elements_z3 = set()
        self._traffic_lights_z3 = set()
        self._cycle_elements_z3 = set()
        self._intersections_z3 = set()
        self._incoming_elements_z3 = set()

    @property
    def verification_z3(self) -> DatatypeRef:
        return self._verification_z3

    @property
    def map_z3(self) -> DatatypeRef:
        return self._map_z3

    @property
    def lanelets_z3(self) -> Set[DatatypeRef]:
        return self._lanelets_z3

    @property
    def traffic_signs_z3(self) -> Set[DatatypeRef]:
        return self._traffic_signs_z3

    @property
    def traffic_signs_elements_z3(self) -> Set[DatatypeRef]:
        return self._traffic_sign_elements_z3

    @property
    def traffic_lights_z3(self) -> Set[DatatypeRef]:
        return self._traffic_lights_z3

    @property
    def cycle_elements_z3(self) -> Set[DatatypeRef]:
        return self._cycle_elements_z3

    @property
    def intersections_z3(self) -> Set[DatatypeRef]:
        return self._intersections_z3

    @property
    def incoming_elements_z3(self) -> Set[DatatypeRef]:
        return self._incoming_elements_z3

    def map_verification_paras(self):
        """
        Maps the properties for verification in context of Z3.

        """
        verification_params = self._config.verification

        connection_thresh = verification_params.connection_thresh
        connection_thresh = RealVal("{:.20f}".format(connection_thresh))

        potential_connection_thresh = verification_params.potential_connection_thresh
        potential_connection_thresh = RealVal("{:.20f}".format(potential_connection_thresh))

        verification_z3 = VerificationSort().verification(connection_thresh, potential_connection_thresh)

        self._verification_z3 = verification_z3

    def map_lanelet_network(self):
        """
        Maps supported CommonRoad elements Lanelet, TrafficSign, and TrafficLight to corresponding Z3 datatypes.

        """
        self._map_z3 = self._map_map()

        for lanelet in self._network.lanelets:
            lanelet_z3 = self._map_lanelet(lanelet)
            self._lanelets_z3.add(lanelet_z3)

        for traffic_sign in self._network.traffic_signs:
            traffic_sign_z3 = self._map_traffic_sign(traffic_sign)
            self._traffic_signs_z3.add(traffic_sign_z3)

        for traffic_light in self._network.traffic_lights:
            traffic_light_z3 = self._map_traffic_light(traffic_light)
            self._traffic_lights_z3.add(traffic_light_z3)

        for intersection in self._network.intersections:
            intersection_z3 = self._map_intersection(intersection)
            self._intersections_z3.add(intersection_z3)

    @staticmethod
    def create_seq(elements: List[Any], sort: SortRef, val_func: Callable = None) -> SeqRef:
        """
        Creates a sequence from list of elements.

        :param elements: List of elements.
        :param sort: Type of elements.
        :param val_func: Function for creating Z3 value type of the given sort.
        :return: Reference of created sequence.
        """
        seq = Empty(SeqSort(sort))

        if elements is None:
            return seq

        for e in elements:
            seq = Concat(seq, Unit(e if val_func is None else val_func(e)))

        return seq

    def add_element_z3(self, element_z3: DatatypeRef):
        """
        Adds element to set of elements of specific custom Z3 type.

        :param element_z3: Reference of Z3 element.
        """
        ref_sort = element_z3.sort()
        if ref_sort == LaneletSort():
            self._lanelets_z3.add(element_z3)
        elif ref_sort == TrafficSignSort():
            self._traffic_signs_z3.add(element_z3)
        elif ref_sort == TrafficSignElementSort():
            self._traffic_sign_elements_z3.add(element_z3)
        elif ref_sort == TrafficLightSort():
            self._traffic_lights_z3.add(element_z3)
        elif ref_sort == CycleElementSort():
            self._cycle_elements_z3.add(element_z3)
        elif ref_sort == IntersectionSort():
            self._intersections_z3.add(element_z3)
        elif ref_sort == IncomingElementSort():
            self._incoming_elements_z3.add(element_z3)

    def _map_map(self):
        """
        Maps information about map to datatype MapZ3.

        :return: Map as Z3 instance.
        """
        if self._complete_map_name is None:
            return MapSort().none

        country_id = StringVal(self._complete_map_name.split("_")[0])
        map_name = StringVal(self._complete_map_name.split("_")[1].split("-")[0])
        map_id = self._complete_map_name.split("_")[1].split("-")[1]

        map_z3 = MapSort().map(country_id, map_name, map_id)
        return map_z3

    def _map_lanelet(self, lanelet: Lanelet):
        """
        Maps a lanelet to an instance of a corresponding datatype in Z3.

        :param lanelet: Lanelet.
        :return: Lanelet as Z3 instance.
        """
        if lanelet is None:
            return LaneletSort().none

        lanelet_id = lanelet.lanelet_id
        left_vertices = self._map_polyline(lanelet.left_vertices)
        right_vertices = self._map_polyline(lanelet.right_vertices)
        successor = Z3Mapping.create_seq(lanelet.successor, IntSort(), IntVal)
        predecessor = Z3Mapping.create_seq(lanelet.predecessor, IntSort(), IntVal)
        left_adj = lanelet.adj_left if lanelet.adj_left is not None else -1
        left_adj_type, right_adj_type = self._pre.adjacency_types[lanelet_id]
        left_adj_type = hash(left_adj_type) if left_adj_type is not None else -1
        left_topological_adjs = Z3Mapping.create_seq(list(self._pre.left_topological_adjs.get(lanelet_id)),
                                                     IntSort(), IntVal)
        adj_left_same_dir = lanelet.adj_left_same_direction
        if adj_left_same_dir is None:
            adj_left_same_dir = False
        right_adj = lanelet.adj_right if lanelet.adj_right is not None else -1
        right_adj_type = hash(right_adj_type) if right_adj_type is not None else -1
        right_topological_adjs = Z3Mapping.create_seq(list(self._pre.right_topological_adjs.get(lanelet_id)),
                                                      IntSort(), IntVal)
        adj_right_same_dir = lanelet.adj_right_same_direction
        if adj_right_same_dir is None:
            adj_right_same_dir = False
        types = Z3Mapping.create_seq([hash(l_type.value) for l_type in lanelet.lanelet_type], IntSort(), IntVal)
        is_polylines_intersection = self._pre.polylines_intersections[lanelet_id]
        traffic_signs = Z3Mapping.create_seq(list(lanelet.traffic_signs), IntSort(), IntVal)
        traffic_lights = Z3Mapping.create_seq(list(lanelet.traffic_lights), IntSort(), IntVal)
        stop_line = self._map_stop_line(lanelet.stop_line, lanelet_id)

        lanelet_z3 = LaneletSort().lanelet(lanelet_id, left_vertices, right_vertices, successor, predecessor, left_adj,
                                           left_adj_type, left_topological_adjs, adj_left_same_dir, right_adj,
                                           right_adj_type, right_topological_adjs, adj_right_same_dir, types,
                                           is_polylines_intersection, traffic_signs, traffic_lights, stop_line)

        return lanelet_z3

    def _map_vertex(self, vertex: np.ndarray):
        """
        Maps a vertex to an instance of a corresponding datatype in Z3.

        :param vertex: Vertex in form of [x, y].
        :return: Vertex as Z3 instance.
        """
        if vertex is None:
            return VertexSort().none

        x = vertex[0]
        y = vertex[1]

        vertex_z3 = VertexSort().vertex(RealVal("{:.20f}".format(x)), RealVal("{:.20f}".format(y)))
        return vertex_z3

    def _map_polyline(self, vs: np.ndarray):
        """
        Maps a polyline to an instance of a corresponding datatype in Z3.

        :param vs: Polyline in form of [[x_0, y_0], ..., [x_n, y_n]].
        :return: Polyline as Z3 instance.
        """
        if vs is None:
            return PolylineSort().none

        size = len(vs)
        length = LineString(vs).length
        vertices = 0
        for poly_id, polylines in self._pre.similar_polylines.items():
            for polyline in polylines:
                if vs is polyline:
                    vertices = poly_id
        start_vertex = self._map_vertex(vs[0])
        end_vertex = self._map_vertex(vs[size - 1])
        is_self_intersection = self._pre.self_intersections[str(vs)]

        polyline_z3 = PolylineSort().polyline(size, length, vertices, start_vertex, end_vertex, is_self_intersection)
        return polyline_z3

    def _map_stop_line(self, stop_line: StopLine, lanelet_id: int):
        """
        Maps a stop line to an instance of a corresponding datatype in Z3.

        :param stop_line: Stop line.
        :return: Stop line as Z3 instance.
        """
        if stop_line is None:
            return StopLineSort().none

        left_point = self._map_vertex(stop_line.start)
        left_point_distance = self._pre.stop_line_distances[lanelet_id][0]
        right_point = self._map_vertex(stop_line.end)
        right_point_distance = self._pre.stop_line_distances[lanelet_id][1]
        line_marking = hash(stop_line.line_marking)
        traffic_signs = Z3Mapping.create_seq(list([] if stop_line.traffic_sign_ref is None
                                                  else stop_line.traffic_sign_ref), IntSort(), IntVal)
        traffic_lights = Z3Mapping.create_seq(list([] if stop_line.traffic_light_ref is None
                                                   else stop_line.traffic_light_ref), IntSort(), IntVal)

        stop_line_z3 = StopLineSort().stop_line(left_point, RealVal("{:.20f}".format(left_point_distance)), right_point,
                                                RealVal("{:.20f}".format(right_point_distance)), line_marking,
                                                traffic_signs, traffic_lights)
        return stop_line_z3

    def _map_traffic_sign(self, traffic_sign: TrafficSign):
        """
        Maps a traffic sign to an instance of a corresponding datatype in Z3.

        :param traffic_sign: Traffic sign.
        :return: Traffic sign as Z3 instance.
        """
        if traffic_sign is None:
            return TrafficSignSort().none

        traffic_sign_id = traffic_sign.traffic_sign_id
        traffic_sign_elements = Z3Mapping.create_seq([self._map_traffic_sign_element(traffic_sign_element,
                                                                                     traffic_sign_id)
                                                      for traffic_sign_element in traffic_sign.traffic_sign_elements],
                                                     TrafficSignElementSort())
        first_occurrence = Z3Mapping.create_seq(list(traffic_sign.first_occurrence), IntSort(), IntVal)
        position = self._map_vertex(traffic_sign.position)
        virtual = traffic_sign.virtual

        distances = Z3Mapping.create_seq([TupleSort([IntSort(), RealSort()]).tuple(lanelet_id, RealVal(distance))
                                          for lanelet_id, distance in self._pre.traffic_sign_distances.get(
                                                traffic_sign_id)],
                                         TupleSort([IntSort(), RealSort()]))

        traffic_sign_z3 = TrafficSignSort().traffic_sign(traffic_sign_id, traffic_sign_elements, first_occurrence,
                                                         position, virtual, distances)
        return traffic_sign_z3

    def _map_traffic_sign_element(self, traffic_sign_element: TrafficSignElement, traffic_sign_id: int):
        """
        Maps a traffic sign element to an instance of a corresponding datatype in Z3.

        :param traffic_sign_element: Traffic sign element.
        :param traffic_sign_id: Traffic sign ID.
        :return: Traffic sign element as Z3 instance.
        """
        if traffic_sign_element is None:
            return TrafficSignElementSort().none

        traffic_sign_element_id = hash(traffic_sign_element.traffic_sign_element_id.name.lower())
        additional_values = []
        for v in traffic_sign_element.additional_values:
            try:
                additional_values.append(float(v))
            except ValueError:
                additional_values.append(hash(v))
        additional_values = Z3Mapping.create_seq(additional_values, RealSort(), RealVal)
        related_traffic_sign = traffic_sign_id

        traffic_sign_element_z3 = TrafficSignElementSort().traffic_sign_element(traffic_sign_element_id,
                                                                                additional_values, related_traffic_sign)

        self._traffic_sign_elements_z3.add(traffic_sign_element_z3)

        return traffic_sign_element_z3

    def _map_traffic_light(self, traffic_light: TrafficLight):
        """
        Maps a traffic light to an instance of a corresponding datatype in Z3.

        :param traffic_light: Traffic light.
        :return: Traffic light as Z3 instance.
        """
        if traffic_light is None:
            return TrafficLightSort().none

        traffic_light_id = traffic_light.traffic_light_id
        cycle_elements = Z3Mapping.create_seq([self._map_cycle_element(cycle_element, traffic_light_id)
                                               for cycle_element in traffic_light.traffic_light_cycle.cycle_elements]
                                              if traffic_light.traffic_light_cycle is not None else [],
                                              CycleElementSort())
        position = self._map_vertex(traffic_light.position)
        direction = hash(traffic_light.direction)
        active = traffic_light.active
        time_offset = traffic_light.traffic_light_cycle.time_offset \
            if traffic_light.traffic_light_cycle is not None else 0

        traffic_light_z3 = TrafficLightSort().traffic_light(traffic_light_id, cycle_elements, position, direction,
                                                            active, time_offset)
        return traffic_light_z3

    def _map_cycle_element(self, cycle_element: TrafficLightCycleElement, traffic_light_id: int):
        """
        Maps a cycle element to an instance of a corresponding datatype in Z3.

        :param cycle_element: Cycle element.
        :param traffic_light_id: Traffic light ID.
        :return: Cycle element as Z3 instance.
        """
        if cycle_element is None:
            return CycleElementSort().none

        duration = cycle_element.duration
        state = hash(cycle_element.state)
        related_traffic_light = traffic_light_id

        cycle_element_z3 = CycleElementSort().cycle_element(duration, state, related_traffic_light)

        self._cycle_elements_z3.add(cycle_element_z3)

        return cycle_element_z3

    def _map_intersection(self, intersection: Intersection):
        """
        Maps an intersection to an instance of a corresponding datatype in Z3.

        :param intersection: Intersection.
        :return: Intersection as Z3 instance.
        """
        if intersection is None:
            return IntersectionSort().none

        intersection_id = intersection.intersection_id
        incoming_elements = Z3Mapping.create_seq([self._map_incoming_element(incoming_element, intersection_id)
                                                  for incoming_element in intersection.incomings],
                                                 IncomingElementSort())

        intersection_z3 = IntersectionSort().intersection(intersection_id, incoming_elements)
        return intersection_z3

    def _map_incoming_element(self, incoming_group: IncomingGroup, intersection_id: int):
        """
        Maps an incoming element to an instance of a corresponding datatype in Z3.

        :param incoming_group: Incoming group element.
        :param intersection_id: Intersection ID.
        :return: Incoming element as Z3 instance.
        """
        if incoming_group is None:
            return IncomingElementSort().none

        incoming_element_id = incoming_group.incoming_id
        incoming_lanelets = Z3Mapping.create_seq(list(incoming_group.incoming_lanelets), IntSort(), IntVal)
        outgoings_right = Z3Mapping.create_seq(list(incoming_group.outgoing_right), IntSort(), IntVal)
        outgoings_straight = Z3Mapping.create_seq(list(incoming_group.outgoing_straight), IntSort(), IntVal)
        outgoings_left = Z3Mapping.create_seq(list(incoming_group.outgoing_left), IntSort(), IntVal)

        incoming_element_z3 = IncomingElementSort().incoming_element(incoming_element_id, incoming_lanelets,
                                                                     outgoings_right, outgoings_straight,
                                                                     outgoings_left, intersection_id)

        self._incoming_elements_z3.add(incoming_element_z3)

        return incoming_element_z3


class Z3Preprocessing(Preprocessing):
    """
    The class is responsible for preprocessing map properties mapped to Z3 instances.
    """

    def __init__(self, lanelet_network: LaneletNetwork, config: MapVerParams):
        super().__init__(lanelet_network, config)

        self._self_intersections = None
        self._polylines_intersections = None
        self._stop_line_distances = None
        self._traffic_sign_distances = None

        self._self_intersections = self.self_intersections

        self._polylines_intersections = self.polylines_intersections

        self._stop_line_distances = self.stop_line_distances

        self._traffic_sign_distances = self.traffic_sign_distances

    @property
    def self_intersections(self) -> Dict[str, bool]:
        """
        Checks self-intersections for all polylines in the map and collects the computed information.

        :return: Booleans indicate whether the polylines self-intersect.
        """
        if self._self_intersections is None:
            self._self_intersections = {}
            for lan in self._network.lanelets:
                l_vs = lan.left_vertices
                self._self_intersections[str(l_vs)] = Z3Preprocessing.is_polyline_self_intersection(l_vs)
                r_vs = lan.right_vertices
                self._self_intersections[str(r_vs)] = Z3Preprocessing.is_polyline_self_intersection(r_vs)
        return self._self_intersections

    @property
    def polylines_intersections(self) -> Dict[int, bool]:
        """
        Checks intersections between two polylines for all polylines in the map and collects the computed information.

        :return: Booleans indicate whether two polylines intersect each other.
        """
        if self._polylines_intersections is None:
            self._polylines_intersections = {}
            for lan in self._network.lanelets:
                left_polyline = lan.left_vertices
                right_polyline = lan.right_vertices
                self._polylines_intersections[lan.lanelet_id] = self.is_polylines_intersection(left_polyline,
                                                                                               right_polyline)
        return self._polylines_intersections

    @property
    def stop_line_distances(self) -> Dict[int, Tuple[float, float]]:
        """
        Checks whether the two points of stop line lie on the corresponding polylines.

        :return: Distances between two points of stop line and the corresponding polylines.
        """
        if self._stop_line_distances is None:
            self._stop_line_distances = {}

            for lanelet in self._network.lanelets:
                if lanelet.stop_line is not None:
                    stop_line = lanelet.stop_line
                    if stop_line.start is None:
                        left_distance = 0.
                    else:
                        left_distance = LineString(lanelet.left_vertices).distance(Point(stop_line.start))
                    if stop_line.end is None:
                        right_distance = 0.
                    else:
                        right_distance = LineString(lanelet.right_vertices).distance(Point(stop_line.end))
                    self._stop_line_distances[lanelet.lanelet_id] = (left_distance, right_distance)

        return self._stop_line_distances

    @property
    def traffic_sign_distances(self) -> Dict[int, List[Tuple[int, float]]]:
        """
        Computes the distances between lanelets and referred traffic signs.

        :return: Distances between lanelets and traffic signs.
        """
        if self._traffic_sign_distances is None:
            self._traffic_sign_distances = {}

            for traffic_sign in self._network.traffic_signs:
                distances = []
                for lanelet in self._network.lanelets:
                    if traffic_sign.traffic_sign_id in lanelet.traffic_signs:
                        if traffic_sign.position is None:
                            distance = 0.0
                        else:
                            distance = lanelet.polygon.shapely_object.distance(Point(traffic_sign.position))
                        distances.append((lanelet.lanelet_id, distance))
                self._traffic_sign_distances.update({traffic_sign.traffic_sign_id: distances})

        return self._traffic_sign_distances

    @staticmethod
    def is_polyline_self_intersection(polyline: np.ndarray) -> bool:
        """
        Checks whether the polyline intersects itself.

        :param polyline: Polyline.
        :return: Boolean indicates whether a polyline is self-intersected.
        """
        line = [(x, y) for x, y in polyline]
        line_string = LineString(line)

        return not line_string.is_simple

    @staticmethod
    def is_polylines_intersection(polyline_0: np.ndarray, polyline_1: np.ndarray) -> bool:
        """
        Checks whether two polylines intersect each other.

        :param polyline_0: First polyline.
        :param polyline_1: Second polyline.
        :return: Boolean indicates whether the two polylines intersect each other.
        """
        line_0 = [(x, y) for x, y in polyline_0]
        line_1 = [(x, y) for x, y in polyline_1]

        line_string_0 = LineString(line_0)
        line_string_1 = LineString(line_1)

        result = line_string_0.intersection(line_string_1)

        return not result.is_empty


def VerificationSort():
    """
    Creates a Z3 sort that represents a verification .

    :return: Reference of created datatype sort.
    """
    VerificationZ3 = Datatype('Verification')
    VerificationZ3.declare('verification',
                         ('connection_thresh', RealSort()),
                         ('potential_connection_thresh', RealSort()))
    VerificationZ3.declare('none')
    VerificationZ3 = VerificationZ3.create()

    return VerificationZ3


def MapSort():
    """
    Creates a Z3 sort that represents a map.

    :return: Reference of created datatype sort.
    """
    MapZ3 = Datatype('Map')
    MapZ3.declare('map',
                  ('country_id', StringSort()),
                  ('map_name', StringSort()),
                  ('map_id', IntSort()))
    MapZ3.declare('none')
    MapZ3 = MapZ3.create()

    return MapZ3


def LaneletSort():
    """
    Creates a Z3 sort that represents a lanelet.

    :return: Reference of created datatype sort.
    """
    LaneletZ3 = Datatype('Lanelet')
    LaneletZ3.declare('lanelet',
                      ('lanelet_id', IntSort()),
                      ('left_polyline', PolylineSort()),
                      ('right_polyline', PolylineSort()),
                      ('successor', SeqSort(IntSort())),
                      ('predecessor', SeqSort(IntSort())),
                      ('left_adj', IntSort()),
                      ('left_adj_type', IntSort()),
                      ('left_topological_adjs', SeqSort(IntSort())),
                      ('adj_left_same_dir', BoolSort()),
                      ('right_adj', IntSort()),
                      ('right_adj_type', IntSort()),
                      ('right_topological_adjs', SeqSort(IntSort())),
                      ('adj_right_same_dir', BoolSort()),
                      ('types', SeqSort(IntSort())),
                      ('is_polylines_intersection', BoolSort()),
                      ('traffic_signs', SeqSort(IntSort())),
                      ('traffic_lights', SeqSort(IntSort())),
                      ('stop_line', StopLineSort()))
    LaneletZ3.declare('none')
    LaneletZ3 = LaneletZ3.create()

    return LaneletZ3


def VertexSort():
    """
    Creates a Z3 sort that represents a vertex.

    :return: Reference of created datatype sort.
    """
    VertexZ3 = Datatype('Vertex')
    VertexZ3.declare('vertex',
                     ('x', RealSort()),
                     ('y', RealSort()))
    VertexZ3.declare('none')
    VertexZ3 = VertexZ3.create()

    return VertexZ3


def PolylineSort():
    """
    Creates a Z3 sort that represents a polyline.

    :return: Reference of created datatype sort.
    """
    PolylineZ3 = Datatype('Polyline')
    PolylineZ3.declare('polyline',
                       ('size', IntSort()),
                       ('length', RealSort()),
                       ('vertices', IntSort()),
                       ('start_vertex', VertexSort()),
                       ('end_vertex', VertexSort()),
                       ('is_self_intersection', BoolSort()))
    PolylineZ3.declare('none')
    PolylineZ3 = PolylineZ3.create()

    return PolylineZ3


def StopLineSort():
    """
    Creates a Z3 sort that represents a stop line.

    :return: Reference of created datatype sort.
    """
    StopLineZ3 = Datatype('StopLine')
    StopLineZ3.declare('stop_line',
                       ('left_point', VertexSort()),
                       ('left_point_distance', RealSort()),
                       ('right_point', VertexSort()),
                       ('right_point_distance', RealSort()),
                       ('line_marking', IntSort()),
                       ('traffic_signs', SeqSort(IntSort())),
                       ('traffic_lights', SeqSort(IntSort())))
    StopLineZ3.declare('none')
    StopLineZ3 = StopLineZ3.create()

    return StopLineZ3


def TrafficSignSort():
    """
    Creates a Z3 sort that represents a traffic sign.

    :return: Reference of created datatype sort.
    """
    TrafficSignZ3 = Datatype('TrafficSign')
    TrafficSignZ3.declare('traffic_sign',
                          ('traffic_sign_id', IntSort()),
                          ('traffic_sign_elements', SeqSort(TrafficSignElementSort())),
                          ('first_occurrence', SeqSort(IntSort())),
                          ('position', VertexSort()),
                          ('virtual', BoolSort()),
                          ('distances', SeqSort(TupleSort([IntSort(), RealSort()]))))
    TrafficSignZ3.declare('none')
    TrafficSignZ3 = TrafficSignZ3.create()

    return TrafficSignZ3


def TrafficSignElementSort():
    """
    Creates a Z3 sort that represents a traffic sign element.

    :return: Reference of created datatype sort.
    """
    TrafficSignElementZ3 = Datatype('TrafficSignElement')
    TrafficSignElementZ3.declare('traffic_sign_element',
                                 ('traffic_sign_element_id', IntSort()),
                                 ('additional_values', SeqSort(RealSort())),
                                 ('related_traffic_sign', IntSort()))
    TrafficSignElementZ3.declare('none')
    TrafficSignElementZ3 = TrafficSignElementZ3.create()

    return TrafficSignElementZ3


def TrafficLightSort():
    """
    Creates a Z3 sort that represents a traffic light.

    :return: Reference of created datatype sort.
    """
    TrafficLightZ3 = Datatype('TrafficLight')
    TrafficLightZ3.declare('traffic_light',
                           ('traffic_light_id', IntSort()),
                           ('cycle_elements', SeqSort(CycleElementSort())),
                           ('position', VertexSort()),
                           ('direction', IntSort()),
                           ('active', BoolSort()),
                           ('time_offset', IntSort()))
    TrafficLightZ3.declare('none')
    TrafficLightZ3 = TrafficLightZ3.create()

    return TrafficLightZ3


def CycleElementSort():
    """
    Creates a Z3 sort that represents a cycle element.

    :return: Reference of created datatype sort.
    """
    CycleElementZ3 = Datatype('CycleElement')
    CycleElementZ3.declare('cycle_element',
                           ('duration', IntSort()),
                           ('state', IntSort()),
                           ('related_traffic_light', IntSort()))
    CycleElementZ3.declare('none')
    CycleElementZ3 = CycleElementZ3.create()

    return CycleElementZ3


def IntersectionSort():
    """
    Creates a Z3 sort that represents an intersection.

    :return: Reference of created datatype sort.
    """
    IntersectionZ3 = Datatype('Intersection')
    IntersectionZ3.declare('intersection',
                           ('intersection_id', IntSort()),
                           ('incoming_elements', SeqSort(IncomingElementSort())))
    IntersectionZ3.declare('none')
    IntersectionZ3 = IntersectionZ3.create()

    return IntersectionZ3


def IncomingElementSort():
    """
    Creates a Z3 sort that represents an incoming element.

    :return: Reference of created datatype sort.
    """
    IncomingElementZ3 = Datatype('IncomingElement')
    IncomingElementZ3.declare('incoming_element',
                              ('incoming_element_id', IntSort()),
                              ('incoming_lanelets', SeqSort(IntSort())),
                              ('outgoings_right', SeqSort(IntSort())),
                              ('outgoings_straight', SeqSort(IntSort())),
                              ('outgoings_left', SeqSort(IntSort())),
                              ('related_intersection', IntSort()))
    IncomingElementZ3.declare('none')
    IncomingElementZ3 = IncomingElementZ3.create()

    return IncomingElementZ3


def TupleSort(sorts: List[ExprRef]):
    """
    Creates a Z3 sort that represents a tuple.

    :return: Reference of created datatype sort.
    """
    TupleZ3 = Datatype('Tuple')
    TupleZ3.declare('tuple', *[('project_%d' % i, sort) for i, sort in enumerate(sorts)])
    TupleZ3.declare('none')
    TupleZ3 = TupleZ3.create()

    return TupleZ3
