import os.path
import pathlib
from typing import List
import dataclasses
import os
import yaml


@dataclasses.dataclass
class FormulaManagerOWL:
    rule_sets: List = dataclasses.field(default_factory=list)

    def __post_init__(self):
        file_names = ['lanelet_rules.yaml']
        for file_name in file_names:
            file_path = os.path.join(pathlib.Path(__file__).parent.resolve(), '', file_name)

            rule_file = open(file_path, 'r')
            self.rule_sets.append(yaml.safe_load(rule_file)['rule_pool']['rules'])
