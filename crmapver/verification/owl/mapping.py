import os.path
import pathlib
from typing import Union
import os

import numpy as np
from commonroad.scenario.lanelet import LaneletNetwork, Lanelet, StopLine
from commonroad.scenario.traffic_sign import TrafficSign
from commonroad.scenario.traffic_light import TrafficLight
from owlready2 import World, AllDifferent, close_world
from shapely import LineString

from crmapver.config import MapVerParams
from crmapver.verification.mapping import Mapping


class OWLMapping(Mapping):
    """
    The class is responsible for mapping the CommonRoad elements to OWL individuals.
    """

    def __init__(self, network: LaneletNetwork, config: MapVerParams = MapVerParams()):
        """
        Constructor.

        :param network: Lanelet network.
        :param config: Configuration.
        """
        super().__init__(network, config)

        ontology_path = os.path.join(pathlib.Path(__file__).parent.resolve(), 'ontology.rdf')
        self._ontology = World().get_ontology(ontology_path).load()

        self._concept_individuals = {}
        self._role_individuals = {}

    @property
    def ontology(self):
        return self._ontology

    def map_lanelet_network(self):
        """
        Maps all elements of a lanelet network to OWL individuals.
        """
        self._map_concepts()
        self._map_roles()
        self._map_data_properties()

        individuals = list(self._concept_individuals.values())
        AllDifferent(individuals)
        for individual in individuals:
            close_world(individual)

    def map_verification_paras(self):
        """
        Maps all verification parameters to OWL individuals.
        """
        pass

    def _map_concepts(self):
        """
        Maps all elements of a lanelet network to OWL individuals of concepts.
        """
        for lanelet in self._network.lanelets:
            self._map_lanelet(lanelet)

            for polyline in [lanelet.left_vertices, lanelet.right_vertices]:
                self._map_polyline(polyline)
                for vertex in polyline:
                    self._map_vertex(vertex)

            if lanelet.adj_left:
                self._map_reference(lanelet.adj_left)
            if lanelet.adj_right:
                self._map_reference(lanelet.adj_right)
            for pred_id in lanelet.predecessor:
                self._map_reference(pred_id)
            for suc_id in lanelet.successor:
                self._map_reference(suc_id)

            for sign_id in lanelet.traffic_signs:
                self._map_reference(sign_id)
            for light_id in lanelet.traffic_lights:
                self._map_reference(light_id)

            if lanelet.stop_line:
                self._map_stop_line(lanelet.stop_line)

        for traffic_sign in self._network.traffic_signs:
            self._map_traffic_sign(traffic_sign)

        for traffic_light in self._network.traffic_lights:
            self._map_traffic_light(traffic_light)

    def _map_roles(self):
        """
        Maps all relations between elements of a lanelet network to OWL individuals of roles.
        """
        for lanelet in self._network.lanelets:
            self._map_has_left_polyline(lanelet, lanelet.left_vertices)
            self._map_has_right_polyline(lanelet, lanelet.right_vertices)

            for polyline in [lanelet.left_vertices, lanelet.right_vertices]:
                for vertex in polyline:
                    self._map_has_vertex(polyline, vertex)
                self._map_has_start_vertex(polyline, polyline[0])
                self._map_has_end_vertex(polyline, polyline[-1])

            for pred_id in lanelet.predecessor:
                self._map_has_predecessor(lanelet, pred_id)
            for suc_id in lanelet.successor:
                self._map_has_successor(lanelet, suc_id)
            if lanelet.adj_left:
                self._map_has_left_adjacency(lanelet, lanelet.adj_left)
            if lanelet.adj_right:
                self._map_has_right_adjacency(lanelet, lanelet.adj_right)

            for sign_id in lanelet.traffic_signs:
                self._map_has_traffic_sign(lanelet, sign_id)

            for light_id in lanelet.traffic_lights:
                self._map_has_traffic_light(lanelet, light_id)

            if lanelet.stop_line:
                if lanelet.stop_line.traffic_sign_ref is not None:
                    for sign_id in lanelet.stop_line.traffic_sign_ref:
                        self._map_has_traffic_sign(lanelet.stop_line, sign_id)
                if lanelet.stop_line.traffic_light_ref is not None:
                    for light_id in lanelet.stop_line.traffic_light_ref:
                        self._map_has_traffic_light(lanelet.stop_line, light_id)

    def _map_data_properties(self):
        """
        Maps all data attributes of the elements of a lanelet network to OWL individuals of data properties.
        """
        for lanelet in self._network.lanelets:
            self._map_has_id(lanelet, lanelet.lanelet_id)

            self._map_is_polylines_intersection(lanelet)

            for polyline in [lanelet.left_vertices, lanelet.right_vertices]:
                self._map_is_self_intersection(polyline)

            refs = lanelet.predecessor + lanelet.successor
            if lanelet.adj_left:
                refs.append(lanelet.adj_left)
            if lanelet.adj_right:
                refs.append(lanelet.adj_right)
            for ref in refs:
                self._map_has_value(ref, ref)
                self._map_is_valid(ref, self._network.find_lanelet_by_id(ref) is not None)

            for sign_ref in lanelet.traffic_signs:
                self._map_has_value(sign_ref, sign_ref)
                self._map_is_valid(sign_ref, self._network.find_traffic_sign_by_id(sign_ref) is not None)

            for light_ref in lanelet.traffic_lights:
                self._map_has_value(light_ref, light_ref)
                self._map_is_valid(light_ref, self._network.find_traffic_light_by_id(light_ref) is not None)

            for polyline in [lanelet.left_vertices, lanelet.right_vertices]:
                self._map_has_number(polyline, len(polyline))

            if lanelet.stop_line:
                if lanelet.stop_line.traffic_sign_ref is not None:
                    for sign_ref in lanelet.stop_line.traffic_sign_ref:
                        self._map_has_value(sign_ref, sign_ref)
                        self._map_is_valid(sign_ref, self._network.find_traffic_light_by_id(sign_ref) is not None)
                if lanelet.stop_line.traffic_light_ref is not None:
                    for light_ref in lanelet.stop_line.traffic_light_ref:
                        self._map_has_value(light_ref, light_ref)
                        self._map_is_valid(light_ref, self._network.find_traffic_light_by_id(light_ref) is not None)

    # ---------------- concepts ----------------

    def _map_lanelet(self, lanelet: Lanelet):
        """
        Maps the lanelet to an OWL individual of the concept Lanelet.

        :param lanelet: Lanelet.
        """
        name = OWLMapping._lanelet_name(lanelet)

        self._concept_individuals[name] = self._ontology.Lanelet(name)

    def _map_polyline(self, polyline: np.ndarray):
        """
        Maps the polyline to an OWL individual of the concept Polyline.

        :param polyline: Polyline.
        """
        name = OWLMapping._polyline_name(polyline)

        self._concept_individuals[name] = self._ontology.Polyline(name)

    def _map_vertex(self, vertex: np.ndarray):
        """
        Maps the vertex to an OWL individual of the concept Vertex.

        :param vertex: Vertex.
        """
        name = OWLMapping._vertex_name(vertex)

        self._concept_individuals[name] = self._ontology.Vertex(name)

    def _map_reference(self, ref: int):
        """
        Maps the reference to an OWL individual of the concept Reference.

        :param ref: Reference.
        """
        name = OWLMapping._reference_name(ref)

        self._concept_individuals[name] = self._ontology.Reference(name)

    def _map_traffic_sign(self, traffic_sign: TrafficSign):
        """
        Maps the reference to an OWL individual of the concept TrafficSign.

        :param traffic_sign: Traffic sign.
        """
        name = OWLMapping._traffic_sign_name(traffic_sign)

        self._concept_individuals[name] = self._ontology.TrafficSign(name)

    def _map_traffic_light(self, traffic_light: TrafficLight):
        """
        Maps the reference to an OWL individual of the concept TrafficLight.

        :param traffic_light: Traffic light.
        """
        name = OWLMapping._traffic_light_name(traffic_light)

        self._concept_individuals[name] = self._ontology.TrafficLight(name)

    def _map_stop_line(self, stop_line: StopLine):
        """
        Maps the reference to an OWL individual of the concept StopLine.

        :param stop_line: Stop line.
        """
        name = OWLMapping._stop_line_name(stop_line)

        self._concept_individuals[name] = self._ontology.StopLine(name)

    # ---------------- roles ----------------

    def _map_has_left_polyline(self, lanelet: Lanelet, left_polyline: np.ndarray):
        """
        Maps the relation between the lanelet and left polyline to an OWL individual of
        the role has_left_polyline.

        :param lanelet: Lanelet.
        :param left_polyline: Left polyline.
        """
        name = OWLMapping._lanelet_polyline_name(lanelet, left_polyline)

        lanelet_individual = self._concept_individuals[OWLMapping._lanelet_name(lanelet)]
        left_polyline_individual = self._concept_individuals[OWLMapping._polyline_name(left_polyline)]

        self._role_individuals[name] = lanelet_individual.has_left_polyline.append(left_polyline_individual)

    def _map_has_right_polyline(self, lanelet: Lanelet, right_polyline: np.ndarray):
        """
        Maps the relation between the lanelet and the right polyline to an OWL individual of
        the role has_right_polyline.

        :param lanelet: Lanelet.
        :param right_polyline: Right polyline.
        """
        name = OWLMapping._lanelet_polyline_name(lanelet, right_polyline)

        lanelet_individual = self._concept_individuals[OWLMapping._lanelet_name(lanelet)]
        right_polyline_individual = self._concept_individuals[OWLMapping._polyline_name(right_polyline)]

        self._role_individuals[name] = lanelet_individual.has_right_polyline.append(right_polyline_individual)

    def _map_has_vertex(self, polyline: np.ndarray, vertex: np.ndarray):
        """
        Maps the relation between the polyline and the vertex to an OWL individual of
        the role has_vertex.

        :param polyline: Polyline.
        :param vertex: Vertex.
        """
        name = OWLMapping._polyline_vertex_name(polyline, vertex)

        polyline_individual = self._concept_individuals[OWLMapping._polyline_name(polyline)]
        vertex_individual = self._concept_individuals[OWLMapping._vertex_name(vertex)]

        self._role_individuals[name] = polyline_individual.has_vertex.append(vertex_individual)

    def _map_has_predecessor(self, lanelet: Lanelet, pred_ref: int):
        """
        Maps the relation between the lanelet and the predecessor reference to an OWL individual of
        the role has_predecessor.

        :param lanelet: Lanelet.
        :param pred_ref: Predecessor reference.
        """
        name = OWLMapping._lanelet_reference_name(lanelet, pred_ref)

        lanelet_individual = self._concept_individuals[OWLMapping._lanelet_name(lanelet)]
        pred_reference_individual = self._concept_individuals[OWLMapping._reference_name(pred_ref)]

        self._role_individuals[name] = lanelet_individual.has_predecessor.append(pred_reference_individual)

    def _map_has_successor(self, lanelet: Lanelet, suc_ref: int):
        """
        Maps the relation between the lanelet and the successor reference to an OWL individual of
        the role has_successor.

        :param lanelet: Lanelet.
        :param suc_ref: Successor reference.
        """
        name = OWLMapping._lanelet_reference_name(lanelet, suc_ref)

        lanelet_individual = self._concept_individuals[OWLMapping._lanelet_name(lanelet)]
        suc_reference_individual = self._concept_individuals[OWLMapping._reference_name(suc_ref)]

        self._role_individuals[name] = lanelet_individual.has_successor.append(suc_reference_individual)

    def _map_has_left_adjacency(self, lanelet: Lanelet, left_adj_ref: int):
        """
        Maps the relation between the lanelet and the left adjacent reference to an OWL individual
        of the role has_left_adjacency.

        :param lanelet: Lanelet.
        :param left_adj_ref: Left adjacent reference.
        """
        name = OWLMapping._lanelet_reference_name(lanelet, left_adj_ref)

        lanelet_individual = self._concept_individuals[OWLMapping._lanelet_name(lanelet)]
        left_adj_reference_individual = self._concept_individuals[OWLMapping._reference_name(left_adj_ref)]

        self._role_individuals[name] = lanelet_individual.has_left_adjacency.append(left_adj_reference_individual)

    def _map_has_right_adjacency(self, lanelet: Lanelet, right_adj_ref: int):
        """
        Maps the relation between the lanelet and the right adjacent reference to an OWL individual
        of the role has_right_adjacency.

        :param lanelet: Lanelet.
        :param right_adj_ref: Right adjacent reference.
        """
        name = OWLMapping._lanelet_reference_name(lanelet, right_adj_ref)

        lanelet_individual = self._concept_individuals[OWLMapping._lanelet_name(lanelet)]
        right_adj_reference_individual = self._concept_individuals[OWLMapping._reference_name(right_adj_ref)]

        self._role_individuals[name] = lanelet_individual.has_right_adjacency.append(right_adj_reference_individual)

    def _map_has_start_vertex(self, polyline: np.ndarray, start_vertex: np.ndarray):
        """
        Maps the relation between the polyline and the start vertex to an OWL individual of the role has_start_vertex.

        :param polyline: Polyline.
        :param start_vertex: Start vertex.
        """
        name = OWLMapping._polyline_vertex_name(polyline, start_vertex)

        polyline_individual = self._concept_individuals[OWLMapping._polyline_name(polyline)]
        start_vertex_individual = self._concept_individuals[OWLMapping._vertex_name(start_vertex)]

        self._role_individuals[name] = polyline_individual.has_start_vertex.append(start_vertex_individual)

    def _map_has_end_vertex(self, polyline: np.ndarray, end_vertex: np.ndarray):
        """
        Maps the relation between the polyline and the end vertex to an OWL individual of
        the role has_end_vertex.

        :param polyline: Polyline.
        :param end_vertex: End vertex.
        """
        name = OWLMapping._polyline_vertex_name(polyline, end_vertex)

        polyline_individual = self._concept_individuals[OWLMapping._polyline_name(polyline)]
        end_vertex_individual = self._concept_individuals[OWLMapping._vertex_name(end_vertex)]

        self._role_individuals[name] = polyline_individual.has_end_vertex.append(end_vertex_individual)

    def _map_has_traffic_sign(self, domain: Union[Lanelet, StopLine], ref: int):
        """
        Maps the relation between the lanelet or stop line as domain and the traffic sign reference to
        an OWL individual of the role has_traffic_sign.

        :param domain: Lanelet or stop line as domain.
        :param ref: Reference.
        """
        if isinstance(domain, Lanelet):
            name = OWLMapping._lanelet_reference_name(domain, ref)
            domain_individual = self._concept_individuals[OWLMapping._lanelet_name(domain)]
        else:
            name = OWLMapping._stop_line_reference_name(domain, ref)
            domain_individual = self._concept_individuals[OWLMapping._stop_line_name(domain)]

        reference_individual = self._concept_individuals[OWLMapping._reference_name(ref)]

        self._role_individuals[name] = domain_individual.has_traffic_sign.append(reference_individual)

    def _map_has_traffic_light(self, domain: Union[Lanelet, StopLine], ref: int):
        """
        Maps the relation between the lanelet or stop line as domain and the traffic light reference to
        an OWL individual of the role has_traffic_light.

        :param domain: Lanelet or stop line as domain.
        :param ref: Reference.
        """
        if isinstance(domain, Lanelet):
            name = OWLMapping._lanelet_reference_name(domain, ref)
            domain_individual = self._concept_individuals[OWLMapping._lanelet_name(domain)]
        else:
            name = OWLMapping._stop_line_reference_name(domain, ref)
            domain_individual = self._concept_individuals[OWLMapping._stop_line_name(domain)]

        reference_individual = self._concept_individuals[OWLMapping._reference_name(ref)]

        self._role_individuals[name] = domain_individual.has_traffic_light.append(reference_individual)

    def _map_has_stop_line(self, lanelet: Lanelet, stop_line: StopLine):
        """
        Maps the relation between the lanelet and the stop line to an OWL individual of the role has_stop_line.

        :param lanelet: Lanelet.
        :param stop_line: Stop line.
        """
        name = OWLMapping._lanelet_stop_line_name(lanelet, stop_line)

        lanelet_individual = self._concept_individuals[OWLMapping._lanelet_name(lanelet)]
        stop_line_individual = self._concept_individuals[OWLMapping._stop_line_name(stop_line)]

        self._concept_individuals[name] = lanelet_individual.has_stop_line(stop_line_individual)

    # ---------------- data properties ----------------

    def _map_is_valid(self, ref: int, valid: bool):
        """
        Maps the data attribute between the reference and the validity property to an OWL individual of
        the data property is_valid.

        :param ref: Reference.
        :param valid: Boolean indicates whether the reference is valid.
        """
        reference_individual = self._concept_individuals[OWLMapping._reference_name(ref)]

        reference_individual.is_valid.append(valid)

    def _map_has_value(self, ref: int, value: int):
        """
        Maps the data attribute between the reference and the value to an OWL individual of
        the data property has_value.

        :param ref: Reference.
        :param value: Value.
        """
        reference_individual = self._concept_individuals[OWLMapping._reference_name(ref)]

        reference_individual.has_value.append(value)

    def _map_has_id(self, lanelet: Lanelet, id: int):
        """
        Maps the data attribute between the lanelet and the ID to an OWL individual of
        the data property has_id.

        :param lanelet: Lanelet.
        :param id: ID.
        """
        lanelet_individual = self._concept_individuals[OWLMapping._lanelet_name(lanelet)]

        lanelet_individual.has_id.append(str(id))

    def _map_has_number(self, polyline: np.ndarray, num: int):
        """
        Maps the data attribute between the polyline and the number of vertices to an OWL individual of
        the data property has_number.

        :param polyline: Polyline.
        :param num: Number of vertices.
        """
        polyline_individual = self._concept_individuals[OWLMapping._polyline_name(polyline)]

        polyline_individual.has_number.append(num)

    def _map_is_self_intersection(self, polyline: np.ndarray):
        """
        Maps the data attribute of polyline to an OWL individual of the data property is_self_intersection.

        :param polyline: Polyline.
        """
        self_intersected = not LineString([(x, y) for x, y in polyline]).is_simple

        polyline_individual = self._concept_individuals[OWLMapping._polyline_name(polyline)]

        polyline_individual.is_self_intersection.append(self_intersected)

    def _map_is_polylines_intersection(self, lanelet: Lanelet):
        """
        Maps the data attribute of lanelet to an OWL individual of the data property is_polylines_intersection.

        :param lanelet: Lanelet.
        """
        line_string_0 = LineString([(x, y) for x, y in lanelet.left_vertices])
        line_string_1 = LineString([(x, y) for x, y in lanelet.right_vertices])

        intersected = not line_string_0.intersection(line_string_1).is_empty

        lanelet_individual = self._concept_individuals[OWLMapping._lanelet_name(lanelet)]

        lanelet_individual.is_polylines_intersection.append(intersected)

    # ---------------- concept names ----------------

    @staticmethod
    def _lanelet_name(lanelet: Lanelet) -> str:
        """
        Creates a name for the OWL individual of the concept Lanelet.

        :param lanelet: Lanelet.
        :return: Name.
        """
        return 'L' + str(hash(lanelet))

    @staticmethod
    def _polyline_name(polyline: np.ndarray) -> str:
        """
        Creates a name for the OWL individual of the concept Polyline.

        :param polyline: Polyline.
        :return: Name.
        """
        return 'P' + str(hash(str(polyline)))

    @staticmethod
    def _vertex_name(vertex: np.ndarray) -> str:
        """
        Creates a name for the OWL individual of the concept Vertex.

        :param vertex: Vertex.
        :return: Name.
        """
        return 'V' + str(hash(str(vertex)))

    @staticmethod
    def _reference_name(ref: int) -> str:
        """
        Creates a name for the OWL individual of the concept Reference.

        :param ref: Reference.
        :return: Name.
        """
        return 'R' + str(hash(ref))

    @staticmethod
    def _traffic_sign_name(traffic_sign: TrafficSign) -> str:
        """
        Creates a name for the OWL individual of the concept TrafficSign.

        :param traffic_sign: Traffic sign.
        :return: Name.
        """
        return 'TS' + str(hash(traffic_sign))

    @staticmethod
    def _traffic_light_name(traffic_light: TrafficLight) -> str:
        """
        Creates a name for the OWL individual of the concept TrafficLight.

        :param traffic_light: Traffic light.
        :return: Name.
        """
        return 'TL' + str(hash(traffic_light))

    @staticmethod
    def _stop_line_name(stop_line: StopLine) -> str:
        """
        Creates a name for the OWL individual of the concept StopLine.

        :param stop_line Stop line.
        :return: Name.
        """
        return 'SL' + str(hash(stop_line))

    # ---------------- role names ----------------

    @staticmethod
    def _lanelet_polyline_name(lanelet: Lanelet, polyline: np.ndarray) -> str:
        """
        Creates a name for an individual of a role between the concepts Lanelet and Polyline.

        :param lanelet: Lanelet.
        :param polyline: Polyline.
        :return: Name.
        """
        return OWLMapping._lanelet_name(lanelet) + OWLMapping._polyline_name(polyline)

    @staticmethod
    def _polyline_vertex_name(polyline: np.ndarray, vertex: np.ndarray) -> str:
        """
        Creates a name for the OWL individual of the role between the concepts Polyline and Vertex.

        :param polyline: Polyline.
        :param vertex: Vertex.
        :return: Name.
        """
        return OWLMapping._polyline_name(polyline) + OWLMapping._vertex_name(vertex)

    @staticmethod
    def _lanelet_reference_name(lanelet: Lanelet, ref: int) -> str:
        """
        Creates a name for the OWL individual of the role between the concepts Lanelet and Reference.

        :param lanelet: Lanelet.
        :param ref: Reference.
        :return: Name.
        """
        return OWLMapping._lanelet_name(lanelet) + OWLMapping._reference_name(ref)

    @staticmethod
    def _lanelet_stop_line_name(lanelet: Lanelet, stop_line: StopLine) -> str:
        """
        Creates a name for the OWL individual of the role between the concepts Lanelet and StopLine.

        :param lanelet: Lanelet.
        :param stop_line: Stop line.
        :return: Name.
        """
        return OWLMapping._lanelet_name(lanelet) + OWLMapping._stop_line_name(stop_line)

    @staticmethod
    def _stop_line_reference_name(stop_line: StopLine, ref: int) -> str:
        """
        Creates a name for the OWL individual of the role between the concepts StopLine and Reference.

        :param stop_line: Stop line.
        :param ref: Reference.
        :return: Name.
        """
        return OWLMapping._stop_line_name(stop_line) + OWLMapping._reference_name(ref)
