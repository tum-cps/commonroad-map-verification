from typing import List
import owlready2
from owlready2 import Imp, sync_reasoner_pellet

from crmapver.config import VerificationParams
from crmapver.verification.formula_ids import FormulaID, extract_formula_ids_from_strings
from crmapver.verification.owl.formulas.formula_manager import FormulaManagerOWL
from crmapver.verification.owl.mapping import OWLMapping
from crmapver.verification.satisfaction import VerificationChecker, InvalidStates


class OWLVerificationChecker(VerificationChecker):
    """
    The class is responsible for solving the desired OWL/SWRL formulas. The inferred invalid states are extracted and
    returned.
    """

    def __init__(self, mapping: OWLMapping, formula_ids: List[FormulaID] = None):
        """
        Constructor.

        :param mapping: OWL mapping.
        :param formula_ids: Formula IDs.
        """
        super().__init__(mapping, formula_ids)

    def check_validity(self, config: VerificationParams, manager_results: List[InvalidStates]):
        """
        Checks the validity of a map. The ontology is reasoned based on the defined concepts, roles, and
        OWL/SWRL rules.

        :param config: Verification config parameters.
        :param manager_results: Manager list.
        """
        self._load_rules(config.formula_manager)

        owlready2.reasoning.JAVA_MEMORY = 4000
        sync_reasoner_pellet(self._mapping.ontology, infer_property_values=True, infer_data_property_values=True,
                             debug=0)

        invalid_states = self._extract_error_relations()

        manager_results.append(invalid_states)

    def _load_rules(self, formula_manager: FormulaManagerOWL):
        """
        Loads the SWRL rules from files and stores in ontology.

        :param formula_manager: OWL specifications.
        """
        for rules in formula_manager.rule_sets:
            with self._mapping.ontology:
                for formula_name, rule_strings in rules.items():
                    formula_id = extract_formula_ids_from_strings([formula_name])[0]
                    if formula_id not in self._formula_ids:
                        continue

                    for rule_string in rule_strings:
                        swrl_rule = Imp()
                        swrl_rule.set_as_rule(rule_string)

    def _extract_error_relations(self) -> InvalidStates:
        """
        Extracts the error relations from the ontology.

        :return: Invalid states.
        """
        invalid_states = {}
        for individual in self._mapping.ontology.individuals():
            for invalid_state in individual.has_error:
                tokens = invalid_state.split('$')
                formula_id = extract_formula_ids_from_strings([tokens[0]])[0]
                if len(tokens) > 1:
                    location = tuple(tokens[1:])
                else:
                    location = tuple([int(ele) for ele in individual.has_id])

                if formula_id in list(invalid_states.keys()):
                    invalid_states[formula_id].append(location)
                else:
                    invalid_states[formula_id] = [location]

        return invalid_states
