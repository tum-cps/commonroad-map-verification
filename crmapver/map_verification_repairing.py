import copy
import os.path
import time
from copy import deepcopy
from os import listdir
from os.path import isfile, join, isdir
from typing import List, Tuple, Optional
import logging
from glob import glob

from commonroad.common.file_reader import ProtobufFileReaderMap
from commonroad.common.file_writer import ProtobufFileWriter, OverwriteExistingFile
from commonroad.scenario.lanelet import LaneletNetwork

from crmapver.drawing.invalid_states.invalid_states_drawer import InvalidStatesDrawer
from crmapver.verification.verification_result import VerificationResult, initial_map_verification, \
    update_map_verification
from crmapver.verification.formula_ids import extract_formula_ids
from crmapver.verification.groups_handler import GroupsHandler
from crmapver.config import MapVerParams
from crmapver.repairing.map_repairer import MapRepairer
from crmapver.verification.map_verifier import MapVerifier
from crmapver.verification.sub_map import SubMap


def collect_scenario_paths(scenario_dir: str, map_names_selected: Optional[List[str]],
                           excluded_maps: Optional[List[str]]) -> List[str]:
    """
    Collects scenarios from a directory and subdirectory and returns only paths to selected maps.

    :param scenario_dir: Scenario directory.
    :param map_names_selected: Selected map names.
    :param excluded_maps: Excluded map names.
    :return: List of paths to maps.
    """
    relevant_scenario_paths = []
    map_names = set()
    scenario_names_complete = [y for x in os.walk(scenario_dir) for y in glob(os.path.join(x[0], '*.pb'))]
    for scenario_path in scenario_names_complete:
        name_split = scenario_path.split("/")[-1].split("_", 2)
        map_name = name_split[0] + "_" + name_split[1].split(".")[0]
        if map_name in map_names or map_name[0] == "C" or map_name in excluded_maps or (
                map_names_selected is not None and map_name not in map_names_selected):
            continue
        else:
            map_names.add(map_name)
            relevant_scenario_paths.append(scenario_path)
    return relevant_scenario_paths


def verify_and_repair_map(network: LaneletNetwork, config: MapVerParams = MapVerParams()) \
        -> Tuple[LaneletNetwork, VerificationResult]:
    """
    Verifies and repairs a CommonRoad map. In the verification process the desired formulas are checked to be
    satisfiable. If a formula cannot be satisfied, an invalid state is inferred. These formulas can be executed
    in parallel. After verification the repairing process starts and removes the invalid states. Subsequent
    invalid states can occur and therefore a further iteration is needed. As a measure the maps in the
    repository commonroad-scenarios require maximum 2 to 4 iterations. If the number of iterations is equal to
    zero, then the map is only verified and not repaired.

    Whether a map is valid or was successfully repaired can be summarized in a file. The mentioned iterations and the
    respective occurred invalid states are listed. To make it more apparent, the invalid states can be also
    visualized with symbols for an invalid map.

    If a large map with more than 200 lanelets should be verified and repaired, the supported partitioning
    is strongly recommended. The map is divided into blocks and for each block the verification is applied separately.
    The verification of the blocks can be parallelized and visualized.

    For validating the map the solvers HOL, ASP, Z3, and OWL are offered. The HOL and ASP solver is more preferable
    in terms of performance.

    Statistics are maintained for the verification as well as repairing process and are returned as result.

    :param network: LaneletNetwork.
    :param config: Config parameters.
    :return: Verified as well as repaired scenario and verification result.
    """
    assert network is not None, 'LaneletNetwork is not provided!'

    if not config.evaluation.overwrite_scenario:
        network = deepcopy(network)

    if config.verification.formulas is None or config.verification.formulas == []:
        config.verification.formulas = extract_formula_ids()

    drawer = InvalidStatesDrawer(network) \
        if config.evaluation.invalid_states_draw_dir else None

    verification_result = VerificationResult()
    map_verification = initial_map_verification(verification_result, str(network.meta_information.complete_map_name),
                                                config)

    verification_time, repairing_time = 0.0, 0.0
    logging.info(f'Validating map {network.meta_information.complete_map_name} with {config.verification.solver}')

    groups_handler = GroupsHandler()

    initial_invalid_states = {}
    final_errors = set()

    org_config = copy.deepcopy(config)

    group_i = 0
    while groups_handler.is_next_group():
        group = groups_handler.next_group()

        final_formulas = list(set(org_config.verification.formulas).intersection(set(group)))
        if not final_formulas:
            continue
        else:
            config.verification.formulas = final_formulas

        start = time.time()
        map_verifier = MapVerifier(network, config)
        invalid_states = map_verifier.verify()
        end = time.time()
        verification_time += end - start

        for formula_id, locations in invalid_states.items():
            pre_locations = initial_invalid_states[formula_id] if formula_id in initial_invalid_states.keys() else []
            initial_invalid_states[formula_id] = pre_locations + locations

        if drawer is not None:
            drawer.save_invalid_states_drawing(
                    invalid_states, config.evaluation.invalid_states_draw_dir,
                    file_name=f'group_{group_i}_{network.meta_information.complete_map_name}',
                    file_format=config.evaluation.file_format)

        for formula_id, locations in invalid_states.items():
            for location in locations:
                iter_i = 0
                errors = {(formula_id, location)}
                while errors and iter_i < config.verification.max_iterations:
                    f_id, loc = errors.pop()

                    element_id = loc[0]

                    sub_map = SubMap(network)
                    sub_map.extract_from_element(element_id)
                    sub_network = sub_map.create_subnetwork()

                    start = time.time()
                    map_repairer = MapRepairer(sub_network)
                    map_repairer.repair_map({f_id: [loc]})
                    end = time.time()
                    repairing_time += end - start

                    start = time.time()
                    map_verifier = MapVerifier(sub_network, config)
                    invalid_states = map_verifier.verify()
                    end = time.time()
                    verification_time += end - start

                    if drawer is not None:
                        drawer \
                            .save_invalid_states_drawing(
                                invalid_states, config.evaluation.invalid_states_draw_dir,
                                file_name=f'group_{group_i}_iteration_{iter_i}_'
                                          f'{network.meta_information.complete_map_name}',
                                file_format=config.evaluation.file_format)

                    for f_id, locs in invalid_states.items():
                        for loc in locs:
                            errors.add((f_id, loc))

                    iter_i += 1

                final_errors = final_errors.union(errors)

        group_i += 1

    invalid_states = {}
    for formula_id, location in final_errors:
        if formula_id in invalid_states.keys():
            invalid_states[formula_id].append(location)
        else:
            invalid_states[formula_id] = [location]

    update_map_verification(map_verification, verification_time, repairing_time, initial_invalid_states)

    if drawer is not None:
        drawer.save_invalid_states_drawing(initial_invalid_states, config.evaluation.invalid_states_draw_dir,
                                           file_name=f'initial_result_{network.meta_information.complete_map_name}',
                                           file_format=config.evaluation.file_format)
        drawer.save_invalid_states_drawing(invalid_states, config.evaluation.invalid_states_draw_dir,
                                           file_name=f'final_result_{network.meta_information.complete_map_name}',
                                           file_format=config.evaluation.file_format)

    logging.info(f'Validating map {network.meta_information.complete_map_name} finished with solver '
                 f'{config.verification.solver}')

    return network, verification_result


def verify_and_repair_maps(networks: List[LaneletNetwork], config: MapVerParams) \
        -> Tuple[List[LaneletNetwork], VerificationResult]:
    """
    List of scenarios are verified and repaired successively.

    Statistics are maintained for the verification as well as repairing process and are returned as result.

    :param networks: List of lanelet networks.
    :param config: Configuration parameters.
    :return: List of verified as well as repaired scenarios and verification result.
    """
    assert networks is not None and len(networks) > 0, 'Scenarios are not provided!'

    verification_result = VerificationResult()

    repaired_scenarios = []
    for network in networks:
        repaired_scenario, scenario_verification_result = \
            verify_and_repair_map(network, config)
        repaired_scenarios.append(repaired_scenario)
        verification_result.map_verifications += scenario_verification_result.map_verifications

    return repaired_scenarios, verification_result


def verify_and_repair_dir_maps(scenarios_dir_path: str, config: MapVerParams) \
        -> Tuple[List[LaneletNetwork], VerificationResult]:
    """
    List of scenarios in directory and included subdirectories are verified and repaired successively.
    The loaded scenarios can be overwritten or a new scenario can be stored with file ending '-repaired.xml'.

    Statistics are maintained for the verification as well as repairing process and are returned as result.

    :param scenarios_dir_path: Path to scenarios in directory.
    :param config: Configuration parameters.
    :return: List of verified as well as repaired scenarios and verification result.
    """
    assert os.path.exists(scenarios_dir_path), 'The path to configuration file is not existent!'

    file_names = []
    dir_names = []
    for name in listdir(scenarios_dir_path):
        if isfile(join(scenarios_dir_path, name)) and name.endswith('.xml'):
            file_names.append(name)
        elif isdir(join(scenarios_dir_path, name)):
            dir_names.append(name)

    networks = []
    for name in file_names:
        network = ProtobufFileReaderMap(join(scenarios_dir_path, name)).open()
        networks.append(network)

    repaired_networks, verification_result = \
        verify_and_repair_maps(networks, config)

    for repaired_scenario, name in zip(repaired_networks, file_names):
        writer = ProtobufFileWriter(scenario=repaired_scenario)
        writer.write_map_to_file()
        file_path = join(scenarios_dir_path, name)
        if not config.evaluation.overwrite_scenario:
            file_path = file_path.replace('.xml', '') + '-repaired.xml'
        writer.write_to_file(file_path, OverwriteExistingFile.ALWAYS)

    for name in dir_names:
        sub_dir_path = join(scenarios_dir_path, name)
        verify_and_repair_dir_maps(sub_dir_path, config)

    return repaired_networks, verification_result
