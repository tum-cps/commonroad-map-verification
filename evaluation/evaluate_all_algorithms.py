from crmapver.verification.map_verifier import Solver
from crmapver.verification.formula_ids import FormulaID
from eval_general import (date_strftime_format, message_format, prepare_and_execute_eval, extract_maps_from_tar,
                          num_executions, formulas, selected_map_names, excluded_map_names, selected_sources)
import logging
from typing import List, Dict, Optional
import warnings
import os


warnings.filterwarnings("ignore")
logging.basicConfig(filename='exceptions_alg_eval.log', encoding='utf-8', level=logging.INFO, format=message_format,
                    datefmt=date_strftime_format)


def sub_dir_eval(scenario_base_dir: str, solver: Solver, map_names: Optional[List[str]],
                 excluded_maps: List[str], executions: int, selected_formulas: List[FormulaID]) \
        -> Dict[str, Dict[str, str]]:
    results = prepare_and_execute_eval(excluded_maps, map_names, scenario_base_dir, solver, selected_formulas)

    total_com_time = 0
    for eval_res in results:
        total_com_time += eval_res.comp_time

    name = scenario_base_dir.split('/')[-1] if map_names is None else map_names[0]
    return {name: {solver.value: format(total_com_time / executions, '.3f')}}


if __name__ == '__main__':
    extract_maps_from_tar()
    eval_results = {}
    scenario_dir = os.path.dirname(os.path.realpath(__file__)) + "/../eval_maps"
    for map_name in selected_map_names:
        for solv_alg in Solver:
            res = sub_dir_eval(scenario_dir, solv_alg, [map_name], excluded_map_names, num_executions,
                               formulas)
            if eval_results.get(list(res.keys())[0]) is None:
                eval_results.update(res)
            else:
                eval_results[list(res.keys())[0]].update(list(res.values())[0])

    selected_map_names = None
    scenario_dir = os.path.dirname(os.path.realpath(__file__)) + "/../eval_maps"
    for source in next(os.walk(scenario_dir))[1]:
        if not any([source_name.lower() in source.lower() for source_name in selected_sources]):
            continue
        for solv_alg in Solver:
            res = sub_dir_eval(os.path.join(scenario_dir, source), solv_alg, selected_map_names, excluded_map_names,
                               num_executions, formulas)
            if eval_results.get(list(res.keys())[0]) is None:
                eval_results.update(res)
            else:
                eval_results[list(res.keys())[0]].update(list(res.values())[0])

    accumulated = {'asp': 0.0, 'z3': 0.0, 'owl': 0.0, 'hol': 0.0}
    for map_source in selected_sources:
        for key, value in eval_results.items():
            if key.lower() == map_source.lower():
                for alg in accumulated.keys():
                    accumulated[alg] += float(value[alg])
    accumulated = {key: format(value, '.3f') for key, value in accumulated.items()}
    eval_results["All maps accumulated"] = accumulated

    output_str = "Map | ASP [s] | DL [s] | HOL [s] | SMT [s]"
    output_str += "\n"
    for key, value in eval_results.items():
        output_str += f"{key if key not in selected_sources else key + ' maps'} | {value['asp']} | {value['owl']} | " \
                      f"{value['hol']} | {value['z3']}"
        output_str += "\n"
    output_str += "\n"
    output_str += "\\textbf{Map} & \\textbf{ASP [s]} & \\textbf{DL [s]} & \\textbf{HOL [s]} & " \
                  "\\textbf{SMT [s]} \\\\ \midrule"
    output_str += "\n"
    for key, value in eval_results.items():
        src = key
        for el in selected_sources:
            if el.lower() == src.lower():
                src = el
        output_str += f"{src if src not in selected_sources else src + ' maps'} & {value['asp']} & " \
                      f"{value['owl']} & {value['hol']} & {value['z3']} \\\\"
        output_str += "\n"
    output_str += f"Number evaluated specifications: {len(formulas)}.\n"

    print(output_str)
    with open("algorithm_evaluation.txt", "a") as file:
        file.write(output_str)
