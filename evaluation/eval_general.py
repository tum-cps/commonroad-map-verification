from crmapver.map_verification_repairing import verify_and_repair_map, collect_scenario_paths
from crmapver.verification.verification_result import MapVerificationComparison
from crmapver.verification.formula_ids import LaneletFormulaID
from crmapver.verification.formula_ids import FormulaID
from crmapver.config import MapVerParams
from crmapver.verification.map_verifier import Solver
from commonroad.common.file_reader import ProtobufFileReaderMap
from typing import List
from multiprocessing import Pool
from functools import partial
import logging
import traceback
import os
import tarfile


date_strftime_format = "%d-%b-%y %H:%M:%S"
message_format = "%(asctime)s - %(levelname)s - %(message)s"

excluded_map_names = ["DEU_Frankfurt-4", "DEU_Frankfurt-258", "DEU_Frankfurt-174", "DEU_Frankfurt-221",
                      "DEU_Frankfurt-31", "DEU_Frankfurt-47", "DEU_Frankfurt-94", "DEU_Frankfurt-46",
                      "DEU_Frankfurt-115", "DEU_Frankfurt-264", "DEU_Frankfurt-182", "DEU_Frankfurt-20",
                      "DEU_Frankfurt-72", "DEU_Frankfurt-14", "DEU_Frankfurt-15", "DEU_Frankfurt-58",
                      "DEU_Frankfurt-41", "DEU_Frankfurt-68", "DEU_Frankfurt-163", "DEU_Frankfurt-22",
                      "DEU_Frankfurt-278", "DEU_Frankfurt-186", "DEU_Frankfurt-119", "DEU_Frankfurt-283",
                      "DEU_Frankfurt-245", "DEU_Frankfurt-274", "DEU_Frankfurt-235", "DEU_Frankfurt-113",
                      "DEU_Frankfurt-147", "DEU_Frankfurt-5"]
selected_map_names = ["DEU_Guetersloh-20", "DEU_BadEssen-3", "DEU_Lohmar-70"]
selected_sources = ["Hand-Crafted", "NGSIM", "Scenario-Factory", "SUMO", "Interactive"]
num_cores = 40
num_executions = 1
formulas = [LaneletFormulaID.UNIQUE_ID, LaneletFormulaID.SAME_VERTICES_SIZE, LaneletFormulaID.VERTICES_MORE_THAN_ONE,
            LaneletFormulaID.EXISTENCE_RIGHT_ADJ, LaneletFormulaID.EXISTENCE_LEFT_ADJ,
            LaneletFormulaID.EXISTENCE_PREDECESSOR, LaneletFormulaID.EXISTENCE_SUCCESSOR,
            LaneletFormulaID.EXISTENCE_TRAFFIC_SIGNS, LaneletFormulaID.EXISTENCE_TRAFFIC_LIGHTS,
            LaneletFormulaID.POLYLINES_INTERSECTION, LaneletFormulaID.LEFT_SELF_INTERSECTION,
            LaneletFormulaID.RIGHT_SELF_INTERSECTION]


def eval_maps(config: MapVerParams, path: str) -> MapVerificationComparison:
    val_res = MapVerificationComparison()
    try:
        network = ProtobufFileReaderMap(path).open()
        val_res.scenario_meta(network)
    except Exception:
        logging.error("{} eval map {}, error during initialization: {}".format(config.verification.solver.value,
                                                                               path,
                                                                               traceback.format_exc()))
        return val_res
    for idx in range(num_executions):
        try:
            _, val = verify_and_repair_map(network, config)
            val_res.update(val.map_verifications[0])
        except Exception:
            logging.error("{} eval map {}, error: {}".format(config.verification.solver.value,
                                                             network.meta_information.complete_map_name,
                                                             traceback.format_exc()))
    return val_res


def prepare_and_execute_eval(excluded_maps: List[str], map_names: List[str], scenario_base_dir: str, solver: Solver,
                             selected_formulas: List[FormulaID]):
    relevant_scenario_paths = collect_scenario_paths(scenario_base_dir, map_names, excluded_maps)
    config = MapVerParams()
    config.verification.formulas = selected_formulas
    config.verification.solver = solver
    func = partial(eval_maps, config)
    num_cores_exec = num_cores if solver is not Solver.Z3 else 1
    with Pool(processes=num_cores_exec) as pool:
        results: List[MapVerificationComparison] = pool.map(func, relevant_scenario_paths)
    return results


def extract_maps_from_tar():
    if not os.path.exists(os.path.dirname(os.path.realpath(__file__)) + "/../eval_maps/"):
        os.makedirs(os.path.dirname(os.path.realpath(__file__)) + "/../eval_maps/")
        with tarfile.open(os.path.dirname(os.path.realpath(__file__)) + "/../eval_maps.tar.xz") as tar_file:
            tar_file.extractall(os.path.dirname(os.path.realpath(__file__)) + "/../eval_maps/")
