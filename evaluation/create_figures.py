import os
from crmapver.map_verification_repairing import verify_and_repair_map, collect_scenario_paths
from crmapver.config import MapVerParams
from commonroad.common.file_reader import ProtobufFileReaderMap
from eval_general import selected_map_names, excluded_map_names

scenario_path = collect_scenario_paths(os.path.dirname(os.path.realpath(__file__)) + "/../eval_maps/",
                                       selected_map_names, excluded_map_names)
config = MapVerParams()

fig_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "figures")
if not os.path.exists(fig_path):
    os.makedirs(fig_path)

config.evaluation.invalid_states_draw_dir = fig_path

for path in scenario_path:
    lanelet_network = ProtobufFileReaderMap(path).open()
    verify_and_repair_map(lanelet_network, config=config)

# Note: The figures in our paper are modified with Inkscape.

