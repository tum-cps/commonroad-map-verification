from crmapver.verification.formula_ids import LaneletFormulaID, TrafficSignFormulaID, TrafficLightFormulaID, \
    IntersectionFormulaID, GeneralFormulaID, FormulaTypes
from eval_general import num_executions, selected_sources, selected_map_names, \
    excluded_map_names, prepare_and_execute_eval, date_strftime_format, message_format, extract_maps_from_tar
from crmapver.verification.map_verifier import Solver
import logging
from typing import List, Dict, Optional, Tuple
import warnings
import os
from collections import Counter

warnings.filterwarnings("ignore")
logging.basicConfig(filename='exceptions_formula_eval.log', encoding='utf-8', level=logging.INFO, format=message_format,
                    datefmt=date_strftime_format)

excluded_formulas = [LaneletFormulaID.INCLUDED_STOP_LINE_TRAFFIC_LIGHTS,
                     LaneletFormulaID.INCLUDED_STOP_LINE_TRAFFIC_SIGNS,
                     TrafficLightFormulaID.AT_LEAST_ONE_CYCLE_ELEMENT]
formulas = [value for value in GeneralFormulaID] + \
           [value for value in LaneletFormulaID] + \
           [value for value in TrafficSignFormulaID] + \
           [value for value in TrafficLightFormulaID] + \
           [value for value in IntersectionFormulaID]
for value in excluded_formulas:
    formulas.remove(value)


def sub_dir_eval(scenario_base_dir: str, solver: Solver, map_names: Optional[List[str]], excluded_maps: List[str]) -> \
        Tuple[Dict[str, Dict[str, str]], Tuple[str, float], Dict[str, int]]:
    results = prepare_and_execute_eval(excluded_maps, map_names, scenario_base_dir, solver, formulas)

    total_com_time = 0.0
    invalid_maps = 0
    lanelet_length = 0.0
    num_lanelets = 0
    num_ts_tl = 0
    num_intersections = 0
    num_errors = 0
    total_repairing_time = 0.0
    max_violations = ("", 0.0)
    most_violated_spec_source = {formula.name: 0 for formula_type in FormulaTypes for formula in formula_type}
    for eval_res in results:
        total_com_time += eval_res.comp_time

        if eval_res.num_invalid_states > 0:
            invalid_maps += 1
        lanelet_length += eval_res.distance / 1000
        num_lanelets += eval_res.lanelets
        num_ts_tl += eval_res.traffic_signs + eval_res.traffic_lights
        num_intersections += eval_res.intersections
        num_errors += eval_res.num_invalid_states
        total_repairing_time += eval_res.repairing_time
        avg_violations = eval_res.num_invalid_states / (num_ts_tl + num_intersections + num_lanelets)
        if avg_violations > max_violations[1]:
            max_violations = (eval_res.map_id, avg_violations)
        most_violated_spec_source = dict(Counter(most_violated_spec_source)+Counter(eval_res.most_violated_spec))

    name = scenario_base_dir.split('/')[-1] if map_names is None else map_names[0]
    return {name: {"Maps": str(len(results)), "Invalid Maps": str(invalid_maps),
                   "Lanelets [km]": format(lanelet_length, '.3f'), "|L|": str(num_lanelets),
                   "|TS| u |TL|": str(num_ts_tl), "|I|": str(num_intersections), "|E|": str(num_errors),
                   "HOL [s]": format(total_com_time / num_executions, '.3f'),
                   "Repairing [s]": format(total_repairing_time / num_executions, '.3f')}}, max_violations, \
        most_violated_spec_source


if __name__ == '__main__':
    logging.info("Evaluating {} formulas.".format(len(formulas)))
    extract_maps_from_tar()
    eval_results = {}
    scenario_dir = os.path.dirname(os.path.realpath(__file__)) + "/../eval_maps/"
    most_viol_total = ("", 0.0)
    most_violated_spec_total = {formula.name: 0 for formula_type in FormulaTypes for formula in formula_type}

    for map_name in selected_map_names:
        res, _, _ = sub_dir_eval(scenario_dir, Solver.HOL, [map_name], excluded_map_names)
        if eval_results.get(list(res.keys())[0]) is None:
            eval_results.update(res)
        else:
            eval_results[list(res.keys())[0]].update(list(res.values())[0])

    selected_map_names = None
    for source in next(os.walk(scenario_dir))[1]:
        if not any([source_name.lower() in source.lower() for source_name in selected_sources]):
            continue
        res, most_viol, most_violated_spec = \
            sub_dir_eval(os.path.join(scenario_dir, source), Solver.HOL, selected_map_names, excluded_map_names)
        most_violated_spec_total = dict(Counter(most_violated_spec_total)+Counter(most_violated_spec))
        if most_viol[1] > most_viol_total[1]:
            most_viol_total = most_viol
        if eval_results.get(list(res.keys())[0]) is None:
            eval_results.update(res)
        else:
            eval_results[list(res.keys())[0]].update(list(res.values())[0])
    # sort dict
    most_violated_spec_total = {k: v for k, v in sorted(most_violated_spec_total.items(), key=lambda item: item[1],
                                                        reverse=True)}

    accumulated = {"Maps": 0, "Invalid Maps": 0, "Lanelets [km]": 0.0, "|L|": 0, "|TS| u |TL|": 0, "|I|": 0, "|E|": 0,
                   "HOL [s]": 0.0, "Repairing [s]": 0.0}
    for map_source in selected_sources:
        for key, value in eval_results.items():
            if key.lower() == map_source.lower():
                for param in accumulated.keys():
                    accumulated[param] += float(value[param]) if type(accumulated[param]) is float else int(
                            value[param])
    accumulated = {key: format(value, '.3f') if type(value) is float else value for key, value in accumulated.items()}
    eval_results["All maps accumulated"] = accumulated

    output_str = "Map | Maps | Invalid Maps | Lanelets [km] | |L| | |TS| u |TL| | |I| | |E| | HOL [s] | Repairing [s] |"
    output_str += "\n"
    for key, value in eval_results.items():
        output_str += f"{key if key not in selected_sources else key + ' maps'} | " \
                      f"{value['Maps']} | {value['Invalid Maps']} | {value['Lanelets [km]']} | " \
                      f"{value['|L|']} | {value['|TS| u |TL|']} | {value['|I|']} | {value['|E|']} | " \
                      f"{value['HOL [s]']} | {value['Repairing [s]']}"
        output_str += "\n"
    output_str += "\n"
    output_str += "\\textbf{Map} & \\textbf{Maps} & \\textbf{Invalid Maps} & \\textbf{Lanelets [km]} & " \
                  "\\boldmath{$|\mathbf{\\laneletSet}|$} & \\boldmath{$|\\trafficSignSet \\cup " \
                  "\\trafficLightSet|$} & \\boldmath{$|\\intersectionSet|$} & \\boldmath{$|\mapValErrors|$} & " \
                  "\\textbf{HOL [s]} & \\textbf{Repairing [s]} \\\\ \midrule"
    output_str += "\n"
    for key, value in eval_results.items():
        src = key
        for el in selected_sources:
            if el.lower() == src.lower():
                src = el
        output_str += f"{src if src not in selected_sources else src + ' maps'} & {value['Maps']} & " \
                      f"{value['Invalid Maps']} & {value['Lanelets [km]']} & " \
                      f"{value['|L|']} & {value['|TS| u |TL|']} & {value['|I|']} & {value['|E|']} & " \
                      f"{value['HOL [s]']} & {value['Repairing [s]']} \\\\"
        output_str += "\n"
    output_str += "\n"
    output_str += f"Map with most violations: {most_viol_total[0]} with {most_viol_total[1]} " \
                  f"violations per road element.\n"

    output_str += f"Number evaluated specifications: {len(formulas)}.\n"
    output_str += f"Specifications with most violations:\n"
    idx = 0
    for key, value in most_violated_spec_total.items():
        output_str += f"    {key}: {value}\n"
        idx += 1
        if idx > 10:
            break

    print(output_str)
    with open("formula_evaluation.txt", "a") as file:
        file.write(output_str)
