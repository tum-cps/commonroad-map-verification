import os
from crmapver.map_verification_repairing import verify_and_repair_map, collect_scenario_paths
from crmapver.config import MapVerParams
from commonroad.common.file_reader import ProtobufFileReaderMap

file_dir = os.path.join(os.getcwd(), 'files')
scenario_path = collect_scenario_paths(os.path.dirname(os.path.realpath(__file__)) + "/../eval_maps/",
                                       ["USA_US101-9"], [])

lanelet_network = ProtobufFileReaderMap(scenario_path[0]).open()
config = MapVerParams()
config.evaluation.file_format = "svg"
config.evaluation.invalid_states_draw_dir = "./"

verify_and_repair_map(lanelet_network, config=config)
