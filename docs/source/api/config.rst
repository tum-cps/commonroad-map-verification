:orphan:
Module Config
=============

Config
-----------

.. automodule:: crmapver.config

``Solver`` class
^^^^^^^^^^^^^^^^
.. autoclass:: Solver
   :members:
   :undoc-members:
   :member-order: bysource

.. autofunction:: _dict_to_params

``BaseParam`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: BaseParam
   :members:
   :undoc-members:
   :member-order: bysource

``VerificationParams`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: VerificationParams
   :members:
   :undoc-members:
   :member-order: bysource

``PartitioningParams`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: PartitioningParams
   :members:
   :undoc-members:
   :member-order: bysource

``EvaluationParams`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: EvaluationParams
   :members:
   :undoc-members:
   :member-order: bysource

``MapVerParams`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: MapVerParams
   :members:
   :undoc-members:
   :member-order: bysource
