:orphan:
Module Verification
===================

Map Verification
----------------

.. automodule:: crmapver.verification.map_verifier

``MapVerifier`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: MapVerifier
   :members:
   :undoc-members:
   :member-order: bysource


.. automodule:: crmapver.verification.satisfaction

``VerificationChecker`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: VerificationChecker
   :members:
   :undoc-members:
   :member-order: bysource


.. automodule:: crmapver.verification.sub_map

``SubMap`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: SubMap
   :members:
   :undoc-members:
   :member-order: bysource


.. automodule:: crmapver.verification.verification_result

``InvalidState`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: InvalidState
   :members:
   :undoc-members:
   :member-order: bysource

``MapVerificationResult`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: MapVerificationResult
   :members:
   :undoc-members:
   :member-order: bysource

``MapVerification`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: MapVerification
   :members:
   :undoc-members:
   :member-order: bysource

``VerificationResult`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: VerificationResult
   :members:
   :undoc-members:
   :member-order: bysource

``MapVerificationComparison`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: MapVerificationComparison
   :members:
   :undoc-members:
   :member-order: bysource

.. autofunction:: extract_verification_computation_times

.. autofunction:: extract_repairing_computation_times

.. autofunction:: extract_benchmark_ids

.. autofunction:: extract_invalid_states

.. autofunction:: extract_verification_parameters

.. autofunction:: initial_map_verification

.. autofunction:: update_map_verification


.. automodule:: crmapver.verification.mapping

``Mapping`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Mapping
   :members:
   :undoc-members:
   :member-order: bysource

``Preprocessing`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Preprocessing
   :members:
   :undoc-members:
   :member-order: bysource


.. automodule:: crmapver.verification.formula_ids

``GeneralFormulaID`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: GeneralFormulaID
   :members:
   :undoc-members:
   :member-order: bysource

``LaneletFormulaID`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: LaneletFormulaID
   :members:
   :undoc-members:
   :member-order: bysource

``TrafficSignFormulaID`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: TrafficSignFormulaID
   :members:
   :undoc-members:
   :member-order: bysource


``TrafficLightFormulaID`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: TrafficLightFormulaID
   :members:
   :undoc-members:
   :member-order: bysource


``IntersectionFormulaID`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: IntersectionFormulaID
   :members:
   :undoc-members:
   :member-order: bysource

.. autofunction:: extract_formula_id

.. autofunction:: extract_formula_ids

.. autofunction:: extract_formula_ids_from_strings

.. autofunction:: extract_formula_ids_by_type

.. autofunction:: filter_formula_ids_by_type



ASP
----------------

.. automodule:: crmapver.verification.asp.satisfaction

``ASPVerificationChecker`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: ASPVerificationChecker
   :members:
   :undoc-members:
   :member-order: bysource

``Context`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Context
   :members:
   :undoc-members:
   :member-order: bysource


.. automodule:: crmapver.verification.asp.mapping

``ASPMapping`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: ASPMapping
   :members:
   :undoc-members:
   :member-order: bysource

``ASPPreprocessing`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: ASPPreprocessing
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.asp.formulas.formula_manager

``FormulaManagerASP`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: FormulaManagerASP
   :members:
   :undoc-members:
   :member-order: bysource



HOL
----------------

.. automodule:: crmapver.verification.hol.satisfaction

``HOLVerificationChecker`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: HOLVerificationChecker
   :members:
   :undoc-members:
   :member-order: bysource


.. automodule:: crmapver.verification.hol.mapping

``HOLMapping`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: HOLMapping
   :members:
   :undoc-members:
   :member-order: bysource

.. autofunction:: _prepare_predicate_funcs

.. autofunction:: _prepare_function_funcs


.. automodule:: crmapver.verification.hol.formula_manager

``FormulaManager`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: FormulaManager
   :members:
   :undoc-members:
   :member-order: bysource


.. automodule:: crmapver.verification.hol.formula_collection

``GeneralFormulas`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: GeneralFormulas
   :members:
   :undoc-members:
   :member-order: bysource

``LaneletFormulas`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: LaneletFormulas
   :members:
   :undoc-members:
   :member-order: bysource

``TrafficSignFormulas`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: TrafficSignFormulas
   :members:
   :undoc-members:
   :member-order: bysource

``TrafficLightFormulas`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: TrafficLightFormulas
   :members:
   :undoc-members:
   :member-order: bysource

``IntersectionFormulas`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: IntersectionFormulas
   :members:
   :undoc-members:
   :member-order: bysource


.. automodule:: crmapver.verification.hol.formula

``DomainName`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: DomainName
   :members:
   :undoc-members:
   :member-order: bysource

``Formula`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Formula
   :members:
   :undoc-members:
   :member-order: bysource


.. automodule:: crmapver.verification.hol.var_domain_iterator

``VarDomainIterator`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: VarDomainIterator
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.hol.var_domain_iterator

``VarDomainIterator`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: VarDomainIterator
   :members:
   :undoc-members:
   :member-order: bysource


.. automodule:: crmapver.verification.hol.context

``Context`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Context
   :members:
   :undoc-members:
   :member-order: bysource


.. automodule:: crmapver.verification.hol.functions.predicates.builtin_predicates

.. autofunction:: equal

.. autofunction:: unequal

.. autofunction:: less

.. autofunction:: greater

.. autofunction:: less_equal

.. autofunction:: greater_equal

.. autofunction:: contains


.. automodule:: crmapver.verification.hol.functions.predicates.intersection_predicates

.. autofunction:: has_incoming_element

.. autofunction:: has_incoming_lanelet


.. automodule:: crmapver.verification.hol.functions.predicates.lanelet_predicates

.. autofunction:: has_left_adj_ref

.. autofunction:: has_right_adj_ref

.. autofunction:: has_left_adj

.. autofunction:: has_right_adj

.. autofunction:: is_left_adj_same_direction

.. autofunction:: is_right_adj_same_direction

.. autofunction:: has_predecessor

.. autofunction:: has_successor

.. autofunction:: has_traffic_sign

.. autofunction:: has_traffic_light

.. autofunction:: is_polylines_intersection

.. autofunction:: is_polyline_self_intersection

.. autofunction:: are_equal_vertices

.. autofunction:: are_intersected_lanelets

.. autofunction:: has_stop_line

.. autofunction:: has_start_point

.. autofunction:: has_end_point

.. autofunction:: are_similar_polylines

.. autofunction:: is_adj_type


.. automodule:: crmapver.verification.hol.functions.predicates.traffic_light_predicates

.. autofunction:: has_cycle_element


.. automodule:: crmapver.verification.hol.functions.term_functions.builtin_functions

.. autofunction:: size

.. autofunction:: index

.. autofunction:: reverse


.. automodule:: crmapver.verification.hol.functions.term_functions.general_functions

.. autofunction:: el_id


.. automodule:: crmapver.verification.hol.functions.term_functions.intersection_functions

.. autofunction:: intersection_id

.. autofunction:: incoming_elements

.. autofunction:: incoming_lanelets


.. automodule:: crmapver.verification.hol.functions.term_functions.lanelet_functions

.. autofunction:: lanelet_id

.. autofunction:: left_adj

.. autofunction:: right_adj

.. autofunction:: left_polyline

.. autofunction:: right_polyline

.. autofunction:: start_vertex

.. autofunction:: end_vertex

.. autofunction:: traffic_signs

.. autofunction:: traffic_lights

.. autofunction:: stop_line

.. autofunction:: predecessors

.. autofunction:: successors

.. autofunction:: stop_line_traffic_signs

.. autofunction:: stop_line_traffic_lights


.. automodule:: crmapver.verification.hol.functions.term_functions.traffic_light_functions

.. autofunction:: traffic_light_id

.. autofunction:: cycle_elements

.. autofunction:: duration


.. automodule:: crmapver.verification.hol.functions.term_functions.traffic_sign_functions

.. autofunction:: traffic_sign_id

.. autofunction:: traffic_sign_elements

.. autofunction:: distance_to_lanelet



.. automodule:: crmapver.verification.hol.parser.parser

``Parser`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Parser
   :members:
   :undoc-members:
   :member-order: bysource


.. automodule:: crmapver.verification.hol.expression_tree.symbols

``Symbol`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Symbol
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.hol.expression_tree.expression

``Expression`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Expression
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.hol.expression_tree.atomic.bool

``Bool`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Bool
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.hol.expression_tree.atomic.predicate

``Predicate`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Predicate
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.hol.expression_tree.binary.binary

``Binary`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Binary
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.hol.expression_tree.binary.bool.equivalence

``Equivalence`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Equivalence
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.hol.expression_tree.binary.bool.implication

``Implication`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Implication
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.hol.expression_tree.domain.domain

``Domain`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Domain
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.hol.expression_tree.domain.dynamic

``DynamicDomain`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: DynamicDomain
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.hol.expression_tree.domain.fixed

``FixedDomain`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: FixedDomain
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.hol.expression_tree.nary.nary

``Nary`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Nary
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.hol.expression_tree.nary.bool.xor

``Xor`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Xor
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.hol.expression_tree.nary.bool.and

``And`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: And
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.hol.expression_tree.term.constant

``Constant`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Constant
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.hol.expression_tree.term.function

``Function`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Function
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.hol.expression_tree.term.term

``Term`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Term
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.hol.expression_tree.term.variable

``Variable`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Variable
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.hol.expression_tree.unary.unary

``Unary`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Unary
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.hol.expression_tree.unary.bool.not

``Not`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Not
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.hol.expression_tree.unary.first_order.counting

``Counting`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Counting
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.hol.expression_tree.unary.first_order.existential

``Existential`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Existential
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.hol.expression_tree.unary.first_order.first_order

``FirstOrder`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: FirstOrder
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.hol.expression_tree.unary.first_order.universal

``Universal`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Universal
   :members:
   :undoc-members:
   :member-order: bysource






OWL
----------------

.. automodule:: crmapver.verification.owl.satisfaction

``OWLVerificationChecker`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: OWLVerificationChecker
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.owl.mapping

``OWLMapping`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: OWLMapping
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.owl.formulas.formula_manager

``FormulaManagerOWL`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: FormulaManagerOWL
   :members:
   :undoc-members:
   :member-order: bysource





Z3
----------------

.. automodule:: crmapver.verification.z3.satisfaction

``Z3VerificationChecker`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Z3VerificationChecker
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.z3.mapping

``Z3Mapping`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Z3Mapping
   :members:
   :undoc-members:
   :member-order: bysource

``Z3Preprocessing`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Z3Preprocessing
   :members:
   :undoc-members:
   :member-order: bysource

``VerificationSort`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: VerificationSort
   :members:
   :undoc-members:
   :member-order: bysource

``MapSort`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: MapSort
   :members:
   :undoc-members:
   :member-order: bysource

``LaneletSort`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: LaneletSort
   :members:
   :undoc-members:
   :member-order: bysource

``VertexSort`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: VertexSort
   :members:
   :undoc-members:
   :member-order: bysource

``PolylineSort`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: PolylineSort
   :members:
   :undoc-members:
   :member-order: bysource

``StopLineSort`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: StopLineSort
   :members:
   :undoc-members:
   :member-order: bysource

``TrafficSignSort`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: TrafficSignSort
   :members:
   :undoc-members:
   :member-order: bysource

``TrafficSignElementSort`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: TrafficSignElementSort
   :members:
   :undoc-members:
   :member-order: bysource

``TrafficLightSort`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: TrafficLightSort
   :members:
   :undoc-members:
   :member-order: bysource

``TrafficLightSort`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: TrafficLightSort
   :members:
   :undoc-members:
   :member-order: bysource

``CycleElementSort`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: TrafficLightSort
   :members:
   :undoc-members:
   :member-order: bysource

``IntersectionSort`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: IntersectionSort
   :members:
   :undoc-members:
   :member-order: bysource

``IncomingElementSort`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: IncomingElementSort
   :members:
   :undoc-members:
   :member-order: bysource

``TupleSort`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: TupleSort
   :members:
   :undoc-members:
   :member-order: bysource


.. automodule:: crmapver.verification.z3.formulas.formula_pool

``FormulaPool`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: FormulaPool
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.z3.formulas.intersection_formulas

``IntersectionFormulaPool`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: IntersectionFormulaPool
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.z3.formulas.lanelet_formulas

``LaneletFormulaPool`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: LaneletFormulaPool
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.z3.formulas.subformulas

``SubformulaPool`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: SubformulaPool
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.z3.formulas.traffic_light_formulas

``TrafficLightFormulaPool`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: TrafficLightFormulaPool
   :members:
   :undoc-members:
   :member-order: bysource

.. automodule:: crmapver.verification.z3.formulas.traffic_sign_formulas

``TrafficSignFormulaPool`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: TrafficSignFormulaPool
   :members:
   :undoc-members:
   :member-order: bysource