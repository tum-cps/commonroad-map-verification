:orphan:
Module Partitioning
=============

.. automodule:: crmapver.partitioning.map_partition

MapPartitioning
-----------------

``MapPartitioning`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: MapPartitioning
   :members:
   :undoc-members:
   :member-order: bysource

LaneletPartitioning
--------------------

``LaneletPartitioning`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: LaneletPartitioning
   :members:
   :undoc-members:
   :member-order: bysource

``EdgeType`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: EdgeType
   :members:
   :undoc-members:
   :member-order: bysource
TrafficSignPartitioning
------------------------

``TrafficSignPartitioning`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: TrafficSignPartitioning
   :members:
   :undoc-members:
   :member-order: bysource

TrafficLightPartitioning
------------------------

``TrafficLightPartitioning`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: TrafficLightPartitioning
   :members:
   :undoc-members:
   :member-order: bysource

``IntersectionPartitioning`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``IntersectionPartitioning`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: IntersectionPartitioning
   :members:
   :undoc-members:
   :member-order: bysource

