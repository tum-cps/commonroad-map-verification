:orphan:
Module Common
=============

Common
-----------

.. automodule:: crmapver.common

``Mappings`` class
^^^^^^^^^^^^^^^^
.. autoclass:: Mappings
   :members:
   :undoc-members:
   :member-order: bysource

``VerificationCheckers`` class
^^^^^^^^^^^^^^^^
.. autoclass:: VerificationCheckers
   :members:
   :undoc-members:
   :member-order: bysource
