:orphan:
Module Repairing
================

Map Repairing
-------------

.. automodule:: crmapver.repairing.map_repairer

``MapRepairer`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: MapRepairer
   :members:
   :undoc-members:
   :member-order: bysource


Repairing
-------------

.. automodule:: crmapver.repairing.repairing

``ElementRepairing`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: ElementRepairing
   :members:
   :undoc-members:
   :member-order: bysource


Lanelet Repairing
-----------------

.. automodule:: crmapver.repairing.lanelet_repairing

``LaneletRepairing`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: LaneletRepairing
   :members:
   :undoc-members:
   :member-order: bysource


Traffic Sign Repairing
----------------------

.. automodule:: crmapver.repairing.traffic_sign_repairing

``TrafficSignRepairing`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: TrafficSignRepairing
   :members:
   :undoc-members:
   :member-order: bysource


Traffic Light Repairing
-----------------------

.. automodule:: crmapver.repairing.traffic_light_repairing

``TrafficSignRepairing`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: TrafficLightRepairing
   :members:
   :undoc-members:
   :member-order: bysource


Intersection Repairing
----------------------

.. automodule:: crmapver.repairing.intersection_repairing

``IntersectionRepairing`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: IntersectionRepairing
   :members:
   :undoc-members:
   :member-order: bysource


General Repairing
----------------------

.. automodule:: crmapver.repairing.general_repairing

``GeneralRepairing`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: GeneralRepairing
   :members:
   :undoc-members:
   :member-order: bysource


Geometry Tools
--------------

.. automodule:: repairing.tools.geometry_tools

.. autofunction:: check_line_intersection_efficient

.. autofunction:: check_intersected_lines

.. autofunction:: contains_points

.. autofunction:: fill_number_of_vertices

.. autofunction:: average_vertices

.. autofunction:: insert_vertices

.. autofunction:: create_mapping

.. autofunction:: compute_path_length_from_polyline

.. autofunction:: line_length
