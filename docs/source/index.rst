****************************
CommonRoad Map Verification and Repairing
****************************

This repository is intended for reproducing the results from our paper
*Map Verification and Repairing Using Formalized Map Specifications* presented at ITSC 2023.
This repository will not be maintained.
The relevant code will be integrated in the CommonRoad Scenario Designer (version 1.0.0 and later).

Using CommonRoad Map Verification and Repairing
===============================================
The recommended way of installation if you only want to use the map verification and repairing
(i.e., you do not want to work with the code directly) is to activate a Python environment
(>= Python 3.8) and install the tool with the following command:
```bash
pip install .
```

Development
-----------
First, clone the repository.
The usage of [Poetry](https://python-poetry.org/) is recommended.
Poetry can be installed using:

.. code-block:: bash

  curl -sSL https://install.python-poetry.org | python3 -

Further installation instructions can be found under the linked website.

Create a new Python environment:

.. code-block:: bash

    poetry shell
    poetry install --with tests,docs,tutorials

We recommend to use PyCharm (Professional) as IDE.

Usage
----
You can find a tutorial and example script in the `tutorials` folder.
The scripts used for the evaluation in our paper can be found in the `evaluation` folder.

The code works with prototype scenarios of the 2024a scenarios which are located
in this repository (`eval_maps.tar.xz`).
Officially released scenarios cannot be used with this tool (use the CommonRoad Scenario Designer).

.. toctree::
   :maxdepth: 2
   :caption: CommonRoad Map Verification and Repairing API

   api/index.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Contact information
===================

:Website: `http://commonroad.in.tum.de <https://commonroad.in.tum.de/>`_
:Email: `commonroad@lists.lrz.de <commonroad@lists.lrz.de>`_
