# CommonRoad Map Verification and Repairing

This repository is intended for reproducing the results from our paper 
_Map Verification and Repairing Using Formalized Map Specifications_ presented at ITSC 2023.  
This repository will not be maintained.  
The code will be integrated in the CommonRoad Scenario Designer (version 1.0.0 and later).  

## Using CommonRoad Map Verification and Repairing
The recommended way of installation if you only want to use the map verification and repairing 
(i.e., you do not want to work with the code directly) is to activate a Python environment 
(>= Python 3.8) and install the tool with the following command:
```bash
pip install .
```

### Development
First, clone the repository.  
The usage of [Poetry](https://python-poetry.org/) is recommended. 
Poetry can be installed using:
```bash
curl -sSL https://install.python-poetry.org | python3 -
```
Further installation instructions can be found under the linked website.

Create a new Python environment:
```bash
poetry shell
poetry install --with tests,docs,tutorials
```
We recommend to use PyCharm (Professional) as IDE.

### Usage
You can find a tutorial and example script in the `tutorials` folder.
The scripts used for the evaluation in our paper can be found in the `evaluation` folder.  

The code works with prototype scenarios of the 2024a scenarios which are located 
in this repository (`eval_maps.tar.xz`).
Officially released scenarios cannot be used with this tool (use the CommonRoad Scenario Designer).

### Documentation
You can generate the documentation within your activated Python environment using
```bash
cd docs/source && sphinx-build -b html . ../public
```
The documentation will be located under docs/public. 

## Citation
**If you use our code for research, please consider to cite our paper:**
```
@inproceedings{Maierhofer2023,
	author = {Sebastian Maierhofer, Yannick Ballnath, and Matthias Althoff},
	title = {Map Verification and Repairing Using Formalized Map Specifications},
	booktitle = {Proc. of the IEEE Int. Conf. on Intelligent Transportation Systems },
	year = {2023},
	pages = {tbd}
	abstract = {Autonomous vehicles benefit from correct maps to participate in traffic safely, 
	            but often maps are not verified before their usage. We address this problem 
	            by providing an approach to verify and repair maps automatically based on a 
	            formalization of map specifications in higher-order logic. Unlike previous work, 
	            we provide a collection of map specifications. We can verify and repair all 
	            possible map parts, from geometric to semantic elements, e.g., adjacency relationships, 
	            lane types, road boundaries, traffic signs, and intersections. Due to the modular 
	            design of our approach, one can integrate additional logics. We compare ontologies, 
	            answer set programming, and satisfiability modulo theories with our higher-order logic 
	            verification algorithm. Our evaluation shows that our approach can efficiently verify 
	            and repair maps from several data sources and of different map sizes. We provide our 
	            tool as part of the CommonRoad Scenario Designer toolbox available 
	            at commonroad.in.tum.de.},
}
```
